// <nowiki>
/*
(c) 2018 by Magnus Manske

This script loads mix'match (https://tools.wmflabs.org/mix-n-match) entries associated with this item.
To reduce traffic, the script only runs if there is, at load time, at least one property in the item that *might* have a match on Mix'n'match.
References can only be dragged onto statements that were present at load time. To drag references onto new statements, reload page first.

Usage: importScript( 'User:Magnus_Manske/mixnmatch_gadget.js' );
*/


let mixnmatch_gadget = function () {

	// NOTE: Generated with https://tools.wmflabs.org/mix-n-match/api.php?query=get_wd_props
	let mnm_properties_in_mix_n_match = 
	[212,213,214,227,229,236,244,245,267,268,269,271,345,347,350,356,370,374,380,382,396,402,428,434,436,454,480,481,486,496,497,498,508,535,539,549,563,586,590,597,600,604,628,630,640,648,649,650,651,652,665,667,673,677,679,685,691,722,723,724,745,757,771,772,781,791,804,809,815,827,829,838,839,842,846,850,858,860,863,866,902,906,920,933,938,950,952,954,973,999,1005,1014,1015,1021,1026,1042,1043,1045,1047,1051,1055,1058,1070,1085,1138,1146,1150,1157,1186,1187,1188,1207,1214,1216,1219,1220,1225,1230,1233,1234,1235,1237,1242,1248,1250,1251,1252,1253,1254,1256,1258,1263,1274,1276,1277,1278,1280,1284,1285,1286,1288,1291,1296,1305,1315,1320,1323,1331,1338,1341,1362,1367,1394,1404,1415,1417,1422,1430,1440,1447,1463,1466,1469,1472,1473,1482,1550,1554,1556,1563,1564,1565,1566,1584,1585,1599,1602,1607,1609,1614,1615,1616,1617,1627,1631,1644,1648,1651,1653,1665,1667,1669,1679,1699,1702,1707,1710,1711,1712,1736,1741,1749,1764,1788,1795,1802,1807,1808,1816,1818,1819,1820,1821,1832,1838,1842,1866,1870,1871,1874,1882,1886,1888,1899,1900,1902,1907,1920,1933,1935,1936,1937,1938,1949,1952,1953,1954,1958,1959,1961,1977,1978,1984,1986,1987,1988,1989,1991,1996,2002,2004,2005,2008,2011,2014,2015,2016,2018,2027,2029,2034,2037,2040,2041,2042,2057,2070,2080,2087,2088,2089,2099,2100,2163,2164,2167,2174,2179,2180,2190,2191,2192,2194,2205,2252,2253,2263,2266,2267,2268,2272,2273,2277,2278,2280,2287,2323,2332,2338,2339,2340,2342,2344,2345,2347,2349,2355,2367,2372,2373,2380,2381,2382,2383,2385,2397,2398,2399,2401,2418,2423,2424,2428,2431,2432,2446,2448,2450,2452,2454,2455,2456,2457,2461,2464,2467,2468,2469,2472,2477,2478,2481,2482,2483,2485,2492,2494,2498,2510,2514,2516,2520,2525,2529,2530,2531,2533,2537,2538,2549,2558,2566,2584,2588,2600,2605,2611,2618,2620,2639,2651,2657,2666,2685,2688,2689,2694,2704,2721,2725,2726,2728,2732,2733,2734,2736,2741,2742,2745,2748,2750,2751,2753,2759,2760,2776,2799,2800,2809,2812,2815,2816,2829,2830,2833,2843,2858,2861,2862,2864,2880,2886,2905,2914,2915,2924,2930,2939,2940,2941,2944,2945,2950,2966,2967,2971,2973,2977,2979,2985,2991,3002,3003,3009,3012,3017,3021,3029,3031,3038,3051,3054,3055,3056,3058,3059,3060,3068,3072,3073,3074,3077,3088,3098,3101,3104,3107,3109,3121,3122,3123,3124,3126,3130,3138,3151,3154,3159,3160,3162,3175,3177,3178,3187,3188,3191,3202,3203,3206,3217,3218,3219,3222,3226,3233,3235,3236,3241,3273,3277,3281,3283,3288,3290,3293,3297,3298,3304,3314,3316,3322,3324,3325,3327,3328,3338,3346,3348,3351,3353,3360,3366,3368,3371,3372,3379,3380,3381,3385,3388,3389,3392,3393,3397,3398,3401,3404,3407,3409,3410,3411,3412,3417,3420,3421,3425,3427,3429,3430,3435,3436,3442,3443,3444,3449,3456,3462,3468,3469,3471,3472,3473,3475,3478,3479,3481,3482,3499,3505,3506,3509,3511,3513,3515,3516,3517,3527,3531,3534,3535,3541,3544,3545,3547,3550,3553,3558,3565,3569,3576,3588,3589,3594,3595,3599,3603,3606,3609,3612,3615,3626,3630,3638,3640,3642,3645,3647,3654,3671,3673,3682,3689,3692,3695,3696,3698,3706,3711,3728,3746,3760,3763,3782,3788,3790,3791,3794,3796,3797,3807,3827,3829,3832,3841,3844,3847,3848,3850,3854,3856,3866,3881,3885,3887,3895,3906,3907,3908,3910,3911,3915,3916,3923,3924,3925,3927,3929,3935,3940,3945,3946,3953,3954,3956,3957,3958,3960,3961,3963,3965,3968,3979,3980,3981,3984,3987,3988,4008,4009,4012,4013,4014,4019,4025,4026,4030,4037,4040,4042,4046,4050,4053,4057,4059,4060,4061,4062,4065,4068,4069,4073,4079,4081,4083,4084,4086,4087,4088,4089,4093,4107,4110,4114,4119,4122,4124,4125,4126,4129,4133,4145,4146,4154,4158,4170,4171,4172,4177,4182,4186,4194,4200,4203,4204,4206,4208,4212,4215,4217,4227,4228,4235,4248,4254,4255,4257,4259,4260,4263,4276,4284,4286,4289,4293,4301,4303,4305,4306,4307,4311,4315,4317,4318,4327,4331,4332,4338,4342,4343,4347,4348,4357,4360,4361,4362,4363,4364,4365,4366,4367,4368,4372,4374,4375,4376,4377,4380,4381,4382,4383,4384,4385,4386,4398,4399,4402,4405,4406,4407,4410,4412,4413,4414,4415,4416,4418,4422,4427,4431,4432,4433,4435,4439,4450,4458,4459,4461,4462,4463,4465,4468,4469,4470,4474,4476,4477,4481,4482,4488,4489,4490,4498,4507,4513,4514,4522,4523,4524,4526,4527,4531,4533,4534,4535,4538,4539,4540,4542,4544,4546,4547,4548,4549,4553,4560,4561,4562,4563,4567,4571,4573,4575,4581,4583,4585,4587,4588,4589,4590,4594,4597,4604,4605,4606,4607,4609,4610,4611,4612,4613,4615,4616,4618,4620,4621,4623,4625,4627,4629,4630,4631,4632,4636,4637,4638,4641,4643,4644,4650,4651,4652,4657,4658,4659,4660,4663,4664,4665,4666,4668,4671,4672,4673,4674,4684,4687,4690,4694,4696,4697,4698,4699,4700,4702,4704,4705,4706,4708,4709,4710,4713,4715,4716,4718,4720,4722,4723,4724,4726,4727,4729,4730,4731,4732,4738,4740,4741,4744,4749,4751,4753,4757,4759,4760,4761,4769,4771,4772,4778,4793,4795,4798,4799,4801,4804,4806,4808,4813,4816,4818,4819,4823,4834,4836,4839,4845,4846,4847,4848,4854,4857,4858,4861,4868,4872,4885,4887,4888,4890,4891,4892,4893,4898,4903,4916,4917,4919,4927,4928,4929,4930,4931,4932,4935,4943,4946,4947,4953,4957,4959,4960,4961,4962,4963,4965,4972,4978,4979,4980,4983,4985,4989,4991,4992,4997,5002,5003,5007,5010,5011,5015,5016,5019,5029,5031,5034,5035,5036,5037,5050,5055,5058,5062,5064,5073,5080,5081,5082,5088,5094,5097,5101,5104,5106,5115,5142,5144,5147,5156,5160,5175,5177,5179,5198,5210,5211,5212,5213,5216,5218,5220,5224,5226,5239,5240,5241,5243,5245,5246,5247,5250,5261,5263,5267,5269,5273,5284,5287,5290,5291,5293,5295,5296,5298,5302,5303,5306,5308,5310,5319,5320,5321,5325,5327,5329,5332,5333,5338,5341,5343,5344,5345,5346,5354,5357,5359,5360,5361,5362,5365,5368,5370,5371,5372,5374,5375,5376,5377,5378,5379,5380,5382,5390,5392,5393,5394,5395,5397,5400,5408,5409,5414,5415,5417,5418,5419,5421,5430,5432,5442,5445,5449,5450,5451,5453,5459,5463,5465,5466,5468,5469,5470,5473,5476,5477,5478,5491,5492,5495,5496,5498,5502,5503,5504,5505,5506,5508,5510,5513,5515,5516,5527,5532,5535,5538,5539,5540,5542,5544,5549,5552,5556,5559,5561,5567,5568,5570,5571,5585,5586,5590,5597,5603,5611,5614,5615,5616,5617,5621,5622,5632,5635,5640,5641,5643,5645,5646,5648,5657,5659,5661,5663,5666,5688,5690,5691,5693,5694,5695,5698,5704,5705,5712,5714,5716,5719,5731,5733,5734,5735,5737,5739,5745,5747,5748,5750,5752,5763,5764,5773,5777,5780,5785,5786,5789,5792,5794,5795,5796,5799,5818,5819,5820,5823,5827,5829,5833,5834,5839,5840,5843,5859,5862,5868,5882,5885,5887,5908,5914,5918,5922,5925,5930,5936,5944,5945,5948,5950,5951,5952,5953,5958,5959,5960,5963,5964,5966,5968,5971,5981,5983,5984,5985,5986,5987,5990,5999,6002,6004,6005,6006,6009,6010,6011,6012,6015,6016,6018,6019,6024,6030,6032,6033,6034,6036,6037,6038,6039,6040,6041,6042,6044,6045,6047,6048,6049,6051,6056,6060,6068,6071,6078,6096,6100,6103,6105,6109,6113,6114,6126,6127,6128,6130,6134,6136,6137,6139,6141,6142,6143,6145,6150,6152,6155,6156,6158,6159,6160,6161,6163,6167,6170,6172,6173,6174,6175,6176,6178,6181,6182,6183,6188,6189,6190,6194,6200,6202,6206,6213,6221,6226,6227,6228,6231,6234,6235,6239,6240,6262,6263,6264,6268,6278,6282,6283,6284,6287,6290,6292,6295,6296,6298,6299,6300,6302,6304,6325,6327,6337,6341,6342,6348,6352,6362,6371,6381,6398,6400,6403,6404,6406,6412,6414,6417,6419,6422,6433,6434,6461,6462,6463,6465,6466,6467,6472,6475,6479,6481,6483,6485,6486,6488,6489,6494,6501,6502,6515,6517,6520,6525,6527,6528,6537,6541,6548,6549,6551,6554,6559,6565,6567,6575,6578,6582,6583,6584,6585,6594,6595,6597,6599,6600,6601,6605,6610,6616,6617,6618,6623,6624,6627,6628,6637,6643,6644,6652,6666,6677,6679,6682,6683,6693,6696,6698,6705,6706,6713,6714,6715,6717,6720,6722,6723,6726,6727,6728,6734,6736,6738,6744,6745,6746,6748,6750,6751,6756,6760,6764,6770,6773,6774,6777,6783,6787,6792,6797,6804,6805,6811,6815,6816,6820,6821,6827,6831,6839,6844,6851,6853,6862,6863,6864,6867,6868,6869,6870,6873,6878,6880,6881,6891,6898,6901,6903,6904,6907,6915,6916,6917,6919,6925,6926,6928,6931,6933,6934,6941,6947,6963,6976,6981,6984,6999,7003,7005,7006,7012,7019,7025,7030,7033,7037,7038,7041,7042,7044,7046,7048,7049,7052,7064,7072,7073,7074,7076,7077,7089,7090,7094,7105,7106,7107,7109,7128,7131,7142,7182,7194,7197,7203,7208,7224,7226,7232,7236,7254,7255,7276,7278,7282,7293,7296,7299,7302,7303,7305,7306,7311,7314,7317,7324,7326,7329,7331,7334,7340,7346,7349,7357,7370,7372,7387,7388,7400,7403,7430,7433,7438,7439,7440,7444,7445,7449,7464,7466,7467,7471,7472,7473,7474,7480,7491,7496,7498,7502,7507,7509,7512,7516,7517,7518,7520,7521,7524,7533,7540,7541,7546,7554,7555,7556,7558,7560,7562,7563,7564,7565,7567,7568,7570,7574,7578,7580,7585,7591,7592,7595,7596,7597,7611,7613,7614,7616,7617,7619,7622,7623,7625,7626,7632,7634,7638,7639,7641,7642,7644,7645,7646,7648,7649,7660,7662,7663,7665,7666,7669,7672,7677,7685,7686,7687,7688,7689,7690,7691,7692,7695,7697,7698,7701,7702,7705,7707,7708,7711,7713,7715,7716,7723,7730,7731,7733,7734,7740,7741,7744,7747,7749,7751,7753,7754,7757,7759,7760,7761,7769,7772,7774,7775,7776,7778,7784,7786,7796,7799,7800,7802,7803,7807,7809,7810,7813,7819,7826,7828,7830,7839,7840,7842,7844,7847,7848,7850,7852,7853,7856,7858,7860,7870,7871,7874,7875,7879,7884,7885,7886,7889,7890,7897,7898,7901,7909,7914,7917,7918,7919,7926,7927,7928,7929,7932,7934,7935,7939,7941,7942,7943,7944,7950,7951,7952,7958,7960,7961,7962,7963,7970,7981,7982,7983,7986,7992,7993,7994,7995,7996,7997,7998,8003,8013,8014,8015,8018,8022,8023,8025,8033,8034,8035,8036,8042,8044,8051,8052,8059,8061,8065,8066,8068,8069,8072,8076,8080,8081,8084,8085,8087,8094,8095,8098,8104,8108,8112,8116,8122,8125,8130,8134,8135,8139,8143,8151,8164,8167,8171,8172,8179,8189,8207,8209,8210,8218,8219,8226,8227,8232,8233,8234,8235,8238,8255,8256,8260,8272,8281,8285,8286,8287,8290,8291,8292,8293,8294,8303,8311,8312,8313,8332,8334,8335,8341,8349,8364,8366,8369,8384,8385,8406,8412,8414,8418,8422,8429,8432,8441,8443,8446,8456,8457,8458,8462,8468,8472,8482,8488,8490,8491,8496,8501,8511,8526,8529,8541,8544,8545,8547,8551,8562,8566,8568,8569,8572,8575,8577,8578,8580,8581,8585,8589,8591,8603,8605,8606,8609,8613,8619,8629,8631,8633,8634,8636,8638,8642,8643,8657,8669,8672,8678,8688,8689,8695,8696,8707,8711,8713,8714,8718,8720,8721,8728,8729,8732,8748,8749,8750,8761,8765,8767,8777,8783,8793,8795,8796,8799,8802,8805,8811,8815,8817,8819,8821,8825,8826,8832,8833,8848,8849,8850,8851,8853,8854,8855,8858,8870,8880,8892,8896,8897,8902,8903,8911,8914,8915,8931,8941,8944,8945,8947,8950,8951,8953,8957,8975,8976,8980,8982,8984,8985,8992,8993,8994,8995,8998,8999,9000,9013,9017,9020,9026,9029,9033,9036,9037,9039,9041,9042,9044,9046,9048,9053,9054,9056,9057,9062,9075,9080,9081,9082,9084,9085,9087,9089,9090,9097,9105,9106,9110,9111,9113,9114,9117,9118,9122,9125,9129,9141,9159,9160,9163,9164,9165,9170,9173,9178,9179,9181,9185,9198,9199,9219,9226,9231,9232,9240,9242,9244,9245,9246,9247,9248,9251,9256,9258,9267,9272,9277,9281,9282,9285,9286,9287,9289,9298,9304,9307,9319,9328,9331,9336,9337,9352,9360,9365,9392,9399,9404,9407,9415,9426,9430,9432,9434,9437,9438,9443,9444,9452,9455,9457,9458,9459,9466,9467,9475,9477,9482,9492,9497,9498,9501,9502,9507,9510,9511,9518,9522,9525,9526,9534,9536,9538,9539,9541,9543,9545,9555,9556,9563,9564,9575,9585,9586,9588,9593,9594,9602,9603,9614,9619,9620,9621,9625,9626,9627,9628,9629,9631,9634,9635,9640,9642,9645,9646,9649,9650,9651,9653,9654,9656,9661,9678,9682,9684,9686,9692,9701,9702,9709,9710,9715,9716,9717,9720,9722,9726,9738,9741,9743,9747,9751,9761,9768,9772,9777,9780,9781,9786,9791,9795,9796,9799,9802,9804,9806,9807,9808,9821,9842,9843,9844,9851,9852,9853,9855,9858,9862,9865,9868,9870,9871,9875,9876,9880,9881,9884,9898,9913,9914,9915,9918,9919,9951,9952,9954,9956,9966,9979,9985,9995,10000,10006,10007,10009,10010,10011,10012,10013,10016,10018,10029,10030,10031,10033,10034,10049,10053,10054,10055,10056,10057,10058,10059,10068,10069,10070,10071,10085,10086,10090,10092,10100,10102,10103,10104,10105,10116,10126,10137,10141,10144,10147,10148,10149,10155,10160,10168,10169,10203,10217,10221,10225,10226,10231,10238,10243,10244,10252,10257,10258,10259,10265,10266,10272,10274,10276,10277,10297,10298,10299,10301,10314,10323,10330,10348,10351,10354,10359,10376,10379,10384,10392,10397,10399,10405,10414,10416,10428,10431,10436,10441,10469,10480,10499,10501,10505,10506,10516,10518,10525,10535,10536,10552,10553,10565,10618,10620,10638,10639,10670,10677,10700,10715,10717,10750,10753,10759,10761,10762,10763,10776,10779,10782,10789,10792,10793,10799,10801,10802,10803,10827,10828,10829,10838,10841,10842,10843,10844,10846,10848,10849,10851,10852,10853,10854,10857,10864,10865,10866,10867,10870,10873,10874,10876,10879,10880,10881,10883,10886,10887,10902,10908,10911,10915,10925,10935,10936,10937,10938,10947,10948,10949,10950,10958,10963,11003,11019,11020,11041,16521]
	;

	let mnm_property_values = {} ;
	let mnm_main_result = {} ;

	if ( mw.config.get('wgNamespaceNumber') != 0 ) return ;
	if ( mw.config.get('wgAction') != 'view' ) return ;

mw.loader.using(['vue', '@wikimedia/codex', 'jquery.ui', 'mediawiki.api'], function ( require ) {

	const MixnmatchGadgetApp = {
		template: `<div id='mixnmatch_container'>
		<table style='width:100%' id='mnm_entries'>
		<tbody>
			<tr v-for='(entry,entry_id) in data.entries' :id="'mnm_row_entry_'+entry.id">
				<td nowrap style='vertical-align:top;'>
					<span v-if='""+entry.q=="0"'>N/A</span>
					<span v-else-if='valueIsBlank(entry.q) || valueIsBlank(entry.user)'>
						<a href='#' @click.prevent='matchThisItemToEntry(entry_id,q_num)' title='Match the current item to this entry (on MnM and Wikidata)'><span style='font-size:14pt;'>⊕</span></a>
						<br/>
						<a href='#' @click.prevent='matchThisItemToEntry(entry_id,0)' title='Set this entry as "not applicable"'>N/A</span> 
					</span>
					<span v-else-if='entry.user==="0"' style='line-height: 100%;'>
						<a href='#' @click.prevent='matchThisItemToEntry(entry_id,q_num)' title='Match the current item to this entry (on MnM and Wikidata)' style='font-size:14pt;'>⊕</a>
						<br/>
						<a href='#' @click.prevent='removePreliminaryMatch(entry_id)' title="Remove the automatic match of this entry from MnM" style='font-size:14pt;'>&#8854;</span> 
					</span>
					<span v-else='valueIsBlank(entry.q) && entry.q != q_num'>
						<span style='font-size:14pt;color:red' title='This entry is matched to a different item!'>!</span>
					</span>
					<span v-else style='font-size:14pt' title='This entry is already matched to the current item'>✓</span>
				</td>

				<td nowrap style='vertical-align:top'>
					<span v-if='catalog2entry(entry).url!=""'>
						<a :href='catalog2entry(entry).url' target='_blank' title='Catalog website'>{{catalog2entry(entry).name}}</a>
					</span>
					<span v-else>{{catalog2entry(entry).name}}</span>
					<br/>
					(
					<a :href='"https://tools.wmflabs.org/mix-n-match/#/entry/"+entry.id' target='_blank' title='Entry in Mix&#39;n&#39;match'>entry</a>
					in
					<a :href='"https://tools.wmflabs.org/mix-n-match/#/catalog/"+entry.catalog' target='_blank' title='Catalog in Mix&#39;n&#39;match'>catalog</a>
					)
				</td>

				<td style='vertical-align:top;width:100%'>
					<div>
						<a v-if='entry.ext_url !== ""' :href='entry.ext_url' target='_blank'>{{entry.ext_name}}</a>
						<span v-else>{{entry.ext_name}}</span>
					</div>
					<div><small>{{entry.ext_desc}}</small></div>
				</td>

				<td style='vertical-align:top'>
					<img :entry='entry_id' class='mnm_drag' src='https://upload.wikimedia.org/wikipedia/commons/thumb/7/76/VisualEditor_-_Icon_-_Reference.svg/32px-VisualEditor_-_Icon_-_Reference.svg.png' title='Drag this onto a statement to add as a new reference' />
				</td>
			</tr>
		</tbody>
		</table>
		</div>`,
		data() { return {
			data : {} ,
			q : '' ,
			q_num : 0 ,
			mw_api : {} ,
			mnm_api : 'https://tools.wmflabs.org/mix-n-match/api.php' ,
		} } ,

		mounted : function () {
			let self = this ;
			self.q = mw.config.get('wgTitle') ;
			self.q_num = self.q.replace ( /\D/g , '' ) ;
			self.mw_api = new mw.Api() ;
			self.data = [] ;
			if ( (mnm_main_result.status||'') != 'OK' ) return ;
			self.data = mnm_main_result.data ;
		} ,

		updated : function () {
			let self = this ;
			$('#mnm_entries td').css ( {'border-bottom':'1px dotted #DDD'} ) ;
			self.addDraggable() ;
			self.addDroppable() ;
		} ,

		methods : {
			catalog2entry : function ( entry ) {
				return this.data.catalogs[entry.catalog] ;
			} ,
			
			setInMnM : function ( entry_id , q , callback ) {
				let self = this ;
				let params = {
					query:'match_q' ,
					tusc_user:mw.config.get('wgUserName') ,
					entry:entry_id ,
					q:q
				} ;
				$.getJSON ( self.mnm_api+'?callback=?' , params , function ( d ) {
					if ( d.status != 'OK' ) {
						//alert ( "Mix'n'match error: " + d.status ) ;
						return callback() ;
					}
					self.data.entries[entry_id].q = self.q.replace ( /\D/g , '' ) ;
					self.data.entries[entry_id].user = 12345 ; // Any user ID will do to show this as check-marked
					callback() ;
				} ) ;
			} ,

			removeFromMnM : function ( entry_id , callback ) {
				let self = this ;
				let params = {
					query:'remove_q' ,
					tusc_user:mw.config.get('wgUserName') ,
					entry:entry_id
				} ;
				$.getJSON ( self.mnm_api+'?callback=?' , params , function ( d ) {
					if ( d.status != 'OK' ) {
						//alert ( "Mix'n'match error: " + d.status ) ;
						return callback() ;
					}
					self.data.entries[entry_id].q = null ;
					self.data.entries[entry_id].user = null ;
					self.data.entries[entry_id].timestamp = null ;
					callback() ;
				} ) ;
			} ,


			checkAndSetInWikidata : function ( entry , callback ) {
				let self = this ;
				let catalog = self.data.catalogs[entry.catalog] ;
				if ( typeof catalog == 'undefined' ) return callback() ;
				if ( self.valueIsBlank(catalog.wd_prop) || !self.valueIsBlank(catalog.wd_qual) ) return callback() ;
				let prop = 'P' + catalog.wd_prop ;
				let summary = 'Matched to #mix\'n\'match [[:toollabs:mix-n-match/#/entry/' + entry.id + '|' + entry.ext_name + ' (#' + entry.id + ')]]' ;
				wdutil.addExternalIdStatement ( prop , entry.ext_id , summary , callback ) ;
			} ,
			
			matchThisItemToEntry : function ( entry_id , q_num ) {
				let self = this ;
				let entry = self.data.entries[entry_id] ;
				if ( typeof entry === 'undefined' ) return false ; // Paranoia
				
				$('#mnm_row_entry_'+entry.id+' td').css ( { 'background-color':'#999' } ) ;				
				let promise_mnm = new Promise((resolve, reject) => { self.setInMnM ( entry.id , q_num , resolve ) } ) ;
				let promise_wd = new Promise((resolve, reject) => { self.checkAndSetInWikidata ( entry , resolve ) } ) ;
				Promise.all([promise_mnm, promise_wd]).then((values) => {
					$('#mnm_row_entry_'+entry.id+' td').css ( { 'background-color':'white' } ) ;
					self.data.entries[entry_id].q = q_num ;
					self.data.entries[entry_id].user = 12345 ; // Any user ID will do to show this as check-marked
				} ) ;
			} ,

			removePreliminaryMatch : function ( entry_id ) {
				let self = this ;
				let entry = self.data.entries[entry_id] ;
				if ( typeof entry === 'undefined' ) return false ; // Paranoia

				$('#mnm_row_entry_'+entry.id+' td').css ( { 'background-color':'#999' } ) ;				
				let promise_mnm = new Promise((resolve, reject) => { self.removeFromMnM ( entry.id , resolve ) } ) ;
				Promise.all([promise_mnm]).then((values) => {
					$('#mnm_row_entry_'+entry.id+' td').css ( { 'background-color':'white' } ) ;
					self.data.entries[entry_id].q = null ;
					self.data.entries[entry_id].user = null ;
					self.data.entries[entry_id].timestamp = null ;
				} ) ;
			} ,
			
			getStatementFromReferenceNode : function ( target ) {
				let self = this ;
				let q = '' ;
				let statement = '' ;
				$.each ( target.classList , function ( k , v ) {
					let m = v.match ( /^wikibase-statement-(Q\d+)(.+)$/i ) ;
					if ( m === null ) return ;
					q = m[1].replace(/^q/,'Q') ;
					statement = m[1]+m[2] ;
				} );
				if ( q != self.q || statement === '' ) return ; // Something went wrong
				return statement ;
			} ,
			
			genericAPIaction : function ( json , callback ) {
				if ( typeof json.summary === 'undefined' ) json.summary = '' ;
				else json.summary += '; ' ;
				json.summary += 'using #mixnmatchgadget' ;
				this.mw_api.postWithEditToken ( json ) .done ( callback ) .fail ( function (a,b,c) {
					if ( b.error.info.test ( /^The statement has already a reference with hash / ) ) {
						callback ( b ) ;
					} else {
						alert ( "API call failed" ) ;
					}
				} ) ;
			} ,
			
			addDraggedRef : function ( params ) {
				let json = {
					action:'wbsetreference',
					statement:params.statement,
					snaks:JSON.stringify(params.snaks),
					format:'json'
				} ;
				this.genericAPIaction ( json , function ( d1 ) {
					let h = "<div>Dropped reference added!</div>" ;
					if ( typeof d1.error != 'undefined' ) h = "<div>"+d1.error.info+"</div>" ;
					$(params.target).find('div.wikibase-statementview-references-container div.wikibase-statementview-references-heading').append(h);
				} ) ;
			} ,
			
			valueIsBlank : function ( v ) {
				if ( typeof v == 'undefined' ) return true ;
				if ( v === null || v === 'null' ) return true ;
				if ( v === 0 ) return true ;
				if ( v === '' ) return true ;
				return false ;
			} ,
			
			dropReference : function ( event , ui , target , dragged ) {
				let self = this ;
				let entry_id = $(dragged).attr('entry') ;
				let entry = self.data.entries[entry_id] ;
				if ( typeof entry == 'undefined' ) return ;
				let catalog = self.data.catalogs[entry.catalog] ;
				if ( typeof catalog == 'undefined' ) return ;
				let catalog_has_property = !self.valueIsBlank(catalog.wd_prop) && self.valueIsBlank(catalog.wd_qual) ;
				let catalog_property = catalog_has_property ? 'P'+catalog.wd_prop : '' ;
				let statement = self.getStatementFromReferenceNode ( target ) ;
				if ( statement === '' ) return ; // Could not find statement
				
				let params = { statement : statement , snaks:{} , target:target , token:'dummy' } ;
				params.snaks['P1810'] = [{ snaktype:'value',property:'P1810',datavalue:{value:entry.ext_name,type:'string'},datatype:'string' }] ; // Named as
				if ( catalog_has_property ) {
					params.snaks[catalog_property] = [{ snaktype:'value',property:catalog_property,datavalue:{value:entry.ext_id,type:'string'},datatype:'external-id' }] ; // Catalog property => external ID
				} else if ( entry.ext_url != '' ) {
					params.snaks.P854 = [{ snaktype:'value',property:'P854',datavalue:{value:entry.ext_url,type:'string'},datatype:'url' }] ;
				}
				if ( !self.valueIsBlank(catalog.source_item) ) {
					params.snaks['P248'] = [{ snaktype:'value',property:'P248',datavalue:{value:{"entity-type":"item","numeric-id":catalog.source_item,"id":"Q"+catalog.source_item},type:'wikibase-entityid'},datatype:'wikibase-item' }] ; // Stated in
				}

				self.addDraggedRef ( params ) ;
			} ,
			
			addDraggable : function () {
				$('img.mnm_drag').css({cursor:'grab'}).draggable({
					zIndex:1099,
					appendTo:'body',
					revert:false,
					cursor:'dragging',
					helper:'clone'
				}) ;
			} ,
			
			addDroppable : function () {
				let self = this ;
				$('div.wikibase-statementview').droppable ( {
					accept: function (dropped) {
						if ( $(dropped).hasClass('mnm_drag') ) return true ;
						return false ;
					} ,
					hoverClass: "wikibase-statementview-droptarget",
					drop: function( event, ui ) {
						let target = $(this)[0] ;
						let dragged = $(ui.draggable)[0] ;

						if ( $(dragged).hasClass('mnm_drag') ) self.dropReference(event,ui,target,dragged) ;
					}
				} ) ;
			} ,
		} ,

		components: {
		}

	}


	$.each ( mnm_properties_in_mix_n_match , function ( dummy , prop_number ) {
		let property = 'P' + prop_number ;
		let prop_node = $('#'+property) ;
		if ( prop_node.length === 0 ) return ;
		if ( typeof mnm_property_values[property] == 'undefined' ) mnm_property_values[property] = [] ;
		$.each ( prop_node.find('div.wikibase-statementview-mainsnak-container div.wikibase-snakview-value') , function ( dummy2 , value_node ) {
			mnm_property_values[property].push ( $(value_node).text() ) ;
		} ) ;
	} ) ;
	if ( Object.keys(mnm_property_values).length === 0 ) { // No relevant properties used in this item
		return ;
	}


	// Get associated Mix'n'match entries
	$.getJSON ( 'https://tools.wmflabs.org/mix-n-match/api.php?callback=?' , {
		query:'get_entries_by_q_or_value' ,
		q:mw.config.get('wgTitle') ,
		json:JSON.stringify(mnm_property_values)
	} , function (d) {
		if ( ((d.data||{}).entries||[]).length==0 ) return ; // No data
		wdutil_app.addTab({
			name: 'mixnmatch',
			label: "Mix'n'match",
		},function(id){
			mnm_main_result = d ;
			const mixnmatch_app = Vue.createMwApp(MixnmatchGadgetApp);
			mixnmatch_app.mount(id);
		});
	}) ;

})}


// This is to use the tab display and wdutil
var wdutil_app ;
//mw.loader.load('https://wikidata-todo.toolforge.org/wdutils.js');
mw.loader.load('https://www.wikidata.org/w/index.php?title=User:Magnus_Manske/wdutil.js&action=raw&ctype=text/javascript');
if ( typeof wdutil!='undefined' ) wdutil.loadCallback(mixnmatch_gadget);
else {
	var wdutil_loaded_callbacks ;
	if ( typeof wdutil_loaded_callbacks=='undefined' ) wdutil_loaded_callbacks = [] ;
	wdutil_loaded_callbacks.push(mixnmatch_gadget);
}

// </nowiki>
