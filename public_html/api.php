<?PHP

require_once '/data/project/magnustools/public_html/php/Widar.php' ;
$widar = new \Widar ( 'wikidata-todo' ) ;
$path = trim($widar->tfc->getRequest ( 'path' , '' )) ;
if ( $path != '' ) $widar->authorize_parameters = "path={$path}" ;
$widar->attempt_verification_auto_forward ( "/{$path}" ) ;
$widar->authorization_callback = 'https://wikidata-todo.toolforge.org/api.php?path='.urlencode($path) ;
if ( $widar->render_reponse(true) ) exit(0);

?>