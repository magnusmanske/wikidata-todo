<?PHP

/*
Items with both properties:
SELECT item FROM pq_extid WHERE property IN (214,227) GROUP BY item HAVING count(distinct property)=2
*/

require_once ( '/data/project/mix-n-match/public_html/php/ToolforgeCommon.php' ) ;
require_once ( '/data/project/mix-n-match/public_html/php/wikidata.php' ) ;

class WikidataCache {
	public $tfc ;
	public $dbm ;

	public function __construct () {
		$this->tfc = new \ToolforgeCommon('wikidata-todo') ;
		$this->UUIDv4 = '/^[0-9A-F]{8}-[0-9A-F]{4}-4[0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/i';
	}

	public function items_with_properties_generator ( $all_properties=[] ) {
		$ret = [] ;
		$this->to_numeric_array ( $all_properties , true ) ;
		$sql = "SELECT `item` FROM `pq_extid` WHERE `property` IN (".implode(',',$all_properties).") GROUP BY `item` HAVING count(distinct `property`)=".count($all_properties) ;
		try {
			$result = $this->getSQL ( $sql ) ;
			while($o = $result->fetch_object()) yield $o->item ;
		} catch(Exception $e) {
  			echo $e->getMessage();
		}
		yield from [] ;

	}

	public function to_numeric_array ( &$arr , $positive_only = false ) {
		foreach ( $arr AS $k => $v ) $arr[$k] = $this->numeric ( $v ) ;
		if ( $positive_only ) {
			$arr = array_filter ( $arr , function ($v) { return $v > 0 ; } ) ;
		}
	}

	public function item_prop_generator ( $item_ids , $property ) {
		$this->to_numeric_array ( $item_ids , true ) ;
		if ( count($item_ids) > 0 ) {
			$property = $this->numeric ( $property ) ;
			$sql = "SELECT `item`,`text` FROM `vw_pq_text` WHERE `property`={$property} AND `item` IN (".implode(',',$item_ids).")" ;
			$result = $this->getSQL ( $sql ) ;
			while($o = $result->fetch_object()) yield [$o->item,$o->text] ;
		}
		yield from [] ;
	}

	public function get_stats() {
		$sql = "SELECT * FROM `vw_overview`" ;
		$result = $this->getSQL ( $sql ) ;
		return $result->fetch_object();
	}

	public function items_generator ( $item_ids ) {
		$wil = new \WikidataItemList ;
		$wil->loadItems ( $item_ids ) ;
		foreach ( $item_ids AS $item_id ) {
			$item = $wil->getItem ( $item_id ) ;
			if ( isset($item) ) yield $item ;
			else $this->removeItemFromDatabase ( $item_id ) ; # Does not exists on WD
		}
		yield from [] ;
	}

	public function extract_external_ids_from_item ( $item ) {
		$ret = [] ;
		if ( !isset($item->j) ) return $ret ;
		if ( !isset($item->j->claims) ) return $ret ;
		$item_id_numeric = $this->numeric($item->getQ()) ;
		foreach ( $item->j->claims AS $property => $claims ) {
			foreach ( $claims AS $claim ) {
				if ( !isset($claim->mainsnak) ) continue ;
				if ( ($claim->mainsnak->snaktype??'')!='value' ) continue ;
				if ( !isset($claim->mainsnak->datavalue) ) continue ;
				if ( ($claim->mainsnak->datatype??'')!='external-id' ) continue ;
				$uuid = strtoupper(explode('$',$claim->id)[1]) ;
				if ( !preg_match($this->UUIDv4,$uuid) ) continue ;
				$o = (object) [
					'item' => $item_id_numeric ,
					'property' => $this->numeric($property) ,
					'text' => $claim->mainsnak->datavalue->value ,
					'uuid' => $uuid ,
					'rank' => $claim->rank
				] ;
				$ret[$o->uuid] = $o ;
			}
		}
		return $ret ;
	}

	public function extract_external_ids_from_db ( $item_id ) {
		$ret = [] ;
		$item_id_numeric = $this->numeric($item_id) ;
		if ( $item_id_numeric <= 0 ) return $ret ;
		$sql = "SELECT * FROM `vw_pq_text` WHERE `item`={$item_id_numeric}" ;
		#print "{$sql}\n" ;
		$result = $this->getSQL ( $sql ) ;
		while($o = $result->fetch_object()) $ret[$o->uuid] = $o ;
		return $ret ;
	}

	public function remove_pq_ids ( $pq_ids ) {
		if ( count($pq_ids) == 0 ) return ;
		$pq_ids = array_unique($pq_ids) ;
		$sql = "DELETE FROM `pq_extid` WHERE id IN (".implode(',',$pq_ids).")" ;
		print "{$sql}\n" ;
		$this->getSQL($sql);
	}

	public function add_enternal_ids ( $external_ids ) {
		if ( count($external_ids) == 0 ) return ;
		$parts = [] ;
		foreach ( $external_ids AS $uuid => $o ) {
			$parts[] = $this->generate_insert_pq_part ( $o ) ;
		}
		$sql = "INSERT IGNORE INTO `pq_extid` (`item`,`property`,`text_size_id`,`rank`,`uuid_bin`) VALUES (" . implode ( '),(' , $parts ) . ")" ;
		#print "{$sql}\n" ;
		$this->getSQL($sql);
	}

	private function removeItemFromDatabase ( $item_id ) {
		$item_id = $this->numeric ( $item_id ) ;
		$sql = "DELETE FROM `pq_extid` WHERE `item`={$item_id}" ;
		$this->getSQL ( $sql ) ;
	}

	private function generate_insert_pq_part ( $o ) {
		$ret = [] ;
		$ret[] = $this->numeric ( $o->item ) ;
		$ret[] = $this->numeric ( $o->property ) ;
		$ret[] = $this->get_or_create_text_size_id ( $o->text ) ;
		$ret[] = "'" . $this->escape($o->rank) . "'" ;
		$ret[] = "unhex(replace('{$o->uuid}','-',''))" ; # unhex(replace(uuid(),'-','')) UUID => BINARY
		$ret = implode ( ',' , $ret ) ;
		return $ret ;
	}

	private function get_or_create_text_size_id ( $text ) {
		$text = trim ( $text ) ;
		$postfix = $this->get_table_postfix ( $text ) ;
		$table = "text_{$postfix}" ;
		$text_id = $this->get_or_create_text_id ( $text , $table ) ;
		$sql = "SELECT * FROM `text2size` WHERE `size`='{$postfix}' AND `text_id`={$text_id}" ;
		#print "{$sql}\n" ;
		$result = $this->getSQL ( $sql ) ;
		if ($o = $result->fetch_object()) return $o->id ;
		$sql = "INSERT IGNORE INTO `text2size` (`size`,`text_id`) VALUE ('{$postfix}',{$text_id})" ;
		#print "{$sql}\n" ;
		$this->getSQL ( $sql ) ;
		return $this->dbm->insert_id ;
	}

	private function get_or_create_text_id ( $text , $table ) {
		$sql = "SELECT * FROM `{$table}` WHERE `text`='".$this->escape($text)."'" ;
		#print "{$sql}\n" ;
		$result = $this->getSQL ( $sql ) ;
		if ($o = $result->fetch_object()) return $o->id ;
		$sql = "INSERT IGNORE INTO `{$table}` (`text`) VALUE ('".$this->escape($text)."')" ;
		#print "{$sql}\n" ;
		$this->getSQL ( $sql ) ;
		return $this->dbm->insert_id ;
	}

	private function get_table_postfix  ( $text ) {
		$l = strlen ( $text ) ;
		if ( $l <= 8 ) return '8' ;
		if ( $l <= 16 ) return '16' ;
		if ( $l <= 32 ) return '32' ;
		if ( $l <= 64 ) return '64' ;
		return 'medium' ;
	}

	public function get_kv ( $key ) {
		$sql = "SELECT * FROM `pq_kv` WHERE `key`='".$this->escape($key)."'" ;
		$result = $this->getSQL ( $sql ) ;
		while($o = $result->fetch_object()) return $o->value ;
	}

	public function set_kv ( $key , $value ) {
		$sql = "UPDATE `pq_kv` SET `value`='".$this->escape($value)."' WHERE `key`='".$this->escape($key)."'" ;
		$this->getSQL ( $sql ) ;
	}

	public function get_recent_changes_items ( $limit = 1000 ) {
		$this->dbwd = $this->tfc->openDBwiki('wikidatawiki');
		$ret = [] ;
		$this->last_rc = $this->get_kv ( 'last_rc' ) ;
		$sql = "SELECT `rc_title`,`rc_timestamp` FROM `recentchanges` WHERE `rc_namespace`=0 AND `rc_timestamp`>='{$this->last_rc}' ORDER BY `rc_timestamp` LIMIT {$limit}" ;
		#$sql = "SELECT `page_title`,`rev_timestamp` FROM `page`,`revision` WHERE `rev_timestamp`>='{$this->last_rc}' AND `rev_page`=`page_id` AND `page_namespace`=0 AND `page_is_redirect`=0 ORDER BY `rev_timestamp` LIMIT {$limit}" ;
		$result = $this->tfc->getSQL ( $this->dbwd , $sql ) ;
		while($o = $result->fetch_object()) {
			$ret[] = $o->rc_title ;
			if ( $this->last_rc < $o->rc_timestamp ) {
				$this->last_rc = $o->rc_timestamp ; # For update later
			}
		}
		$ret = array_unique($ret);
		return $ret ;
	}

	public function get_all_items_batch ( $limit = 1000 ) {
		$this->dbwd = $this->tfc->openDBwiki('wikidatawiki');
		$ret = [] ;
		$this->last_all = $this->numeric ( $this->get_kv ( 'last_all' ) ) ;
		$sql = "SELECT `page_title`,`page_id` FROM `page` WHERE `page_namespace`=0 AND `page_is_redirect`=0 AND `page_id`>{$this->last_all} ORDER BY `page_id` LIMIT {$limit}" ;
		$result = $this->tfc->getSQL ( $this->dbwd , $sql ) ;
		while($o = $result->fetch_object()) {
			$ret[] = $o->page_title ;
			if ( $this->last_all < $o->page_id ) {
				$this->last_all = $o->page_id ; # For update later
			}
		}
		$ret = array_unique($ret);
		return $ret ;
	}

	public function update_all ( $limit = 1000 ) {
		$item_ids = $this->get_all_items_batch ( $limit ) ;
		$this->update_items ( $item_ids ) ;
		$this->set_kv ( 'last_all' , $this->last_all ) ;
	}

	public function update_from_recent_changes ( $limit = 1000 ) {
		$item_ids = $this->get_recent_changes_items ( $limit ) ;
		$this->update_items ( $item_ids ) ;
		$this->set_kv ( 'last_rc' , $this->last_rc ) ;
	}

	public function update_item ( $item ) {
		$external_ids_wd = $this->extract_external_ids_from_item($item) ;
		$external_ids_db = $this->extract_external_ids_from_db($item->getQ()) ;
		$add_external_ids = [] ;
		$remove_pq_ids = [] ;
		foreach ( $external_ids_db AS $uuid => $o ) {
			if ( !isset($external_ids_wd[$uuid]) ) $remove_pq_ids[] = $o->pq_id ;
			else {
				foreach ( $external_ids_wd[$uuid] AS $k => $v ) {
					if ( $external_ids_db[$uuid]->$k == $v ) continue ;
					$remove_pq_ids[] = $o->pq_id ; # Remove existing
					$add_external_ids[$uuid] = $external_ids_wd[$uuid] ; # Add new
					break ;
				}
			}
		}
		foreach ( $external_ids_wd AS $uuid => $o ) {
			if ( isset($external_ids_db[$uuid]) ) continue ;
			$add_external_ids[$uuid] = $o ; # Add new
		}
		$this->remove_pq_ids($remove_pq_ids) ;
		$this->add_enternal_ids($add_external_ids);
	}

	public function update_items ( $item_ids ) {
		foreach ( $this->items_generator($item_ids) AS $item ) {
			$this->update_item ( $item ) ;
		}
	}


	public function numeric ( $s ) {
		return preg_replace('|\D|','',"{$s}")*1;
	}

	public function dbmConnect($connection=true) {
		if ( $connection ) $this->dbm = $this->openToolDB() ;
		else unset ( $this->dbm ) ;
	}

	private function openToolDB () {
		$db = $this->tfc->openDBtool ( 'wikidata_cache_p' , '' , '' , false ) ;
		if ( $db === false ) die ( "Cannot access DB: " . $o['msg'] ) ;
		$db->set_charset("utf8") ;
		return $db ;
	}

	private function logError ( $msg = 'Unspecified error' ) {
		$this->last_error = $msg ;
		return false ;
	}

	public function escape ( $s ) {
		if ( !isset($this->dbm) ) $this->dbmConnect();
		return $this->dbm->real_escape_string ( $s ) ;
	}

	public function getSQL ( $sql ) {
		$reconnects_left = 4 ;
		while ( $reconnects_left > 0 ) {
			if ( !isset($this->dbm) ) $this->dbm = $this->openToolDB() ;
			try {
				$ret = $this->tfc->getSQL ( $this->dbm , $sql , 2 ) ;
				return $ret ;
			} catch (Exception $e) {
				unset ( $this->dbm ) ;
				$reconnects_left-- ;
				sleep ( 5 ) ;
			}
		}
	}
}

?>