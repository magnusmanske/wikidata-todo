#!/usr/bin/php
<?PHP

require_once ( 'public_html/php/common.php' ) ;

$prop = 245 ;
$url = "$wdq_internal_url?q=" . urlencode('claim['.$prop.'] and noclaim[214]') . "&props=$prop"  ;
$j = json_decode ( file_get_contents ( $url ) ) ;

$filename = "public_html/static/aux_props.tab" ;
$handle = fopen($filename, 'w') ;

foreach ( $j->props->$prop AS $v ) {
	if ( $v[1] != 'string' ) continue ;
	$q = $v[0] ;
	$pv = $v[2] ;
	$url = "http://viaf.org/viaf/search?query=local.names+all+%22" . $pv . "%22+and+local.sources+any+%22jpg%22&sortKeys=holdingscount&recordSchema=BriefVIAF" ;
#	print "$url\n" ;
	$h = file_get_contents ( $url ) ;
	if ( !preg_match_all ( '/<v:viafID .+?>(\d+)<\/v:viafID>/' , $h , $m ) ) continue ;
	if ( count($m) != 2 ) continue ;
	if ( count($m[1]) != 1 ) continue ;
	$viaf = $m[1][0] ;
	fwrite ( $handle , "Q$q\tP214\t\"$viaf\"\n" ) ;
}

fclose ( $handle ) ;

?>