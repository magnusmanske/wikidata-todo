<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');

require_once ( 'php/ToolforgeCommon.php' ) ;
$tfc = new ToolforgeCommon();

print $tfc->getCommonHeader ('Images in categories used on Commons' ) ;

$category = trim($tfc->getRequest("category",""));
$depth = $tfc->getRequest("depth","5")*1;
$zero_only = $tfc->getRequest("zero_only","0")*1;
$zero_only_checked = $zero_only ? "checked" : "" ;

print <<<HTML
<form method="get" class="form">
<div style="width:20rem;">Root category: <input type="text" name="category" value="{$category}" style="width:auto;" /></div>
<div>Depth: <input type="number" name="depth" value="{$depth}" style="width:auto;" /></div>
<div><label><input type="checkbox" name="zero_only" value="1" {$zero_only_checked} /> Show only categories with no files used on Wikidata</label></div>
<div><input type="submit" value="Do it" class="btn btn-outline-primary" /></div>
</form>
HTML;

if ( $category!='' ) {
	$category_encoded = urlencode($category);
	$url = "https://petscan.wmflabs.org/?cb_labels_any_l=1&ores_prob_to=&links_to_no=&ores_prob_from=&ns%5B14%5D=1&namespace_conversion=keep&show_disambiguation_pages=both&sitelinks_yes=&before=&sitelinks_no=&maxlinks=&categories={$category_encoded}%0D%0A&language=commons&templates_no=&project=wikimedia&labels_yes=&minlinks=&cb_labels_no_l=1&output_limit=&negcats=&referrer_url=&depth=5&outlinks_yes=&search_wiki=&larger=&ores_type=any&outlinks_any=&pagepile=&wikidata_prop_item_use=&format=json&edits%5Bbots%5D=both&cb_labels_yes_l=1&min_redlink_count=1&interface_language=en&active_tab=tab_pageprops&langs_labels_yes=&source_combination=&wikidata_item=no&edits%5Banons%5D=both&search_max_results=5000&subpage_filter=either&common_wiki_other=&doit=&output_compatability=quick-intersection&sparse=1" ;
	$j = json_decode(file_get_contents($url));
	$db = $tfc->openDBwiki('commonswiki');
	$categories = [] ;
	foreach ( $j->pages AS $page ) $categories[] = $db->real_escape_string(preg_replace('|^Category:|','',$page)) ;
	print "<h2>Results</h2>";
	if ( count($categories)==0 ) print "<p>No Commons categories matching the query.</p>";
	else {
		$categories = '"'.implode('","',$categories).'"';
		$zero_only_condition = $zero_only ? "AND files_used_on_wikidata=0" : "";
		$sql = <<<SQL
			SELECT cl1.cl_to,
			(SELECT count(DISTINCT page_id) FROM page,globalimagelinks,categorylinks cl2 WHERE cl1.cl_to=cl2.cl_to AND cl2.cl_from=page_id AND page_namespace=6 AND gil_to=page_title AND gil_wiki="wikidatawiki" AND gil_page_namespace_id=0) AS files_used_on_wikidata,
			(SELECT count(DISTINCT page_id) FROM page,categorylinks cl2 WHERE cl1.cl_to=cl2.cl_to AND cl2.cl_from=page_id AND page_namespace=6) AS files_total
			FROM categorylinks cl1 
			WHERE cl_to not rlike "detail"
			AND cl_to not rlike "view"
			AND cl_to IN ($categories)
			GROUP BY cl_to
			HAVING files_total>0 {$zero_only_condition}
			ORDER BY files_total DESC;
		SQL;
		$result = $tfc->getSQL($db,$sql);
		if ( $zero_only ) print "<p><b>These Commons categories contain files, but none are used on Wikidata (yet).</b></p>";
		print "<table class='table'>" ;
		print "<thead><tr><th>Category</th>";
		if ( !$zero_only ) print "<th>Files used on Wikidata</th>";
		print "<th>Files in category</th></tr></thead>";
		print "<tbody>";
		while($o = $result->fetch_object()){
			$url = "https://commons.wikimedia.org/wiki/Category:".urlencode($o->cl_to) ;
			$pretty_cat = str_replace('_',' ',$o->cl_to);
			print "<tr>" ;
			print "<td><a href='{$url}' target='_blank'>{$pretty_cat}</a></td>";
			if ( !$zero_only ) print "<td>{$o->files_used_on_wikidata}</td>";
			print "<td>{$o->files_total}</td>";
			print "</tr>" ;
		}
		print "<tbody></table>";
	}
}

print $tfc->getCommonFooter() ;

?>