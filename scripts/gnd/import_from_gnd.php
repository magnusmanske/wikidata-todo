#!/usr/bin/php
<?PHP

require_once ( '/data/project/wikidata-todo/scripts/gnd/ImportFromGND.php' ) ;

if ( count($argv) == 1 ) {
	print "USAGE: {$argv[0]} [QID|GND_ID] [1=testing; default: create item]\n" ;
	exit(0);
}

$gnd = new ImportFromGND();
if ( preg_match('|^Q\d+$|',$argv[1]) ) $gnd->load_from_wikidata_item ( $argv[1] ) ;
else $gnd->load_from_gnd($gnd_id) ;

$commands = $gnd->getStatements() ;
if ( !isset($commands) ) exit(0);
print implode("\n",$commands)."\n" ;
if ( ($argv[2]??'') != '1' ) {
	$q = $gnd->runQuickStatements($commands) ;
	if ( isset($gnd->item_id) ) $q = $gnd->item_id ;
	print "https://www.wikidata.org/wiki/{$q}\n" ;
}

?>