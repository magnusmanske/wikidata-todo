#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');
ini_set('memory_limit','500M');

require_once ( '/data/project/wikidata-todo/scripts/wikiflix/wikiflix.php' ) ;


$wf = new WikiFlix;

if ( isset($argv[1]) and $argv[1]=='reset' ) {
	$wf->reset_all();
}

if ( isset($argv[1]) and $argv[1]=='json' ) {
	$wf->generate_main_page_data();
} else if ( isset($argv[1]) and $argv[1]=='person' ) {
	$wf->update_persons();
} else if ( isset($argv[1]) and $argv[1]=='sec_labels' ) {
	$wf->import_missing_section_labels();
} else if ( isset($argv[1]) and $argv[1]=='whitelist' ) {
	$wf->import_movie_whitelist();
} else {
	$wf->update_from_sparql();
	$wf->make_rc_unavailable();
	$wf->import_movie_whitelist();
	$wf->add_missing_movie_details();	
	$wf->update_persons();
	$wf->generate_main_page_data();
}

/* NOTES

Commons iframe:
<iframe src="https://commons.wikimedia.org/wiki/File:Tess_of_the_Storm_Country_(1914).webm?embedplayer=yes" width="null" height="20" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen></iframe>

Internet archive iframe:
<iframe src="https://archive.org/embed/peril_of_doc_ock" width="640" height="480" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen></iframe>

Youtube iframe no cookies:
<iframe width="1440" height="762" 
src="https://www.youtube-nocookie.com/embed/7cjVj1ZyzyE" frameborder="0" allow="autoplay; encrypted-media" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen></iframe>

*/

?>