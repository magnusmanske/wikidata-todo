var wd_new_item_wizard = {

	api : '//www.wikidata.org/w/api.php?callback=?' ,
	
	init : function () {
		var self = this ;
		var portletLink = mw.util.addPortletLink( 'p-navigation', '#', 'New item wizard','t-wd_niw');
		$(portletLink).click ( function () {
			self.run() ;
			return false ;
		} ) ;
	} ,
	
	run : function () {
		var self = this ;
		if ( typeof ( wd_auto_desc ) === 'undefined' ) {
			$.getScript ( 'https://en.wikipedia.org/w/index.php?title=MediaWiki:Wdsearch-autodesc.js&action=raw&ctype=text/javascript&smaxage=21600&maxage=86400' , function () {
				self.startDialog() ;
			} ) ;
		} else {
			self.startDialog() ;
		}
		
	} ,

	startDialog : function () {
		var self = this ;
		self.id = "new_item_wizard_dialog" ;
		$('#'+self.id).remove() ;
		$('#mw-content-text').before ( "<div id='"+self.id+"' title='New item wizard' style='overflow:auto'><div id='"+self.id+"_content'></div></div>" ) ;
		$('#'+self.id).dialog ( {
			modal : true ,
			height:400 ,
			maxWidth: 1000,
			width : '500px'
		} ) ;
		
		self.title = 'Charles Darwin' ; // TEST FIXME
		self.showStep ( 1 ) ;
	} ,
	
	showStep : function ( new_step ) {
		var self = this ;
		self.step = new_step ;
		var focus ;
		var h = '' ;
		h += "<h2>Step " + self.step + "</h2><div><form id='niw_form'>" ;
		
		if ( self.step == 1 ) {
			h += "<p style='font-size:14pt'>So, you want to create a new item?</p>" ;
			h += "<p>Not all topics make for valid items on Wikidata. If you think your new item would be relevant on w.g. Wikipedia, or if it is required as a \"structural\" item on Wikidata, then by all means, proceed! If the item is about your cat, don't.</p>" ;
			h += "<p>Enter a title for your new item:</p>" ;
			h += "<p><input id='niw_title' type='text' style='width:300px' value='" + self.title + "' /></p>" ;
			focus = '#niw_title' ;
		} else if ( self.step == 2 ) {
			h += "<p style='font-size:14pt'>Make sure your item does not already exist!</p>" ;
			h += "<div id='niw_search_results' style='max-height:240px;border:1px solid #DDD;padding:2px;overflow:auto'>" ;
			h += "</div>" ;
		} else if ( self.step == 3 ) {
			h += "<p style='font-size:14pt'>Classify your item</p>" ;
			h += "<div id='niw_classify' style='max-height:240px;border:1px solid #DDD;padding:2px;overflow:auto'>" ;
			h += "<div><label><input type='radio' name='niw_classification' value='human' p='5' q='31' /> A (real) person</label></div>" ;
			h += "<div><label><input type='radio' name='niw_classification' value='other' /> Something else</label></div>" ;
			h += "</div>" ;
		}

		h += "<p style='text-align:right'>" ;
		if ( self.step > 1 ) h += "<input type='button' id='niw_prev' value='Previous step' /> &nbsp; " ;
		h += "<input type='submit' id='niw_next' value='Next step' /></p>" ;
		
		h += "</div></div>" ;
		$('#'+self.id+'_content').html ( h ) ;
		if ( focus !== undefined ) $(focus).focus() ;
		$('#niw_form').submit ( function ( e ) {
			e.preventDefault() ;
			self.next() ;
			return false ;
		} ) ;
		$('#niw_prev').click ( function () {
			self.showStep ( self.step-1 ) ;
		} ) ;
		
		if ( self.step == 2 ) self.search() ;
	} ,
	
	search : function () {
		var self = this ;
		$.getJSON ( self.api , {
			action:'query',
			list:'search',
			srsearch:self.title,
			srlimit:50,
			srnamespace:0,
			format:'json'
		} , function (d) {
			var h = "<table border=1 cellspacing=0 cellpadding=2><tbody>" ;
			var qs = [] ;
			$.each ( ((d.query||{}).search||{}) , function ( k , v ) {
				qs.push ( v.title ) ;
				h += "<tr>" ;
				h += "<td><a style='color:blue' id='niw_result_" + v.title + "' target='_blank' href='/wiki/" + v.title + "'>" + v.title + "</a></td>" ;
				h += "<td style='font-size:8pt' id='niw_desc_" + v.title + "'><i>Loading description...</i></td>" ;
				h += "</tr>" ;
			} ) ;
			h += "</tbody></table>" ;
			$('#niw_search_results').html ( h ) ;
			$.each ( qs , function ( dummy , q ) {
				wd_auto_desc.labelItem ( q , function ( label ) {
					$('#niw_result_'+q).text ( label ) ;
				} ) ;
				wd_auto_desc.loadItem ( q , { target:$('#niw_desc_'+q) , links:'wikidata' } ) ;
			} ) ;
//			console.log ( qs ) ;
		} ) ;
	} ,
	
	next : function () {
		var self = this ;
		
		if ( self.step == 1 ) {
			self.title = $('#niw_title').val() ;
			self.showStep ( 2 ) ;
			return ;
		}

		if ( self.step == 2 ) {
			self.showStep ( 3 ) ;
			return ;
		}
		
	} ,



	fin : true
} ;



$(document).ready ( function () {
	wd_new_item_wizard.init() ;
} ) ;