<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR); // |E_ALL
ini_set('display_errors', 'On');

include_once ( 'php/common.php' ) ;

$url = 'https://petscan.wmflabs.org/?language=commons&project=wikimedia&depth=0&categories=Files%20from%20BioAcoustica&combination=subset&negcats=&ns%5B6%5D=1&larger=&smaller=&minlinks=&maxlinks=&before=&after=&max_age=&show_redirects=both&edits%5Bbots%5D=both&edits%5Banons%5D=both&edits%5Bflagged%5D=both&templates_yes=&templates_any=&templates_no=&outlinks_yes=&outlinks_any=&outlinks_no=&links_to_all=&links_to_any=&links_to_no=&sparql=&manual_list=&manual_list_wiki=&pagepile=&wikidata_source_sites=&subpage_filter=either&common_wiki=auto&source_combination=&wikidata_item=no&wikidata_label_language=&wikidata_prop_item_use=&wpiu=any&sitelinks_yes=&sitelinks_any=&sitelinks_no=&min_sitelink_count=&max_sitelink_count=&labels_yes=&cb_labels_yes_l=1&langs_labels_yes=&labels_any=&cb_labels_any_l=1&langs_labels_any=&labels_no=&cb_labels_no_l=1&langs_labels_no=&format=json&output_compatability=quick-intersection&sparse=on&sortby=none&sortorder=ascending®exp_filter=&min_redlink_count=1&doit=Do%20it%21&interface_language=en&active_tab=tab_output' ;
$j = json_decode ( file_get_contents ( $url ) ) ;

print get_common_header ( '' , 'BioAcoustica FIle Matcher' ) ;

$spec2files = [] ;
foreach ( $j->pages AS $file ) {
	$file = preg_replace ( '/^File:/' , '' , $file ) ;
	$file2 = preg_replace ( '/_/' , ' ' , $file ) ;
	$file2 = preg_replace ( '/ MHV /' , ' ' , $file2 ) ;
	if ( preg_match ( '/^BioAcoustica [0-9 \-]+ (.+?)[0-9.-]/' , $file2 , $m ) ) $spec2files[trim(preg_replace('/\s*\(.+?\)\s*/',' ',ucfirst(trim($m[1]))))][] = $file ;
	else $spec2files['_'][] = $file ;
}

print "<p>Files from BioAcoustica, matched to Wikidata species.</p>" ;
print "<p>See also <a href='https://tools.wmflabs.org/fist/file_candidates/#/candidates/?group=TAXON%20AUDIO'>File Candidates taxon audio</a></p>" ;

print "<table class='table table-condensed table-striped'>" ;
print "<thead><tr><th>Species</th><th>Wikidata item</th><th>Has audio</th><th>Files</th></tr></thead>" ;
print "<tbody>" ;

ksort ( $spec2files ) ;
foreach ( $spec2files AS $species => $files ) {
	$q = '' ;
	$has_audio = '' ;
	if ( $species == '_' ) {
		$species = '<i>Unidentified species</i>' ;
	} else {
		$sparql = "SELECT ?q ?audio { ?q wdt:P225 '$species' OPTIONAL { ?q wdt:P51 ?audio } }" ;
		$j = getSPARQL ( $sparql ) ;
		if ( count($j->results->bindings) == 1 ) {
			$i = $j->results->bindings[0] ;
			$q = preg_replace ( '/^.+\//' , '' , $i->q->value ) ;
			if ( isset($i->audio) ) $has_audio = 'YES' ;
//			print_r ( $i ) ; exit ( 0 ) ;
		}
	}
	
	print "<tr><td>$species</td><td>" ;
	if ( $q != '' ) print "<a href='https://www.wikidata.org/wiki/$q' target='_blank'>$q</a>" ;
	print "</td><td>$has_audio</td><td style='font-size:8pt;white-space:nowrap'>" . str_replace('_',' ',implode('<br/>',$files)) . "</td></tr>" ;
}

print "</tbody></table>" ;

#print "<pre>" ; print_r ( $spec2files ) ; print "</pre>" ;

?>