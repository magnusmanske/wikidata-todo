<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');

require_once ( 'php/ToolforgeCommon.php' ) ;

function from_json($j) {
	$ret = [];
	foreach ($j->pages as $page) {
	 	if ( isset($page->metadata) and isset($page->metadata->coordinates) ) {
	 		list($lat,$lon) = explode('/',$page->metadata->coordinates);
	 		$ret[$page->page_title] = ['lat'=>$lat*1,'lon'=>$lon*1];
	 	}
	}
	return $ret;
}

function distance_in_meters($latitudeFrom, $longitudeFrom, $latitudeTo,  $longitudeTo) {
	$long1 = deg2rad($longitudeFrom);
	$long2 = deg2rad($longitudeTo);
	$lat1 = deg2rad($latitudeFrom);
	$lat2 = deg2rad($latitudeTo);
	 
	//Haversine Formula
	$dlong = $long2 - $long1;
	$dlati = $lat2 - $lat1;
	 
	$val = pow(sin($dlati/2),2)+cos($lat1)*cos($lat2)*pow(sin($dlong/2),2);
	$res = 2 * asin(sqrt($val));
	$radius = 6371; # km
	return abs(($res*$radius)*1000); # meters
}

$tfc = new ToolforgeCommon();

print $tfc->getCommonHeader ('Images near items' ) ;

$category = trim($tfc->getRequest("category",""));
$depth = $tfc->getRequest("depth","5")*1;
$p31 = trim($tfc->getRequest("p31",""));
$max_distance_m = $tfc->getRequest("max_distance_m","150")*1;


print <<<HTML
<div class="lead">
This item can load a category tree of Commons images with coodrinates, and then find Wikidata items of a certain P31 with coordinates but without images.
Then, it will try to find nearby (<{$max_distance_m} meters) pairs.
</div>
<form method="get" class="form">
<div class="form-group row">
	<label class="col-sm-2 col-form-label">Root category:</label>
	<div class="col-sm-6">
		<input type="text" name="category" value="{$category}" placeholder="K8 Telephone booths" />
	</div>
	<label class="col-sm-1 col-form-label">Depth:</label>
	<div class="col-sm-3">
		<input type="number" name="depth" value="{$depth}" />
	</div>
</div>
<div class="form-group row">
	<label class="col-sm-2 col-form-label">P31:</label>
	<div class="col-sm-10">
		<input type="text" name="p31" value="{$p31}" style="width:auto;" placeholder="Q712868" />
	</div>
</div>
<div class="form-group row">
	<label class="col-sm-2 col-form-label">Max distance:</label>
	<div class="col-sm-2">
		<input type="number" name="max_distance_m" value="{$max_distance_m}" />
	</div>
	<div class="col-sm-1">meters</div>
</div>
<input type="submit" value="Do it" class="btn btn-outline-primary" />
(<a href="https://wikidata-todo.toolforge.org/images_near_items.php?category=Telephone+booths+in+the+United+Kingdom&depth=5&p31=Q712868&max_distance_m=50">Example: UK red telephone booths</a>)
</div>
</form>
HTML;


if ( $category!='' and $p31!='' ) {
	# Load images with category
	$c = urlencode($category);
	$url = "https://petscan.wmflabs.org/?psid=25416248&categories={$c}&depth={$depth}&output_compatability=quick-intersection&format=json&doit=";
	$commons = json_decode(file_get_contents($url));
	$images = from_json($commons);
	unset($commons);

	# Load items without image
	$sparql = "SELECT ?q ?coords { ?q wdt:P31/wdt:P279* wd:{$p31} ; wdt:P625 ?coords MINUS { ?q wdt:P18 [] } }";
	if ( true ) { # Use just SPARQL without PetScan
		$items = [];
		foreach ( $tfc->getSPARQL_TSV($sparql) as $j ) {
			$q = $tfc->parseItemFromURL($j['q']);
			if ( preg_match('|^Point\((\S+) (\S+)\)$|',$j['coords'],$m) ) {
				$items[$q] = ['lon'=>$m[1]*1,'lat'=>$m[2]*1];
			}
		}
	} else { # Use PetScan
		$sparql = urlencode($sparql);
		$url = "https://petscan.wmflabs.org/?psid=25416152&output_compatability=quick-intersection&format=json&doit=";
		$wd = json_decode(file_get_contents($url));
		$items = from_json($wd);
		unset($wd);
	}

	$found = 0 ;
	print "<ul>";
	foreach ( $images AS $image_title => $image_coordinates ) {
		foreach ( $items AS $q => $item_coordinates ) {
			$distance_m = distance_in_meters($image_coordinates['lat'],$image_coordinates['lon'],$item_coordinates['lat'],$item_coordinates['lon']);
			$distance_m = round($distance_m);
			if ( $distance_m > $max_distance_m ) continue;
			$image_title_pretty = str_replace('_',' ',$image_title);
			print <<<ROW
			<li>
				Image
				<a href="https://commons.wikimedia.org/wiki/File:{$image_title}" target='_blank'>{$image_title_pretty}</a>
				was taken {$distance_m} meters from the item
				<a href="https://www.wikidata.org/wiki/{$q}" target="_blank">{$q}</a>
			</li>
			ROW;
			$found++ ;
		}
	}
	print "</ul>";
	print "<div>{$found} pairs found.</div>";
}


print $tfc->getCommonFooter() ;

?>