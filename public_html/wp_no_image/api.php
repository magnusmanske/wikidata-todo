<?PHP

header('Content-type: text/plain; charset=UTF-8');

require_once ( '../php/common.php' ) ;

$wiki = get_request ( 'wiki' , '' ) ;
$page = get_request ( 'page' , '' ) ;

if ( $wiki == '' or $page == '' ) exit(0);

$dir = '/data/project/wikidata-todo/public_html/wp_no_image' ;

$filename = "{$dir}/{$wiki}.has_image" ;
file_put_contents($filename, "{$page}\n", FILE_APPEND);

?>