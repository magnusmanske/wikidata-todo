#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR); // E_ALL|

require_once ( '../../public_html/php/common.php' ) ;


$dbu = openToolDB ( 'duplicity_p' ) ;

$sql = "select * from no_wd where wiki='specieswiki'" ;

$fh = fopen ( 'type2page.species.tab' , 'w' ) ;
if(!$result = $dbu->query($sql)) die('There was an error running the query [' . $dbu->error . ']');
while($o = $result->fetch_object()){
	if ( !preg_match('/^[A-Z][a-z]{3,} [a-z]{3,}$/',$o->title) ) continue ;
	$t = $o->title ;
	$h = file_get_contents ( 'https://species.wikimedia.org/wiki/'.$t ) ;
	$h = preg_replace ( '/<[\/]{0,1}[iI]>/' , '' , $h ) ;
//	print "Checking $t\n" ;
	if ( !preg_match ( '/\n([A-Za-z ]+?)\s*:\s*<strong class="selflink">(.+?)<\/strong>/' , $h , $m ) ) continue ;
	if ( strtolower($m[2]) != strtolower($t) ) continue ;
	$type = strtolower ( $m[1] ) ;
	if ( $type != 'species' ) continue ;
	fwrite ( $fh , "$type\t$t\n" ) ;
}
fclose ( $fh ) ;

?>