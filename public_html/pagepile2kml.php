<?PHP

require_once ( 'php/common.php' ) ;
require_once ( 'php/ToolforgeCommon.php' ) ;
require_once ( '/data/project/pagepile/public_html/pagepile.php' ) ;
require_once ( 'php/wikidata.php' ) ;

$tfc = new ToolforgeCommon ( 'sparql_rc' ) ;

function fixXML ( $s ) {
	$doc = new DOMDocument();
	$fragment = $doc->createDocumentFragment();
	$fragment->appendXML($s);
	return $doc->saveXML($fragment) ;
//	return htmlentities($s,ENT_DISALLOWED) ;
}

function renderItem ( $i , $claim ) {
	$dv = $claim->mainsnak->datavalue->value ;
	$q = $i->getQ() ;
	print "<Placemark id='$q'>\n" ;
	print "<name>" . fixXML($i->getLabel()) . "</name>\n" ;
	print "<Point>\n" ;
	print "<coordinates>" . $dv->longitude . "," . $dv->latitude . ",0</coordinates>\n" ;
//	print "<styleUrl>#msn_ylw-pushpin1</styleUrl>" ;
	print "</Point>\n" ;
	print "<Description>   <![CDATA[ <a href='https://www.wikidata.org/wiki/$q'>$q</a> ]]> </Description>\n" ;
	print "</Placemark>\n" ;
}

$pagepile = trim ( $tfc->getRequest ( 'pagepile' , '' ) ) ;

if ( $pagepile == '' ) {
	print $tfc->getCommonHeader ( '' , 'PagePile2KML' ) ;
	print "<p>Generate KML:<br/><form method='get' action='?' class='form-inline'>
	PagePile <input type='number' name='pagepile' placeholder='ID' />
	<input type='submit' value='Generate KML' class='btn btn-primary' />
	</form>
	Exapmple: #<a href='?pagepile=253'>253</a></p>
	<p>See <a href='/pagepile'>PagePile</a> for further information.</p>" ;
	print $tfc->getCommonFooter() ;
	exit ( 0 ) ;
}



if(0) header('Content-type: text/plain; charset=UTF-8');
else header('Content-type: application/xml; charset=UTF-8');
header("Cache-Control: no-cache, must-revalidate");

$pp = new PagePile ( $pagepile ) ;
$pp = $pp->getAsWikidata() ; // It's the only way to be sure!

?>
<?xml version="1.0" encoding="UTF-8"?>
<kml xmlns="http://www.opengis.net/kml/2.2" xmlns:kml="http://www.opengis.net/kml/2.2" xmlns:atom="http://www.w3.org/2005/Atom">
<Document>
<open>1</open>

<?PHP

print "<name>PagePile #$pagepile</name>\n" ;

$qcoords = array() ;
$pp->getAsBatches ( function ( $batch , $pp ) use ( $qcoords ) {
	$qs = array() ;
	foreach ( $batch AS $b ) {
		if ( $b->ns != 0 ) continue ;
		$qs[] = $b->page ;
	}

	$wdl = new WikidataItemList ;
	$wdl->loadItems ( $qs ) ;
	foreach ( $qs AS $q ) {
		if ( !$wdl->hasItem($q) ) continue ;
		$i = $wdl->getItem($q) ;
		$claims = $i->getClaims ( 'P625' ) ;
		if ( count($claims) == 0 ) continue ;
		renderItem ( $i , $claims[0] ) ;
	}
	
	
} , 50 ) ;

?>

</Document>
</kml>
