<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');
require_once ( '/data/project/wikidata-todo/scripts/gnd/ImportFromGND.php' ) ;

function fin ( $status = 'OK' ) {
	global $gnd , $out ;
	$out['status'] = $status ;
	$callback = $gnd->tfc->getRequest('callback','') ;
	if ( $callback!='' ) print "{$callback}(" ;
	print json_encode($out) ;
	if ( $callback!='' ) print ");" ;
	exit(0);
}

$gnd = new ImportFromGND();
$action = $gnd->tfc->getRequest('action','') ;
$item = $gnd->tfc->getRequest('item','') ;
$gnd_id = $gnd->tfc->getRequest('gnd','') ;
$out = [] ;

if ( $action == 'get_qs') {
	if ( $item == '' and $gnd_id == '' ) fin("item or gnd required") ;
	try {
		if ( $item!='') $gnd->load_from_wikidata_item ( $item ) ;
		else $gnd->load_from_gnd($gnd_id) ;
		$out['qs'] = $gnd->getStatements() ;
		//if ( $action == 'run' ) $out['q'] = $gnd->runQuickStatements($out['qs']) ;
	} catch(Exception $e) {
		fin($e->getMessage());
	}
} else fin('Missing or invalid action') ;

fin();

?>