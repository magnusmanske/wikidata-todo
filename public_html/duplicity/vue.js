'use strict';

let router ;
let app ;
let wd = new WikiData() ;

let subjects = {} ;

function compute_server(wiki) {
    if ( wiki=="specieswiki" ) return "species.m.wikimedia.org";
    let m = wiki.match(/^(.+)wiki$/);
    if ( m!=null ) return m[1]+".m.wikipedia.org";
    m = wiki.match(/^(.+)(wik.+)$/);
    if ( m!=null ) return m[1]+".m."+m[2]+".org";
}

function compute_server_desktop(wiki) {
    let ret = compute_server(wiki).replace('.m.','.');
    return ret;
}


$(document).ready ( function () {

    vue_components.toolname = 'duplicity' ;
//    vue_components.components_base_url = 'https://tools.wmflabs.org/magnustools/resources/vue/' ; // For testing; turn off to use tools-static
    Promise.all ( [
        vue_components.loadComponents ( ['wd-date','wd-link','tool-translate','tool-navbar','commons-thumbnail','widar','autodesc','typeahead-search','value-validator',
            'vue_components/main-page.html',
            'vue_components/article-page.html',
            'vue_components/list-page.html',
            'vue_components/stats-page.html',
            'vue_components/categories-page.html',
            ] )
    ] )
    .then ( () => {
    	widar_api_url = "./api.php";
		const routes = [
	        { path: '/', component: MainPage , props:true },
	        { path: '/article/:wiki', component: ArticlePage , props:true },
	        { path: '/article/:wiki/:fixed_page', component: ArticlePage , props:true },
	        { path: '/list/:wiki', component: ListPage , props:true },
	        { path: '/list/:wiki/:category', component: ListPage , props:true },
	        { path: '/stats/:wiki', component: StatsPage , props:true },
	        { path: '/categories/:wiki', component: CategoriesPage , props:true },
	        // { path: '/subject/:subject/:q', component: SubjectPage , props:true },
	        // { path: '/shex/:e', component: ShexPage , props:true },
	      ] ;
		router = new VueRouter({routes}) ;
		app = new Vue ( { router } ) .$mount('#app') ;
		$('#help_page').attr('href','https://www.wikidata.org/wiki/Wikidata:Duplicity');
    } ) ;
} ) ;
