#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR); // E_ALL|

require_once ( '../../public_html/php/common.php' ) ;

$fh = fopen ( "taxa.tab" , 'w' ) ;

$db = openDB ( 'wikidata' , 'wikidata' , true ) ;
$dbws = openDB ( 'en' , 'wikispecies' , true ) ;

$hadthat = array() ;
$sql = "select ips_site_page from wb_items_per_site where ips_site_id='specieswiki'" ;
if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
while($o = $result->fetch_object()){
	$hadthat[$o->ips_site_page] = $o->ips_site_page ;
}

$sql = "select distinct page_title from page,templatelinks where tl_from=page_id and page_namespace=0 and tl_title IN ('Animalia','Plantae','Fungi','Protista','Eukaryota','Virus','Bacteria','Archaea','Taxonav')" ;
if(!$result = $dbws->query($sql)) die('There was an error running the query [' . $dbws->error . ']');
while($o = $result->fetch_object()){
	$t = str_replace ( '_' , ' ' , $o->page_title ) ;
	if ( isset($hadthat[$t]) ) continue ;
	if ( preg_match ( '/unassigned/i' , $t ) ) continue ;
	fwrite ( $fh , "$t\n" ) ;
}

fclose ( $fh ) ;

?>