<?PHP

require_once ( 'php/common.php' ) ;

die ( "DEACTIVATED" ) ;

$wiki = get_request ( 'wiki' , '' ) ;

if ( !isset ( $_REQUEST['run'] ) ) {
	print get_common_header ( '' , 'Random Wikidata item without "instance of"' ) ;
	print "<div class='lead'>To get redirected to a random Wikidata item without \"instance of\" (P31) or \"subclass of\" (P279):" ;
	print "<p><a href='//tools.wmflabs.org/wikidata-todo/random_item_without_instance.php?run=1'>http://tools.wmflabs.org/wikidata-todo/random_item_without_instance.php?run=1</a></p>" ;
	print "To ensure that the item has a link to a Wiki(m|p)edia, e.g. \"enwiki\":" ;
	print "<p><a href='//tools.wmflabs.org/wikidata-todo/random_item_without_instance.php?run=1'>http://tools.wmflabs.org/wikidata-todo/random_item_without_instance.php?run=1&wiki=enwiki</a></p>" ;
	print "<hr/><i>Tip:</i> Drag one of the above links into your bookmarks for easy \"new random item\"; you can subsequently change the wiki by editing the bookmark." ;
	print "</div>" ;
	print get_common_footer() ;
	exit ( 0 ) ;
}

header('Content-type: text/html');

$db = openDB ( 'wikidata' , '' ) ;

$r = rand() /  getrandmax() ;

$sql = 'select * from page where page_namespace=0 and not exists ( select * from pagelinks,linktarget where pl_target_id=lt_id AND page_id=pl_from and lt_namespace=120 and lt_title IN ("P31","P279") limit 1) and page_random>=' . $r ;
if ( $wiki != '' ) {
	$wiki = $db->real_escape_string ( $wiki ) ;
	$sql .= " and exists (select * from wb_items_per_site where ips_site_id='$wiki' and ips_item_id=substr(page_title,1) limit 1)" ;
}
$sql .= ' order by page_random limit 1' ;
$result = getSQL ( $db , $sql ) ;
while($r = $result->fetch_object()){
	print "<html><head><meta http-equiv='refresh' content='0; url=//www.wikidata.org/wiki/" . $r->page_title . "' /></head></html>" ;
}


?>