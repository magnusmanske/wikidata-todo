<?PHP

require_once ( '/data/project/wikidata-todo/public_html/php/ToolforgeCommon.php' ) ;
require_once ( '/data/project/wikidata-todo/public_html/php/wikidata.php' ) ;
require_once ( '/data/project/quickstatements/public_html/quickstatements.php' ) ;

class ImportFromGND {
	public $id ;
	public $name = '' ;
	public $desc = '' ;
	public $aliases = [] ;
	public $dates ;
	public $statements_item = [] ;
	public $statements_dates = [] ;
	public $item ;
	public $item_id ;
	public $tfc ;
	public $wil ;
	private $label_languages = ['de','en','fr','it','es','nl'] ;
	private $conf_file = '/data/project/wikidata-todo/reinheitsgebot.conf' ;
	private $wikidata_search_results = [] ;
	private $code4 = [
		'https://d-nb.info/standards/elementset/gnd#placeOfBirth' => 'P19' ,
		'https://d-nb.info/standards/elementset/gnd#placeOfDeath' => 'P20' ,
		'https://d-nb.info/standards/elementset/gnd#professionOrOccupation' => 'P106',
		'https://d-nb.info/standards/elementset/gnd#fieldOfStudy' => 'P101',
		'https://d-nb.info/standards/elementset/gnd#placeOfActivity' => 'P937',
		'https://d-nb.info/standards/elementset/gnd#familialRelationship' => 'family',
	] ;
	private $code9 = [
		'family' => [
			'v:Vater' => 'P22' ,
			'v:Mutter' => 'P25' ,
			'v:Sohn' => 'P40' ,
			'v:Tochter' => 'P40' ,
			'v:Bruder' => 'P3373' ,
			'v:Schwester' => 'P3373' ,
			'v:.*?Ehefrau' => 'P26' ,
			'v:.*?Ehemann' => 'P26' ,
		]
	] ;

	function __construct () {
		$this->tfc = new ToolforgeCommon ;
		$this->tfc->getQS ( 'wikidata-todo' , $this->conf_file ) ;
		$this->wil = new WikidataItemList ;
	}

	public function load_from_gnd ( $gnd_id ) {
		$this->id = $gnd_id ;
		$this->item_id = $this->getItemForGND($this->id) ;
		if ( isset($this->item_id) ) {
			$this->wil->loadItem($this->item_id) ;
			$this->item = $this->wil->getItem($this->item_id) ;
		}
		$this->load_gnd_xml() ;
	}

	public function load_from_wikidata_item ( $q ) {
		$this->item_id = strtoupper($q) ;
		$this->wil->loadItem($this->item_id) ;
		$this->item = $this->wil->getItem($q) ;
		$gnd_id = '' ;
		if ( isset($this->item) ) $gnd_id = $this->item->getFirstString('P227');
		if ( $gnd_id=='' ) throw new Exception("Item {$q} does not have a GND ID") ;
		$this->id = $gnd_id ;
		$this->load_gnd_xml() ;
	}

	protected function load_gnd_xml() {
		$url = $this->getUrlMARC21XML($this->id) ;
		$xml = $this->load_xml($url);
		$doc = simplexml_load_string($xml) ;
		$this->parseXML($doc);
	}

	public function load_xml ( $url , $attempts_left = 5 ) {
		$again = false ;
		try {
			$xml = @file_get_contents($url) ;
			if ( !isset($xml) or $xml=='' ) $again = true ;
		} catch(Exception $e) {
			$again = true ;
		}
		if ( !$again ) return $xml ;
		if ( $attempts_left == 0 ) return ;
		sleep(2);
		return $this->load_xml($url,$attempts_left-1) ;
	}

	protected function getUrlMARC21XML($gnd) {
		return "https://d-nb.info/gnd/{$gnd}/about/marcxml" ;
	}

	protected function parseName($s) {
		$s = (string) $s ;

		# Last, First => First Last
		if ( preg_match ( '/^(.+?), (.+), (der|die) ([^,]+)$/' , $s , $m ) ) {
			$s = $m[2]." ".$m[1]." ".$m[3].$m[4] ; # "der Jüngere" etc
		} else {
			$s = preg_replace ( '|^(.+?), (.+)$|' , '$2 $1' , $s ) ;
		}

		$s = preg_replace ( '|- +|' , '-' , $s ) ;

		$s = preg_replace ( '|\s{2,}|' , ' ' , $s ) ;

		$s = trim ( $s ) ;
		return $s ;
	}

	protected function parseDate($s) {
		$s = (string) $s ;
		return $s ;
	}

	private function extendSearchQuery ( $query , $property = '' , $value = '' ) {
		$query = trim ( $query ) ;
		if ( $property != '' and $value != '' ) {
			if ( preg_match ( '/ /' , $value ) ) $value = '"' . $value . '"' ;
			$query = trim("{$query} haswbstatement:{$property}={$value}") ;
		}
		return $query ;
	}

	public function getCachedWikidataSearch ( $query , $property = '' , $value = '' ) {
		$query = $this->extendSearchQuery ( $query , $property , $value ) ;
		if ( isset($this->wikidata_search_results[$query]) ) return $this->wikidata_search_results[$query] ;
		$items = [] ;
		$url = "https://www.wikidata.org/w/api.php?format=json&action=query&list=search&srnamespace=0&srsearch=".urlencode($query) ;
		$j = json_decode(file_get_contents($url)) ;
		if ( isset($j->query) and isset($j->query->search) ) {
			foreach ( $j->query->search as $result ) $items[] = $result->title ;
		}
		$this->wikidata_search_results[$query] = $items ;
		return $items ;
	}

	public function getItemForGND($gnd,$check_multiple=false) {
		if ( !isset($gnd) or $gnd=='' ) return ;
		$items = $this->getCachedWikidataSearch('','P227',$gnd) ;
		if ( count($items) == 1 ) {
			if ( $items[0] == 'Q955464' ) return 'Q152002' ; # Parson => pastor
			return $items[0] ;
		}
		if ( count($items) == 0 ) $this->logMissingItem($gnd) ;
		if ( $check_multiple ) return 'multiple' ;
	}

	protected function logMissingItem($gnd) {
		throw new Exception("NO ITEM FOR GND {$gnd}") ;
	}

	protected function parseUrlGND($url) {
		if ( preg_match('|https{0,1}://d-nb.info/gnd/([^\?\#]+)|',$url,$m) ) return $m[1] ;
	}

	protected function setDate($prop,$s) {
		if ( $s == '' ) return ;
		if ( preg_match('|^(\d{1,2})\.(\d{1,2})\.(\d{3,4})$|',$s,$m) ) $s = "+{$m[3]}-{$m[2]}-{$m[1]}T00:00:00Z/11" ;
		else if ( preg_match('|^(\d{1,2})\.(\d{3,4})$|',$s,$m) ) $s = "+{$m[2]}-{$m[1]}-01T00:00:00Z/10" ;
		else if ( preg_match('|^(\d{3,4})$|',$s,$m) ) $s = "+{$m[1]}-01-01T00:00:00Z/09" ;
		else return ;
		$this->statements_dates[$prop] = $s ;
	}

	protected function parseXML($doc) {
		$attributes = '@attributes' ;
		foreach ( ($doc->datafield??[]) AS $x ) {
			$tag = ($x->attributes()->tag??0)*1 ;
			if ( $tag == 100 ) { # Name
				foreach($x->subfield AS $s){
					if ( $s->attributes()->code=='a') $this->name = $this->parseName($s[0]);
					if ( $s->attributes()->code=='d') $this->dates = $this->parseDate($s[0]);
				}
			}
			if ( $tag == 43 ) { # Country
				$country_codes = [] ;
				foreach($x->subfield AS $s){
					if ( $s->attributes()->code=='c' ) {
						$country_codes[] = (string)$s ;
					}
				}
				foreach ( $country_codes AS $c ) {
					if ( preg_match('|^XA-(.+)$|',$c,$m) ) {
						$items = $this->getCachedWikidataSearch('','P297',$m[1]) ;
						if ( count($items) == 1 ) $this->statements_item[] = ['P27',$items[0]];
					}
				}
			}
			if ( $tag == 75 ) { # Type of item
				foreach($x->subfield AS $s){
					if ( $s->attributes()->code=='b' ) {
						$item_type = (string)$s ;
						if ( $item_type == 'piz' ) $this->statements_item[] = ['P31','Q5'];
					}
				}
			}
			/*
			# Deactivated as per https://www.wikidata.org/wiki/Topic:X83gj12k9agm6ccn
			if ( $tag == 400 ) { # Aliases
				foreach($x->subfield AS $s){
					if ( $s->attributes()->code!='a') continue ;
					$alias = $this->parseName($s[0]);
					$this->aliases[$alias] = $alias ;
				}
			}
			*/
			if ( $tag == 548 ) { # Dates
				$first = '' ;
				$second = '' ;
				$code4 = '' ;
				foreach($x->subfield AS $s) {
					if ( $s->attributes()->code=='4' ) {
						if ( preg_match('|^https:|',$s) ) $code4 = (string) $s ;
					}
					if ( $s->attributes()->code!='a' ) continue ;
					if ( !preg_match('|^(.*)-(.*)$|',$s,$m) ) continue ;
					$first = $m[1] ;
					$second = $m[2] ;
				}
				if ( $code4 == 'https://d-nb.info/standards/elementset/gnd#dateOfBirthAndDeath' ) {
					$this->setDate('P569',$first) ;
					$this->setDate('P570',$second) ;
				}
				if ( $code4 == 'https://d-nb.info/standards/elementset/gnd#periodOfActivity' ) {
					$this->setDate('P2031',$first) ;
					$this->setDate('P2032',$second) ;
				}
			}
			if ( $tag == 375 ) { # Gender
				foreach($x->subfield AS $s) {
					if ( $s->attributes()->code=='a' ) {
						$sex = (string) $s ;
						if ( $sex == '1' ) $this->statements_item[] = ['P21','Q6581097'];
						if ( $sex == '2' ) $this->statements_item[] = ['P21','Q6581072'];
					}
				}
			}
			if ( $tag == 670 ) { # Homepage
				foreach($x->subfield AS $s){
					if ( $s->attributes()->code=='u' ) {
						$url = (string) $s ;
						$this->statements_item[] = ['P856','"'.$url.'"'] ;
					}
				}
			}
			if ( $tag == 678 ) { # Description
				foreach($x->subfield AS $s) $this->desc = trim((string) $s) ;
			}
			if ( in_array($tag, [500,550,551]) ) { # GND internal links
				$prop = '' ;
				unset($target_q) ;
				$code9 = '' ;
				foreach($x->subfield AS $s){
					if ( $s->attributes()->code=='4' and $prop=='' ) {
						$prop = $this->code4[(string)$s]??'' ;
					}
					if ( $s->attributes()->code=='0' and !isset($target_q) ) {
						$target_gnd = $this->parseUrlGND((string)$s) ;
						try {
							$target_q = $this->getItemForGND ( $target_gnd ) ;
						} catch(Exception $e) {
							# Ignore
						}
					}
					if ( $s->attributes()->code=='9' ) {
						$code9 = (string) $s ;
					}
				}
				if ( isset($this->code9[$prop]) ) {
					foreach ( $this->code9[$prop] AS $pattern => $new_prop ) {
						$p = "|^{$pattern}$|" ;
						if ( preg_match($p,$code9) ) $prop = $new_prop ;
					}
				}
				if ( !preg_match('|^P\d+$|',$prop) ) continue ;
				if ( isset($target_q) ) $this->statements_item[] = [$prop,$target_q] ;
			}
		}
	}

	protected function getNow() {
		if ( isset($this->now) ) return $this->now ;
		$this->now = date ( '+Y-m-d' ) . 'T00:00:00Z/11' ;
		return $this->now ;
	}

	protected function addOwnReference(&$statements) {
		foreach ( $statements AS $k => $s ) {
			if ( count($s) != 3 ) continue ;
			if ( $s[1] == 'P227' ) continue ; # Avoid self-reference
			if ( !preg_match('|^P\d+$|',$s[1]) ) continue ; # Only for statements
			$s[] = 'S227' ;
			$s[] = $this->quote($this->id) ;
			$s[] = 'S248' ;
			$s[] = "Q23833686" ;
			$s[] = 'S813' ;
			$s[] = $this->getNow() ;
			$statements[$k] = $s ;
		}
	}

	protected function hasReferenceToGND ( $claim ) {
		foreach ( ($claim->references??[]) AS $ref_group ) {
			if ( !isset($ref_group->snaks) ) continue ;
			if ( !isset($ref_group->snaks->P227) ) continue ;
			foreach ( $ref_group->snaks->P227 AS $snak ) {
				if ( !isset($snak->datavalue) ) continue ;
				if ( ($snak->datavalue->value??'') == $this->id ) return true ;
			}
		}
		return false ;
	}

	protected function filterStatements($commands) {
		if ( !isset($this->item) ) return $commands ; # Can't filter
		$ret = [] ;
		foreach ( $commands AS $c ) {
			if ( count($c) != 3 ) {
				$ret[] = $c ;
				continue ;
			}
			if ( preg_match('/^(L|A|D)/',$c[1]) ) { # Bad names
				if ( preg_match('/(\.\.\.)/',$c[2]) ) continue ;
			}
			if ( preg_match('|^L(.+)$|',$c[1],$m) ) {
				if ( $this->item->hasLabelInLanguage($m[1]) ) continue ;
			}
			if ( preg_match('|^D(.+)$|',$c[1],$m) ) {
				if ( $this->item->hasDescriptionInLanguage($m[1]) ) continue ;
			}
			if ( preg_match('|^A(.+)$|',$c[1],$m) ) {
				$language = $m[1] ;
				$alias = preg_replace('|^"(.+)"$|','$1',$c[2]) ;
				$aliases = $this->item->getAliases($language) ;
				if ( in_array($alias,$aliases) ) continue ;
				if ( $alias == $this->item->getLabel($language,true) ) continue ;
			}
			$found = false ;
			if ( preg_match('|^P\d+$|',$c[1]) ) {
				if ( preg_match('|^Q\d+$|',$c[2]) ) { # Item
					$claims = $this->item->getClaims($c[1]) ;
					foreach ( $claims AS $claim ) {
						if ( $this->item->getTarget($claim) != $c[2] ) continue ;
						if ( $this->hasReferenceToGND($claim) ) $found = true ;
					}
				} else if ( preg_match('|^"(.+)"$|',$c[2],$m) ) { # String
					if ( $this->hasExternalID($c[1],$m[1]) ) $found = true ;
				} else if ( preg_match('|^(\+.+)/(\d+)$|',$c[2],$m) ) { # Date
					$time_value = $m[1] ;
					$precision = $m[2] * 1 ;
					$claims = $this->item->getClaims($c[1]) ;
					foreach ( $claims AS $claim ) {
						if ( $claim->mainsnak->datavalue->value->time != $time_value ) continue ;
						if ( $claim->mainsnak->datavalue->value->precision != $precision ) continue ;
						if ( $this->hasReferenceToGND($claim) ) $found = true ;
					}
				}
			}
			if ( $found ) continue ;
			$ret[] = $c ;
		}
		return $ret ;
	}

	public function quote($s) {
		$s = trim($s) ;
		if ( strlen($s) > 250 ) {
			$s = substr ( $s , 0 , 250 ) .'...' ;
		}
		$s = "\"{$s}\"" ;
		return $s ;
	}

	protected function hasLabel ( $language ) {
		if ( !isset($this->item) ) return false ;
		return $this->item->hasLabelInLanguage($language);
	}

	protected function hasDescription ( $language ) {
		if ( !isset($this->item) ) return false ;
		return $this->item->hasDescriptionInLanguage($language);
	}

	protected function hasAlias ( $language , $label ) {
		if ( !isset($this->item) ) return false ;
		$aliases = $this->item->getAliases($language);
		return in_array(trim($label), $aliases) ;
	}

	protected function hasExternalID ( $prop , $value = '' ) {
		if ( !isset($this->item) ) return false ;
		if ( $value == '' ) return $this->item->hasClaims($prop) ;
		$values = $this->item->getStrings($prop);
		return in_array($value,$values);
	}

	protected function hasStatementForProperty ( $prop ) {
		if ( !isset($this->item) ) return false ;
		return $this->item->hasClaims($prop) ;
	}

	public function getStatements() {
		$ret = [] ;
		if ( isset($this->item_id) and $this->item_id!='' ) {
			# We already know which item to update
			$items = [ $this->item_id ] ;
		} else {
			$items = $this->getCachedWikidataSearch('','P227',$this->id) ;
			if ( count($items) > 1 ) {
				throw new Exception("MULTIPLE ITEMS WITH THAT GND (".implode(',',$items)."), NOT PROCESSSING!") ;
			}
		}
		if ( count($items) == 1 ) {
			$item = $items[0] ;
			if ( $item != $this->item_id ) {
				$this->item_id = $item ;
				unset ( $this->item ) ;
			}
			if ( !isset($this->item) ) {
				$this->wil->loadItem($this->item_id) ;
				$this->item = $this->wil->getItem($this->item_id) ;
			}
			if ( !isset($this->item) ) throw new Exception("Failed to load item {$this->item_id}") ;
		}
		else {
			$item = 'LAST' ;
			$ret[] = ['CREATE'] ;
			unset($this->item) ;
			unset($this->item_id) ;
		}

		# Label
		if ( $this->name != '' ) {
			foreach ( $this->label_languages AS $lang ) {
				if ( $this->hasLabel($lang) ) continue ;
				$ret[] = [$item,"L{$lang}",$this->quote($this->name)] ;
			}
		}

		# Aliases
		foreach ( $this->aliases AS $a ) {
			if ( $this->hasAlias('de',$a) ) continue ;
			$ret[] = [$item,"Ade",$this->quote($a)] ;
		}

		# Description
		if ( $this->desc != '' and !$this->hasDescription('de') ) {
			$ret[] = [$item,"Dde",$this->quote($this->desc)] ;
		}

		# GND
		$ret[] = [$item,'P227',$this->quote($this->id)] ;

		# Statements
		foreach ( $this->statements_item AS $si ) {
			$ret[] = [ $item , $si[0] , $si[1] ] ;
		}

		# Dates
		foreach ( $this->statements_dates AS $prop => $value ) {
			if ( $this->hasStatementForProperty($prop) ) continue ;
			$ret[] = [ $item , $prop , $value ] ;
		}

		$ret = $this->filterStatements($ret);
		$this->addOwnReference($ret) ;

		foreach ($ret AS $k => $s ) $ret[$k] = implode("\t",$s) ;
		return $ret ;
	}

	public function runQuickStatements ( $commands ) {
		$q = $this->tfc->runCommandsQS ( $commands ) ;
		if ( isset($this->item_id) ) $q = $this->item_id ;
		return $q ;
	}
}

?>
