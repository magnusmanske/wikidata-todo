<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');
ini_set('memory_limit','500M');

require_once ( '../scripts/wikidata_cache/wikidata_cache.php' ) ;

function get_property_permutations ( $q , $prop2values , $format ) {
	$ret = ["q" => ["type" => "uri","value" => "http://www.wikidata.org/entity/{$q}"]] ;
	$ret = [ $ret ] ;
	foreach ( $prop2values AS $prop => $values ) {
		$nret = [] ;
		foreach ( $values AS $v ) {
			foreach ( $ret AS $r ) {
				$x = $r ;
				$x[$prop] = [ "type" => "literal","value" => $v ] ;
				$nret[] = $x ;
			}
		}
		$ret = $nret ;
	}
	if ( $format == 'json' ) {
		foreach ( $ret AS $k0 => $v0 ) {
			foreach ( $v0 AS $k => $v ) $ret[$k0][$k] = $v['value'] ;
		}
	}
	return $ret ;
}

function flush_batch_ids ( $batch ) {
	global $wc , $format , $use_beacon_target_prop , $beacon_target_prop ;
	global $first_output_row , $all_properties ;
	$out = '' ;
	$beacon_map = [] ;
	$item2values = [] ;
	if ( $format == 'beacon' && $use_beacon_target_prop ) {
		foreach ( $batch AS $id ) $beacon_map["{$id}"] = '' ;
		foreach ( $wc->item_prop_generator($batch,$beacon_target_prop) AS $id_value ) {
			$beacon_map["{$id_value[0]}"] = $id_value[1] ;
		}
	} else if ( $format == 'json_verbose' or $format == 'json' ) {
		foreach ( $all_properties AS $prop ) {
			$prop = strtolower($prop);
			foreach ( $batch AS $item_id ) $item2values["{$item_id}"][$prop] = [] ;
			foreach ( $wc->item_prop_generator($batch,$prop) AS $id_value ) {
				$item2values[$id_value[0]][$prop][] = $id_value[1] ;
			}
		}
	}
	foreach ( $batch AS $q ) {
		if ( $format == 'html' ) {
			$out .= "<div>{$q}</div>" ;
		} else if ( $format == 'beacon' ) {
			if ( $use_beacon_target_prop ) {
				$out .= "Q$q||{$beacon_map[$q]}\n" ;
			} else {
				$out .= "Q{$q}\n" ;
			}
		} else if ( $format == 'json_verbose' or $format == 'json' ) {
			$permutations = get_property_permutations("Q{$q}",$item2values["{$q}"],$format) ;
			foreach ( $permutations AS $perm ) {
				if ( $first_output_row ) $first_output_row = false ;
				else $out .= "," ;
				$out .= json_encode($perm) ;
			}
		}
	}
	print $out ;
	#$wc->tfc->flush();
}

$wc = new WikidataCache ;
$batch_size = 1000 ;
$use_beacon_target_prop = false ;
$first_output_row = true ;

$action = $wc->tfc->getRequest ( 'action' , '' ) ;
$format = $wc->tfc->getRequest ( 'format' , 'html' ) ;
$beacon_target_prop = trim($wc->tfc->getRequest ( 'beacon_target_prop' , '' )) ;
$all_properties  = trim ( $wc->tfc->getRequest ( 'all_properties' , '' ) ) ;
$all_properties = explode ( "\n" , $all_properties ) ;
foreach ( $all_properties AS $k => $v ) $all_properties[$k] = trim($v) ;

if ( $format == 'beacon' ) {
	header('Content-Type: text/plain');
	print "#FORMAT: BEACON\n" ;
	print "#PREFIX: http://www.wikidata.org/entity/\n" ;
	$desc = "Wikidata items that have properties ".implode(' and ',$all_properties) ;
	if ( $beacon_target_prop != '' ) {
		$wil = new \WikidataItemList ;
		$wil->loadItems ( [$beacon_target_prop] ) ;
		$beacon_property_item = $wil->getItem ( $beacon_target_prop ) ;
		if ( isset($beacon_property_item) ) {
			$property_url = $beacon_property_item->getFirstString('P1630') ;
			if ( isset($property_url) ) {
				$url = preg_replace ( '|\$1/*$|' , '' , $property_url ) ;
				print "#TARGET: {$url}\n" ;
				$use_beacon_target_prop = true ;
				$desc .= "; linking to {$beacon_target_prop}" ;
			}
		}
	}
	print "#DESCRIPTION: {$desc}\n" ;
} else if ( $format == 'json_verbose' ) {
	header('Content-Type: application/json');
	print "{\"head\":{\"vars\":[\"q\"" ;
	foreach ( $all_properties AS $p ) print ",\"".strtolower($p)."\"" ;
	print "]},\"results\":{\"bindings\":[" ;
} else if ( $format == 'json' ) {
	header('Content-Type: application/json');
	print "[" ;
} else {
	print $wc->tfc->getCommonHeader ( '' , 'Wikidata cache' ) ;
}

if ( $action == '' ) {
?>

<form method='get' class='form'>

<div style='display:flex'>
	<textarea name='all_properties' placeholder='One property ID (Pxxx) per line' row=2 style='width=5rem'></textarea>
	<button class='btn btn-outline-primary' name='action' value='items_with_properties'>Items with all these properties</button>
</div>
<hr/>

<div style='display:flex'>
	<button class='btn btn-outline-primary' name='action' value='stats'>Show statistics</button>
</div>
<hr/>

<div>
Format: 
<label><input type='radio' name='format' value='html' checked> HTML</label>
<label><input type='radio' name='format' value='beacon'> BEACON</label>
<label><input type='radio' name='format' value='json'> JSON</label>
<label><input type='radio' name='format' value='json_verbose'> JSON (verbose)</label>
</div>

<div>
	Beacon target property
	<input type='text' name='beacon_target_prop' placeholder='Property ID (Pxxx)' />
</div>

</form>

<?PHP
} else if ( $action == 'items_with_properties' ) {
	$batch_ids = [] ;
	foreach ( $wc->items_with_properties_generator ( $all_properties ) AS $item_id ) {
		$batch_ids[] = $item_id ;
		if ( count($batch_ids) >= $batch_size ) {
			flush_batch_ids ( $batch_ids ) ;
			$batch_ids = [] ;
		}
	}
	flush_batch_ids ( $batch_ids ) ;
	$wc->tfc->flush();

	if ( $format == 'json_verbose' ) {
		print "]}}" ;
	} else if ( $format == 'json' ) {
		print "]" ;
	}
} else if ( $action == 'stats' ) {
	$stats = $wc->get_stats();
	print "<table><tbody>" ;
	foreach ( $stats AS $k => $v ) {
		print "<tr><th>{$k}</th>" ;
		print "<td style='text-align:right; font-family:courier'>".number_format($v)."</td></tr>" ;
	}
	print "</tbody></table>" ;
}


?>