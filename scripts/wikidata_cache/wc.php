#!/usr/bin/php
<?PHP

/*
jsub -mem 8g -cwd -once -continuous -N wc_all ./wc.php update_all
jsub -mem 4g -cwd -once -continuous -N wc_rc ./wc.php update_rc
*/

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR); # |E_ALL
ini_set('display_errors', 'On');

require_once ( './wikidata_cache.php' ) ;

$wc = new WikidataCache ;

$action = $argv[1]??'update_rc' ;

if ( $action == 'update_rc' ) {
	while ( true ) $wc->update_from_recent_changes () ;
} else if ( $action == 'update_all' ) {
	while ( true ) $wc->update_all () ;
} else if ( $action == 'auto' ) {
	while ( true ) {
		$wc->update_from_recent_changes () ;
		$wc->update_all () ;
	}
} else if ( $action == 'test' ) {
	$items = $wc->get_items_with_properties ( [214,227] ) ;
	print count($items)."\n" ;
} else {
	die ( "Unknown action '{$action}'\n" ) ;
}

?>