<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');
ini_set('memory_limit','500M');

require_once ( 'php/ToolforgeCommon.php' ) ;
include_once ( "php/common.php" ) ;

function get_stats ( $s ) {
	$ret = (object) array ( 'chars'=>0 ,'present'=>false , 'words'=>0 , 'plain_text'=>'' ) ;
	if ( !isset($s) or trim($s) == '' ) return $ret ;
	if ( preg_match ( '/^<i>/' , $s ) ) return $ret ;

	$s = trim ( strip_tags ( $s ) ) ; // De-HTMLify
	$ret->plain_text = $s ;
	$ret->words = str_word_count ( $s ) ;
	$ret->chars = strlen ( preg_replace ( '/\s/' , '' , $s ) ) ;
	
	if ( $ret->words > 0 ) $ret->present = true ;

//	$ret->s = $s ;
//	print "<pre>" ; print_r ( $ret ) ; print "</pre>" ;
	return $ret ;
}

$tfc = new ToolforgeCommon ( 'test_descriptions' ) ;
$number_of_items = get_request ( 'noi' , 10 ) ; # Max 500
$language = get_request ( 'lang' , 'en' ) ;

$url = "https://www.wikidata.org/w/api.php?action=query&list=random&rnnamespace=0&rnlimit=$number_of_items&rnfilterredir=nonredirects&format=json" ;
$j = json_decode ( file_get_contents ( $url ) ) ;

print get_common_header ( '' , 'Description test' ) ;
print "<style>td.num{font-family:courier;text-align:right;}</style>" ;
print "<p>This tool gets a number of random items from Wikidata, and shows their automatic and manual descriptions, for comparison.</p>" ;
print "<form method='get' class='form form-inline'>
<p>
Language: <select name='lang'>" ;

$languages = array('en','de','vi','nl','pl','fr','sv','fi','el');
foreach ( $languages AS $l ) {
	print "<option value='$l' " . ($language==$l?'selected':'') . ">$l" ;
	if ( $l != 'en' and $l != 'de' ) print ' [partially translated]' ;
	print "</option>" ;
}

print "</select>
Number of items: <input type='number' name='noi' value='$number_of_items' />
<input type='submit' class='btn btn-primary-outline' name='doit' value='Run' />
</p>
</form>" ;

//if ( !isset($_REQUEST['doit']) ) exit ( 0 ) ;

print "<table class='table table-condensed table-striped'>" ;
print "<thead><tr><th>Item</th><th>Automatic</th><th>Manual</th></tr></thead><tbody>" ;

$urls = [] ;
foreach ( $j->query->random AS $x ) {
	$q = $x->title ;
	$urls[$q] = "https://tools.wmflabs.org/autodesc/?q=$q&lang=$language&mode=long&links=wikidata&redlinks=&format=json&get_infobox=&infobox_template=" ;
}
$descs = $tfc->getMultipleURLsInParallel ( $urls , 10 ) ;
#print "!<pre>" ; print_r ( $descs ) ; print "</pre>!" ;

$identical = 0 ;
$stats = [ 'auto'=>['chars'=>0,'words'=>0,'present'=>0] , 'manual'=>['chars'=>0,'words'=>0,'present'=>0] ] ;
foreach ( $descs AS $q => $desc ) {
	$j2 = json_decode ( $desc ) ;
	if ( !is_object($j2) ) $j2 = (object) array ( 'label'=>'ERROR' , 'result'=>'CONNECTION ERROR' , 'manual_description'=>'CONNECTION ERROR' ) ;
	
	print "<tr>" ;
	print "<td><a href='https://www.wikidata.org/wiki/$q' target='_blank'>{$j2->label}</a></td>" ;
	print "<td>{$j2->result}</td>" ;
	print "<td>{$j2->manual_description}</td>" ;
	print "</tr>" ;
	
	// Stats
	$st = array (
		'auto' => get_stats ( $j2->result ) ,
		'manual' => get_stats ( $j2->manual_description )
	) ;
	if ( $st['auto']->plain_text == $st['manual']->plain_text ) $identical++ ;
	foreach ( array('auto','manual') AS $key ) {
		$stats[$key]['chars'] += $st[$key]->chars ;
		$stats[$key]['words'] += $st[$key]->words ;
		if ( $st[$key]->present ) $stats[$key]['present']++ ;
	}
}

print "<tr><th>Description present</th><td class='num'>" . $stats['auto']['present'] . "</td><td class='num'>" . $stats['manual']['present'] . "</td></tr>" ;
print "<tr><th>Words</th><td class='num'>" . $stats['auto']['words'] . "</td><td class='num'>" . $stats['manual']['words'] . "</td></tr>" ;
print "<tr><th>Characters</th><td class='num'>" . $stats['auto']['chars'] . "</td><td class='num'>" . $stats['manual']['chars'] . "</td></tr>" ;
print "<tr><th>Both descriptions identical</th><td colspan=2 class='num'>$identical</td></tr>" ;

print "</tbody></table>" ;

?>