#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR); // E_ALL|

require_once ( '../../public_html/php/common.php' ) ;

$db = openDB ( 'wikidata' , 'wikidata' , true ) ;

$fh = fopen ( "people.create" , 'w' ) ;

$dbws = openDB ( 'en' , 'wikispecies' , true ) ;

$people = array() ;
$cats = array ( 'Taxon Authorities' , 'Biologists' ) ;
foreach ( $cats AS $cat ) {
	$people2 = getPagesInCategory ( $dbws , $cat , 10 , 0 , true ) ;
	foreach ( $people2 AS $k => $v ) $people[$k] = $v ;
}

#$people = array ( 'Laura Codorniú' ) ;

#$people = explode ( "\n" , file_get_contents ( 'people.tab' ) ) ;
foreach ( $people AS $p ) {
	$p = str_replace ( '_' , ' ' , $p ) ;
	
	$skip = false ;
	$sql = "select * from wb_items_per_site WHERE ips_site_id='specieswiki' AND ips_site_page='" . $db->real_escape_string($p) . "' LIMIT 1" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	while($o = $result->fetch_object()) $skip = true ;
	if ( $skip ) continue ;

	if ( preg_match ( '/\(.+\)/' , $p ) ) continue ;
	$p2 = trim ( preg_replace ( '/\b[A-Z]\./' , ' ' , $p ) ) ;
	$p2 = preg_replace ( '/^\s*(\S+)\b.*?\b(\S+)$/' , '\1%\2' , $p2 ) ;
	if ( !preg_match ( '/\%/' , $p2 ) ) continue ; // Last name only
	$p2 = $db->real_escape_string ( $p2 ) ;
	$sql = "select distinct term_entity_id from wb_terms where term_entity_type='item' AND term_type IN ('label','alias') AND term_text LIKE '$p2'" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	$qs = array() ;
	while($o = $result->fetch_object()) $qs[] = $o->term_entity_id ;
	
	if ( count($qs) > 0 ) continue ;
	
	$s = "CREATE\n" ;
	$s .= "LAST\tLen\t\"$p\"\n" ;
	$s .= "LAST\tSspecieswiki\t\"$p\"\n" ;
	$s .= "LAST\tP31\tQ5\n" ;
	
	fwrite ( $fh , $s ) ;
}

fclose ( $fh ) ;

?>