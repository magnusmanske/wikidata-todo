#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR); // E_ALL|

require_once ( '../../public_html/php/common.php' ) ;
require_once ( '../../public_html/php/wikidata.php' ) ;

$wil = new WikidataItemList ;
$db = openDB ( 'wikispecies' , 'wikispecies' ) ;

$fn = '/data/project/wikidata-todo/scripts/wikispecies/vernacular.qs' ;

$langs = array(
	'als' => 'gsw',
	'bat-smg' => 'sgs',
	'be-x-old' => 'be-tarask',
	'bh' => 'bho',
	'fiu-vro' => 'vro',
	'no' => 'nb',
	'qqq' => 'qqq', # Used for message documentation.
	'qqx' => 'qqx', # Used for viewing message keys.
	'roa-rup' => 'rup',
	'simple' => 'en',
	'zh-classical' => 'lzh',
	'zh-min-nan' => 'nan',
	'zh-yue' => 'yue',
);

/*
curl -g -o taxa.wdq 'http://wdq.wmflabs.org/api?q=claim[225]&props=225'
sed 's/,/\n/g' taxa.wdq | grep '"\]' | sed 's/["\]]//g' | sed 's/"//g' > taxa.names
*/

$s = file_get_contents ( 'taxa.names' ) ;
$tx = explode ( "\n" , $s ) ;
unset ( $s ) ;
$taxa = array() ;
foreach ( $tx AS $t ) $taxa[strtolower($t)] = 1 ;
unset($tx) ;

$fp = fopen ( $fn , 'r' ) ;
while(!feof($fp)) {
	$s = fgets ( $fp ) ;
	if ( preg_match ( '/\[/' , $s ) ) continue ; # Paranoia
//	$d = explode ( "\t" , trim($s) ) ;
	if ( !preg_match ( '/^(.+?\t)(\S+?):"(.+)"(.*)$/' , $s , $m ) ) { print $s ; continue ; }
	$tn = strtolower($m[3]) ;
	if ( isset($taxa[$tn]) ) continue ;
	if ( isset($langs[$m[2]]) ) $m[2] = $langs[$m[2]] ;
	$s = $m[1] . $m[2] . ':"' . $m[3] . '"' . $m[4] . "\n" ;
	print $s ;
}
fclose($fp);

?>