<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR);
ini_set('display_errors', 'On');
ini_set('memory_limit','500M');

require_once ( '/data/project/magnustools/public_html/php/Widar.php' ) ;
// require_once ( '../php/ToolforgeCommon.php' ) ;
$tfc = new ToolforgeCommon('duplicity') ;


$action = $tfc->getRequest ( 'action' , '' ) ;

$out = ["status"=>"OK"];
if ( $action == 'random_nowd_page' ) {
	$dbu = $tfc->openDBtool ( 'duplicity_p' ) ;
	$wiki = $tfc->getRequest ( 'wiki' , '' ) ;
	$wiki = $dbu->real_escape_string ( $wiki ) ;
	while ( !isset($out["page"]) ) {
		$r = mt_rand()/mt_getrandmax() ;
		$sql = "SELECT * FROM no_wd WHERE wiki='{$wiki}' AND random>={$r} ORDER BY random LIMIT 1" ;
		$result = $tfc->getSQL ( $dbu , $sql ) ;
		while($o = $result->fetch_object()){
			$out["page"] = $o->title;
			$out["id"] = $o->id;
		}
	}

} else if ( $action == 'invalidate_page' ) {
	$wiki = $tfc->getRequest ( 'wiki' , '' ) ;
	$page = $tfc->getRequest ( 'page' , '' ) ;

	$dbu = $tfc->openDBtool ( 'duplicity_p' ) ;
	$wiki = $dbu->real_escape_string ( $wiki ) ;
	$page = $dbu->real_escape_string ( $page ) ;

	$sql = "DELETE FROM `no_wd` WHERE `wiki`='{$wiki}' AND `title`='{$page}'" ;
	$tfc->getSQL ( $dbu , $sql ) ;

} else if ( $action == 'wikis' ) {
	$dbu = $tfc->openDBtool ( 'duplicity_p' ) ;
	$out['wikis'] = [];
	$sql = "SELECT `wiki`,count(*) AS `cnt` FROM `no_wd` GROUP BY `wiki` ORDER BY `wiki`" ;
	$result = $tfc->getSQL ( $dbu , $sql ) ;
	while($o = $result->fetch_object()) $out['wikis'][] = $o ;

} else if ( $action == 'articles' ) {
	$wiki = $tfc->getRequest ( 'wiki' , '' ) ;
	$category = str_replace(' ','_',$tfc->getRequest ( 'category' , '' ) );


	$dbu = $tfc->openDBtool ( 'duplicity_p' ) ;
	$wiki = $dbu->real_escape_string ( $wiki ) ;
	$category = $dbu->real_escape_string ( $category ) ;

	$articles = [];
	$sql = "SELECT * FROM `no_wd` WHERE `wiki`='{$wiki}' ORDER BY `creation_date`" ;
	$out['sql'] = $sql;
	$result = $tfc->getSQL ( $dbu , $sql ) ;
	while($o = $result->fetch_object()) $articles[] = $o ;

	if ( $category!='' and count($articles)>0 ) {
		$dbw = $tfc->openDBwiki ( $wiki ) ;
		$pages = [];
		foreach ( $articles as $a ) $pages[] = $dbw->real_escape_string(str_replace(' ','_',$a->title));
		$pages = implode ( "','" , $pages );
		$sql = "SELECT DISTINCT `page_title` FROM `page`,`categorylinks` WHERE `page_id`=`cl_from` AND `page_title` IN ('{$pages}') AND `page_namespace`=0 AND `page_is_redirect`=0 AND `cl_to`='{$category}'" ;
		$pages = [];
		$result = $tfc->getSQL ( $dbw , $sql ) ;
		while($o = $result->fetch_object()) $pages[] = str_replace('_',' ',$o->page_title);
		$articles2 = [] ;
		foreach ( $articles as $a ) {
			if ( in_array($a->title, $pages) ) $articles2[] = $a ;
		}
		$articles = $articles2;
	}

	$out['articles'] = $articles;


} else if ( $action == 'stats' ) {
	$out['data'] = [];
	$out['wikis'] = [];
	$out['dates'] = [];
	$wiki = $tfc->getRequest ( 'wiki' , '' ) ;

	$dbu = $tfc->openDBtool ( 'duplicity_p' ) ;
	$wiki = $dbu->real_escape_string ( $wiki ) ;

	$sql = "SELECT * FROM `stats` WHERE `wiki`='{$wiki}'" ;
	$result = $tfc->getSQL ( $dbu , $sql ) ;
	while($o = $result->fetch_object()) {
		$out['data']["{$o->date}"] = $o->count*1 ;
	}

} else if ( $action == 'categories' ) {
	$out['categories'] = [];
	$wiki = $tfc->getRequest ( 'wiki' , '' ) ;
	$min_pages = $tfc->getRequest ( 'min_pages' , '10' ) * 1 ;

	$dbu = $tfc->openDBtool ( 'duplicity_p' ) ;
	$wiki = $dbu->real_escape_string ( $wiki ) ;

	$pages = [] ;
	$sql = "SELECT `title` FROM `no_wd` WHERE `wiki`='{$wiki}'" ;
	$result = $tfc->getSQL ( $dbu , $sql ) ;
	while($o = $result->fetch_object()) $pages[] = $dbu->real_escape_string ( str_replace(' ','_',$o->title) ) ;

	if ( count($pages)>0 ) {
		$pages = implode ( "','" , $pages );
		$dbw = $tfc->openDBwiki ( $wiki ) ;
		$sql = "SELECT `cl_to`,count(*) AS `cnt` FROM `page`,`categorylinks` WHERE `page_id`=`cl_from` AND `page_title` IN ('{$pages}') AND `page_namespace`=0 AND `page_is_redirect`=0 GROUP BY `cl_to` HAVING `cnt`>={$min_pages} ORDER BY `cnt` DESC" ;
		$result = $tfc->getSQL ( $dbw , $sql ) ;
		while($o = $result->fetch_object()) $out['categories'][] = [$o->cl_to,$o->cnt];	
	}

} else {
	$widar = new Widar('wikidata-todo');
	$widar->attempt_verification_auto_forward("/duplicity/#/article/enwiki");
	$widar->authorization_callback = 'https://wikidata-todo.toolforge.org/duplicity/api.php?path=https://wikidata-todo.toolforge.org/duplicity/api.php' ;
	try {
		if ( $widar->render_reponse(true) ) exit(0);
		$out["status"] = "Unknown action: ".$action;
	} catch ( Exception $e ) { # Error
		$out["status"] = $e->getMessage() ;
	}
}

header('Content-Type: application/json');
print json_encode($out);

?>