<?PHP

### THIS ONLY FORWARDS TO THE UPDATED VERSION

#error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR); // 
#ini_set('display_errors', 'On');

require_once ( 'php/common.php' ) ;
$wiki = get_request ( 'wiki' , '' ) ;
$page = get_request ( 'page' , '' ) ;
$mode = get_request ( 'mode' , '' ) ;
$cat = trim ( get_request ( 'cat' , '' ) ) ;

if ( $mode == 'cats' ) $mode = 'categories';

if ( $mode=='' and $page!='' ) $mode = 'article';

// https://tools.wmflabs.org/wikidata-todo/duplicity.php?wiki=enwiki&norand=1&page=Real%5FPro%5FVolleyball

$url = "/duplicity/#/{$mode}" ;
if ( $wiki!='' ) $url .= "/{$wiki}";
if ( $cat!='' ) $url .= "/{$cat}";
if ( $page!='' ) $url .= "/{$page}";

header("Location: {$url}");
exit(0);

?>