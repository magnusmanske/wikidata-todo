#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');
ini_set('memory_limit','500M');

require_once ( '/data/project/quickstatements/public_html/quickstatements.php' ) ;
require_once ( '/data/project/wikidata-todo/public_html/php/ToolforgeCommon.php' ) ;
require_once ( '/data/project/wikidata-todo/public_html/php/wikidata.php' ) ;

class Taxon {
	public $name, $q, $rank, $rank_q ;
	public $author, $author_q, $year ;
	public $p31 = 'Q16521' ;

	public function __construct($s='') {
		if ( $s=='' ) return ;
		if ( preg_match('|^Q\d+$|',$s) ) {
			// TODO
			return;
		}
		$this->setName($s);
	}

	public function setName($name) {
		$this->name = $this->normalizeString($name);
	}

	public function setRank($name,$taxon_ranks=[]) {
		$this->rank = strtolower($this->normalizeString($name));
		$this->setRankQ($taxon_ranks);
	}

	public function hasQ() {
		return isset($this->q);
	}

	public function hasRankQ() {
		return isset($this->rank_q);
	}

	protected function normalizeString($s) {
		return trim(preg_replace('|\s+|',' ',html_entity_decode(strip_tags(str_replace('_',' ',$s)))));
	}

	protected function setRankQ($taxon_ranks) {
		if ( isset($taxon_ranks[$this->rank]) ) $this->rank_q = $taxon_ranks[$this->rank];
	}

}


class SpeciesWikiMatcherCreator {
	public $verbose = true;
	protected $current_title ;
	protected $create_no_duplicate_person = true ;
	protected $need_quotes = ['L','D','A','S'] ;
	protected $language_codes = ['en','de','fr','es','it','pt','sv'] ;
	protected $category2statement = [
		"Acarologists" => ["P106","Q19798999"],
		"Annelidologists" => ["P106","Q38687772"],
		"Arachnologists" => ["P106","Q17344952"],
		"Botanists" => ["P106","Q2374149"],
		"Carcinologists" => ["P106","Q16868721"],
		"Cnidariologists" => ["P106","Q5137366"],
		"Entomologists" => ["P106","Q3055126"],
		"Helminthologists" => ["P106","Q27497422"],
		"Herpetologists" => ["P106","Q16271064"],
		"Herpetologists" => ["P106","Q16271064"],
		"Ichthyologists" => ["P106","Q4205432"],
		"Lepidopterists" => ["P106","Q497294"],
		"Lichenologists" => ["P106","Q15924544"],
		"Malacologists" => ["P106","Q16271261"],
		"Mammalogists" => ["P106","Q16831394"],
		"Myriapodologists" => ["P106","Q27924982"],
		"Nematologists" => ["P106","Q21491495"],
		"Ornithologists" => ["P106","Q1225716"],
		"Ostracodologists" => ["P106","Q21484199"],
		"Palaeontologists" => ["P106","Q1662561"],
		"Paleobotanists" => ["P106","Q30059961"],
		"Phycologists" => ["P106","Q12358881"],
		"Spongiologists" => ["P106","Q26936818"],
		"Zoologists" => ["P106","Q350979"],
	] ;
	protected $taxon_ranks = [
		'cultivar' => 'Q4886' ,
		'species' => 'Q7432' ,
		'speciea' => 'Q7432' ,
		'apecies' => 'Q7432' ,
		'speies' => 'Q7432' ,
		'spcies' => 'Q7432' ,
		'genus' => 'Q34740' ,
		'genu' => 'Q34740' ,
		'genera' => 'Q34740' ,
		'family' => 'Q35409' ,
		'familiae' => 'Q35409' ,
		'famila' => 'Q35409' ,
		'familia' => 'Q35409' ,
		'order' => 'Q36602' ,
		'ordo' => 'Q36602' ,
		'kingdom' => 'Q36732' ,
		'class' => 'Q37517' ,
		'classis' => 'Q37517' ,
		'phylum' => 'Q38348' ,
		'nothosubspecies' => 'Q59772023' ,
		'subspecies' => 'Q68947' ,
		'subspecis' => 'Q68947' ,
		'subpecies' => 'Q68947' ,
		'subspeccies' => 'Q68947' ,
		'subspcies' => 'Q68947' ,
		'subspesies' => 'Q68947' ,
		'domain' => 'Q146481' ,
		'tribe' => 'Q227936' ,
		'tribus' => 'Q227936' ,
		'form' => 'Q279749' ,
		'formae' => 'Q279749' ,
		'formas' => 'Q279749' ,
		'division' => 'Q334460' ,
		'subvariety' => 'Q630771' ,
		'cryptic species complex' => 'Q765940' ,
		'variety' => 'Q767728' ,
		'varietas' => 'Q767728' ,
		'varietate' => 'Q767728' ,
		'varietates' => 'Q767728' ,
		'varietatess' => 'Q767728' ,
		'varieties' => 'Q767728' ,
		'subphylum' => 'Q1153785' ,
		'nothospecies' => 'Q1306176' ,
		'superspecies' => 'Q1783100' ,
		'infraclass' => 'Q2007442' ,
		'superfamily' => 'Q2136103' ,
		'superfamilia' => 'Q2136103' ,
		'suprafamilia' => 'Q2136103' ,
		'infraphylum' => 'Q2361851' ,
		'subfamily' => 'Q2455704' ,
		'subfamilia' => 'Q2455704' ,
		'subkingdom' => 'Q2752679' ,
		'infraorder' => 'Q2889003' ,
		'infraordo' => 'Q2889003' ,
		'cohorte' => 'Q2981883' ,
		'series' => 'Q3025161' ,
		'infrakingdom' => 'Q3150876' ,
		'section' => 'Q3181348' ,
		'sectio' => 'Q3181348' ,
		'sectione' => 'Q3181348' ,
		'subgenus' => 'Q3238261' ,
		'branch' => 'Q3418438' ,
		'subdomain' => 'Q3491996' ,
		'subdivision' => 'Q3491997' ,
		'superclass' => 'Q3504061' ,
		'forma specialis' => 'Q3825509' ,
		'forma' => 'Q3825509' ,
		'subtribe' => 'Q3965313' ,
		'subtribus' => 'Q3965313' ,
		'superphylum' => 'Q3978005' ,
		'group' => 'Q4150646' ,
		'infracohort' => 'Q4226087' ,
		'form' => 'Q5469884' ,
		'infrafamily' => 'Q5481039' ,
		'subclass' => 'Q5867051' ,
		'subclassis' => 'Q5867051' ,
		'suborder' => 'Q5867959' ,
		'subordo' => 'Q5867959' ,
		'surbordo' => 'Q5867959' ,
		'superorder' => 'Q5868144' ,
		'subsection' => 'Q5998839' ,
		'subsectione' => 'Q5998839' ,
		'subsectio' => 'Q5998839' ,
		'nothogenus' => 'Q6045742' ,
		'magnorder' => 'Q6054237' ,
		'supercohort' => 'Q6054425' ,
		'infralegion' => 'Q6054535' ,
		'sublegion' => 'Q6054637' ,
		'superlegion' => 'Q6054795' ,
		'parvorder' => 'Q6311258' ,
		'parvordo' => 'Q6311258' ,
		'grandorder' => 'Q6462265' ,
		'legion' => 'Q7504331' ,
		'mirorder' => 'Q7506274' ,
		'subcohorte' => 'Q7509617' ,
		'species group' => 'Q7574964' ,
		'epifamily' => 'Q10296147' ,
		'subsection' => 'Q10861375' ,
		'section' => 'Q10861426' ,
		'subseries' => 'Q13198444' ,
		'subform' => 'Q13202655' ,
		'supertribe' => 'Q14817220' ,
		'supertribus' => 'Q14817220' ,
		'superkingdom' => 'Q19858692' ,
		'subterclass' => 'Q21061204' ,
		'hyporder' => 'Q21074316' ,
		'clade' => 'Q713623',
		'cladus' => 'Q713623',
	] ;
	protected $sitelink2q_cache = [];

	function __construct(){
		$this->tfc = new ToolforgeCommon('duplicity') ;
		$this->wil = new WikidataItemList();
		$this->dbu = $this->tfc->openDBtool ( 'duplicity_p' ) ;
		$this->qs = $this->tfc->getQS('duplicity_process_specieswiki','/data/project/wikidata-todo/reinheitsgebot.conf');
	}

	function candidate_generator() {
		#yield "Abrochtha meselsoni" ; return ; # TESTING FIXME
		$sql = "SELECT * FROM `no_wd` WHERE `wiki`='specieswiki' ORDER BY rand()" ;
		$result = $this->tfc->getSQL ( $this->dbu , $sql ) ;
		while($o = $result->fetch_object()) {
			$title = $o->title ;
			yield $title ;
		}
	}

	function process_source($title,$wikitext) {
		if ( $this->verbose) print "SOURCE: {$title}\n" ;
		# pattern match {{BHL|bibliography/152220}} to P4327
	}

	protected function sikpped($reason) {
		print "  Skipped because: {$reason}\n" ;
	}

	function process_person($title,$wikitext) {
		if ( $this->verbose) print "PERSON: {$title}\n" ;

		if ( preg_match('/^[A-Z]\./',$title) && $this->create_no_duplicate_person ) return $this->sikpped("{$title} (X.Y.Z. Smith)");
		if ( stristr($title,'/') ) return $this->sikpped('subpage: '.$title);
		if ( !stristr($title,' ') ) return $this->sikpped('single name: '.$title);

		$title2 = preg_replace ( '/ [A-Z]\. /' , ' ' , $title ); # Middle initial
		$query = "{$title2} haswbstatement:P31=Q5" ;
		$candidate_pages = $this->search_query($query);
		if ( count($candidate_pages) > 0 and $this->create_no_duplicate_person ) return $this->sikpped("Already someone with that name: https://wikidata-todo.toolforge.org/duplicity/#/article/specieswiki/".str_replace(' ','_',$title));

		# Create new item
		$qs = $this->get_new_item_qs ( $title ) ;
		$qs[] = ["LAST","P31","Q5"] ;
		$this->add_statements_based_on_categories($wikitext,$qs);
		$q = $this->create_item($qs) ;
		if ( !isset($q) ) return ;
		$this->remove_candidate_from_database($title);
		return $q ;
	}

	protected function add_statements_based_on_categories($wikitext,&$qs) {
		foreach ( $this->category2statement as $category => $prop_value ) {
			if ( !preg_match('/\[\[Category:'.$category.'(\]\]|\|)/i',$wikitext) ) continue ;
			$command = ["LAST",$prop_value[0],$prop_value[1]] ;
			$qs[] = $command ;
			#print implode('\t',$command)."\n" ;
		}
	}

	function process_herbarium($title,$wikitext) {
		if ( $this->verbose) print "HERBARIUM: {$title}\n" ;
	}

	function find_parent_taxon($title,$taxon_rank_q) {
		if ( $taxon_rank_q == "Q7432" ) $taxon_rank_parent_q = "Q34740" ; # Species->Genus
		else if ( $taxon_rank_q == "Q68947" ) $taxon_rank_parent_q = "Q7432" ; # Subspecies->Species
		else return '' ;
		$found = preg_match('|^(\S+) (\S+)$|',$title,$m) ;
		if ( !$found and preg_match('/^(\S+) (var\.|subsp\.|×) (\S+)$/',$title,$m) ) {
			$found = true ;
			$m = ['',$m[1],$m[3]] ;
		}
		if ( $found ) {
			if ( $taxon_rank_q == "Q7432" ) $parent_taxon_name = $m[1] ;
			else if ( $taxon_rank_q == "Q68947" ) $parent_taxon_name = "{$m[1]} {$m[2]}" ;
			$query = "{$parent_taxon_name} haswbstatement:P31=Q16521 haswbstatement:P105={$taxon_rank_parent_q}" ; # ...and is taxon and has parent taxon rank
			$candidate_pages = $this->search_query($query);
			if ( count($candidate_pages) == 1 ) {
				$parent_taxon_q = $candidate_pages[0] ;
				#print "Found parent taxon: https://www.wikidata.org/wiki/{$parent_taxon_q}\n" ;
				return $parent_taxon_q ;
			} else {
				#print "Found parent no taxon for '{$title}'\n" ;
			}
		}
		return '' ;
	}

	function find_taxon_rank_and_extinct($title,$wikitext) {
		$is_extinct = false ;
		$taxon_rank_q = '' ;
		$rows = explode ( "\n" , $wikitext ) ;
		foreach ( $rows AS $row ) {
			$row = trim($row) ;
			if ( !preg_match ( '|^(.+?):[ †]*\'+\[*(.+?)\]*\'+|' , $row , $m ) ) continue ;
			if ( strtolower($m[2]) != strtolower($title) and $m[2]!='{{BASEPAGENAME}}' ) continue ; # Not the species name
			$rank = trim(strtolower($m[1])) ;
			#print "Found taxon rank {$rank}: {$row}\n" ;
			if ( !isset($this->taxon_ranks[$rank]) ) continue ;
			$taxon_rank_q = $this->taxon_ranks[$rank] ;
			#print "{$rank} => {$taxon_rank_q}\n" ;
			if ( stristr($row,'†') ) $is_extinct = true ;
		}
		if ( $taxon_rank_q == '' and preg_match('| subsp\. |',$title) ) $taxon_rank_q = 'Q68947' ;
		if ( $taxon_rank_q == '' and preg_match('| sect\. |',$title) ) $taxon_rank_q = 'Q3181348' ;
		if ( $taxon_rank_q == '' and preg_match('| var\. |',$title) ) $taxon_rank_q = 'Q767728' ;
		#print "{$wikitext}\n\n" ;
		#print "{$taxon_rank_q} : {$is_extinct}\n" ; exit(0);
		return [$taxon_rank_q,$is_extinct] ;
	}

	function process_taxon($title,$wikitext) {
		if ( $this->verbose) print "TAXON: {$title}\n" ;
		#print "https://species.wikimedia.org/wiki/".$this->tfc->urlEncode($title)."\n" ;
		if ( preg_match('/\(/',$title) ) return $this->sikpped('Strange taxon name');

		list($taxon_rank_q,$is_extinct) = $this->find_taxon_rank_and_extinct($title,$wikitext);
		$parent_taxon_q = $this->find_parent_taxon($title,$taxon_rank_q) ;

		$processed_title = preg_replace ( '/\b(var\.|subsp\.|×) /i' , '' , $title ) ;
		$query = "{$processed_title} haswbstatement:P31=Q16521|P31=Q98961713" ; # ...and is taxon or extinct taxon
		$candidate_pages = $this->search_query($query);
		if ( count($candidate_pages) == 0 ) {
			$taxon_q = "Q16521" ;
			if ( $is_extinct ) $taxon_q = 'Q98961713' ;
			$qs = $this->get_new_item_qs ( $title ) ;
			$qs[] = ["LAST","P225",$this->quote($title)] ;
			$qs[] = ["LAST","P31",$taxon_q] ; # Taxon
			if ( $taxon_rank_q!='' ) $qs[] = ["LAST","P105",$taxon_rank_q] ;
			if ( $parent_taxon_q!='' ) $qs[] = ["LAST","P171",$parent_taxon_q] ;
			$q = $this->create_item($qs) ;
			if ( !isset($q) ) return ;
			$this->remove_candidate_from_database($title);
			return $q ;
		}
		if ( count($candidate_pages) == 1 ) {
			$q = $candidate_pages[0] ;
			#print "Possible match for '{$title}': https://www.wikidata.org/wiki/{$q}\n" ;
			$this->add_specieswiki_page_to_item ( $q , $title ) ;
			return $q ;
		}
		$this->log_issue ( "Multiple taxon pages" , $title , implode(",",$candidate_pages) ) ;
	}

	function process_disambiguation($title,$wikitext) {
		if ( $this->verbose) print "DISAMBIG: {$title}\n" ;
		$query = "\"{$title}\" haswbstatement:P31=Q4167410" ; # ...and is disambiguation page
		$candidate_pages = $this->search_query($query);
		if ( count($candidate_pages) == 0 ) {
			$qs = $this->get_new_item_qs ( $title ) ;
			$qs[] = ["LAST","P31","Q4167410"] ; # Disambiguation page
			$q = $this->create_item($qs) ;
			if ( !isset($q) ) return ;
			$this->remove_candidate_from_database($title);
			return $q ;
		}
		if ( count($candidate_pages) == 1 ) {
			$q = $candidate_pages[0] ;
			$this->add_specieswiki_page_to_item ( $q , $title ) ;
			return $q ;
		}
		$this->log_issue ( "Multiple disambiguation pages" , $title , implode(",",$candidate_pages) ) ;
	}

	function quote($s) {
		return '"'.$s.'"' ;
	}

	function get_new_item_qs($title) {
		$qs = [] ;
		foreach ( $this->language_codes AS $lang ) {
			$qs[] = ["LAST","L{$lang}",$title] ;
		}
		$qs[] = ["LAST","Sspecieswiki",$title] ;
		return $qs ;
	}

	function get_specieswiki_link ( $q ) {
		$this->wil->loadItem($q);
		$i = $this->wil->getItem($q) ;
		if ( !isset($i) ) return ;
		return $i->getSitelink("specieswiki") ;
	}

	function does_item_have_specieswiki_link ( $q ) {
		$sitelink = $this->get_specieswiki_link($q);
		return isset($sitelink) ;
	}

	function add_specieswiki_page_to_item ( $q , $title ) {
		$siltelink = $this->get_specieswiki_link($q);
		if ( isset($sitelink) ) {
			if ( $title==$sitelink ) {
				$this->remove_candidate_from_database($title);
				return $this->sikpped("{$title} is already linked correctly from Wikidata");
			} else {
				return $this->sikpped("Item {$q} already has a specieswiki sitelink (trying for https://wikidata-todo.toolforge.org/duplicity/#/article/specieswiki/".urlencode(str_replace(' ','_',$title))." )");
			}
		}
		$qs[] = [$q,"Sspecieswiki",$title] ;
		$qs = $this->fix_qs ( $qs ) ;
		$this->tfc->runCommandsQS($qs) ;
		$this->remove_candidate_from_database($title);
		$this->sleep();
	}

	function remove_candidate_from_database ( $title ) {
		$safe_title = $this->dbu->real_escape_string($title);
		$sql = "DELETE FROM `no_wd` WHERE `wiki`='specieswiki' AND title='{$safe_title}'" ;
		#print "{$sql}\n" ; exit(0);
		$this->tfc->getSQL ( $this->dbu , $sql ) ;
	}

	function log_issue ( $message , $page_title = '' , $note = '' ) {
		print "  !! {$message}: {$page_title} ({$note})\n" ;
	}

	function search_query($query) {
		$ret = [] ;
		$url = "https://www.wikidata.org/w/api.php?action=query&list=search&format=json&srnamespace=0&srsearch=" . urlencode($query) ;
		#print "{$url}\n" ;
		$j = json_decode ( file_get_contents ( $url ) ) ;
		if ( !isset($j) or !isset($j->query) or !isset($j->query->search) ) {
			print_r($j) ;
			return $ret;
		}
		foreach ( $j->query->search AS $s ) {
			$ret[] = $s->title ; # Qxxx
		}
		return $ret ;
	}

	function fix_qs ( $qs_orig ) {
		$qs = [] ;
		foreach ( $qs_orig AS $row ) {
			if ( is_array($row) ) {
				# Quote strings for labels etc
				if ( count($row)>=3 and in_array($row[1][0],$this->need_quotes) ) {
					$row[2] = $this->quote($row[2]) ;
				}
				# Add source information
				if ( count($row)==3 and $row[1][0]=='P' ) {
					$row[] = "S143" ;
					$row[] = "Q13679" ;
					$row[] = "S813" ;
					$row[] = date('+Y-m-d').'T00:00:00Z/11' ;
				}
				# Turn into string
				$qs[] = implode ( "\t" , $row ) ;
			} else { # String, we hope
				$qs[] = $row ;
			}
		}
		#print_r($qs); exit(0);
		return $qs ;
	}

	# $qs: Array of strings or arrays
	function create_item($qs) {
		array_unshift ( $qs , ["CREATE"] ) ;
		$qs = $this->fix_qs ( $qs ) ;
		#print_r($qs) ;
		try {
			$q = $this->tfc->runCommandsQS($qs) ;
		} catch (Exception $e) {
			unset($q);
		}
		if ( !isset($q) or $q=='' or $q==null or $q=='Q' ) {
			$fail = true ;
			if ( isset($this->current_title) ) {
				$q = $this->sitelink2q($this->current_title);
				if ( isset($q) and $q!=-1 ) {
					if ( $this->verbose) print "Tried to create a new item for {$this->current_title} but found it already as https://www.wikidata.org/wiki/{$q}\n";
					$fail = false ;
				}
			}
			if ( $fail ) {
				if ( $this->verbose) print("Problem with ".implode(" || ",$qs)."\n");
				return ;
			}
		}
		if ( $this->verbose) print "Created https://www.wikidata.org/wiki/{$q}\n" ;
		$this->sleep();
		return $q ;
	}

	function sleep() {
		sleep(2); # To prevent anti-abuse measures
	}

	function process_specialized($title,$wikitext) {
		$mode = '' ;
		if ( preg_match('|\{\{Publications|i',$wikitext) ) $mode = 'source' ;
		else if ( preg_match('|\[\[category:Sources|i',$wikitext) ) $mode = 'source' ;
		else if ( preg_match('|\[\[category:ISSN|i',$wikitext) ) $mode = 'source' ;
		else if ( preg_match('|\[\[category:Journals|i',$wikitext) ) $mode = 'source' ;
		else if ( preg_match('|\[\[category:series identifiers|i',$wikitext) ) $mode = 'source' ;
		else if ( preg_match('|\{\{Authority[_ ]control|i',$wikitext) ) $mode = 'person' ;
		else if ( preg_match('/\{\{Tax(on|a)[_ ]authored/i',$wikitext) ) $mode = 'person' ;
		else if ( preg_match('|\[\[Category:Taxon[_ ]authorities|i',$wikitext) ) $mode = 'person' ;
		else if ( preg_match('|\[\[Category:[a-z]+?ists\]\]|i',$wikitext) ) $mode = 'person' ;
		else if ( preg_match('|\{\{Herbarium|i',$wikitext) ) $mode = 'herbarium' ;
		else if ( preg_match('/\n(Genus|Species|Subspecies):/i',$wikitext) ) $mode = 'taxon' ;
		else if ( preg_match('/== *\{\{int:Taxonavigation/i',$wikitext) ) $mode = 'taxon' ;
		else if ( preg_match('/\bdisambig/i',$wikitext) ) $mode = 'disambiguation' ;

		if ( $mode!='person' ) return false; # TODO FIXME TESTING

		if ( $mode == '' ) {
			print "???: {$title}\n" ;
			return false ;
		}
		$method = "process_{$mode}" ;
		$this->$method($title,$wikitext) ;
		return true ;
	}

	function getWikiHTML($title) {
		$url = "https://species.wikimedia.org/w/api.php?format=json&prop=text&action=parse&page=".
			urlencode(str_replace(' ','_',$title)) ;
		$j = json_decode(file_get_contents($url));
		if ( !isset($j) or !isset($j->parse) or !isset($j->parse->text) ) return ;
		return $j->parse->text->{"*"};
	}

	function taxon_rank2q($taxon) {
		if ( isset($this->taxon_ranks[$taxon]) ) return $this->taxon_ranks[$taxon];
	}

	function sitelink2q($title) {
		if ( isset($this->sitelink2q_cache[$title]) ) {
			$qs = $this->sitelink2q_cache[$title];
		} else {
			$url = "https://www.wikidata.org/w/api.php?action=wbgetentities&sites=specieswiki&format=json&titles=".urlencode($title);
			$j = json_decode(file_get_contents($url));
			if ( !isset($j->entities) ) return ;
			$entities = $j->entities;
			$qs = array_keys((array)$entities);
			$this->sitelink2q_cache[$title] = $qs ;
		}
		if ( count($qs)==1 ) return $qs[0];
	}

	function fix_title($s) {
		return trim(preg_replace('|\s+|',' ',html_entity_decode(strip_tags($s))));
	}

	function try_match_taxon_distant_parent($title,$taxon_name,$distant_parent_taxon_name) {
		$sparql = "SELECT ?taxon { ?taxon wdt:P225 \"{$taxon_name}\" . ?parent wdt:P225 \"{$distant_parent_taxon_name}\" . ?taxon wdt:P171+ ?parent }";
		$items = $this->tfc->getSPARQLitems($sparql,'taxon');
		if ( count($items)==0 ) {
			// New entry?
			$taxon_p31_q = 'Q16521';

			$qs = [];
			$qs[] = ["LAST","Len",$taxon_name] ;
			if ( $taxon_name!=$distant_parent_taxon_name ) $qs[] = ["LAST","Den",$distant_parent_taxon_name] ;
			$qs[] = ["LAST","P31",$taxon_p31_q,"S143","Q13679"] ;
			$qs[] = ["LAST","P225","\"{$taxon_name}\"","S143","Q13679"] ;
			// $qs[] = ["LAST","P105",$taxon_rank_q,"S143","Q13679"] ;
			$qs[] = ["LAST","Sspecieswiki",$title] ;

			$q = $this->create_item($qs) ;
			if ( !isset($q) ) return false;
			if ( $this->verbose) print("{$title} is now https://www.wikidata.org/wiki/{$q}\n");
			$this->remove_candidate_from_database($title);
			return true;
		} else if ( count($items)==1 ) {
			$q = $items[0];
			if ( $this->verbose) print("Trying to add {$title} to https://www.wikidata.org/wiki/{$q}\n");
			$this->add_specieswiki_page_to_item($q,$title);
			return true;
		} else {
			// Not touching this
			if ( $this->verbose) print ("{$taxon} has multiple hits, not touching this\n");
			return false;
		}

	}

	function create_new_written_work_from_html($title,$html) {
		$page_title = '';
		$ipni = '';
		$abbrev = '';
		if ( preg_match('|<a rel="nofollow" class="external text" href="https://www.ipni.org/p/(.+?)">IPNI</a>|',$html,$m) ) $ipni = $m[1];
		if ( preg_match('|<li>Title: <i><b>(.+?)</b></i></li>|',$html,$m) ) $page_title = $m[1] ;
		if ( preg_match('|<li>Abbreviation: <i><b>(.+?)</b></i></li>|',$html,$m) ) $abbrev = $m[1] ;
		if ( $page_title=='' and $ipni!='' ) $page_title = $title ;
		if ( $page_title=='' ) return false; # Not a written work

		if ( $ipni!='' ) {
			$query = "haswbstatement:\"P2008={$ipni}\"" ;
			$items = $this->search_query($query);
			if ( count($items)==1 ) {
				$q = $items[0];
				if ( !$this->does_item_have_specieswiki_link($q) ) {
					$this->add_specieswiki_page_to_item ( $q , $title ) ;
					return true;
				}
			}
		}

		$qs = [];
		$qs[] = ["LAST","Len",$page_title] ;
		if ( $abbrev!='' ) $qs[] = ["LAST","Aen",$abbrev] ;
		if ( $ipni!='' ) $qs[] = ["LAST","P2008","\"{$ipni}\""] ;
		$qs[] = ["LAST","P31","Q47461344","S143","Q13679"] ;
		$qs[] = ["LAST","Sspecieswiki",$title] ;

		$q = $this->create_item($qs) ;
		if ( !isset($q) ) return false;
		if ( $this->verbose) print "Created new written work https://www.wikidata.org/wiki/{$q}\n";
		$this->remove_candidate_from_database($title);
		return true;
	}

	function create_new_taxon_from_html($title) {
		$title = str_replace('_',' ',$title);

		// $title = preg_replace('| *\(.*\) *|',' ',$title);
		if ( preg_match('|^(\S+) \((.+)\)$|',$title,$m) ) {
			return $this->try_match_taxon_distant_parent($title,$m[1],$m[2]);
		}
		if ( preg_match('|[,]|',$title) ) return ;

		$html = $this->getWikiHTML($title);

		if ( $this->create_new_written_work_from_html($title,$html) ) {
			return true;
		}

		if ( preg_match('|The name of this taxon appears to be <b>invalid</b> under the relevant nomenclatural code|',$html) ) {
			$this->remove_candidate_from_database($title);
			return ;
		}

		if ( preg_match('|<li>Authors:|',$html) ) {
			$qs = [];
			$qs[] = ["LAST","Len",$title] ;
			$qs[] = ["LAST","P31","Q47461344","S143","Q13679"] ;
			$qs[] = ["LAST","Sspecieswiki",$title] ;

			$q = $this->create_item($qs) ;
			if ( !isset($q) ) return false;
			if ( $this->verbose) print "Created new source https://www.wikidata.org/wiki/{$q}\n";
			$this->remove_candidate_from_database($title);
			return true;
		}

		if ( preg_match('|<li>BPH Index: (\d+)</li>|',$html,$m) ) {
			$qs = [];
			$qs[] = ["LAST","Len",$title] ;
			$qs[] = ["LAST","P31","Q5633421","S143","Q13679"] ;
			$qs[] = ["LAST","P4569","\"{$m[1]}\"","S143","Q13679"] ;
			$qs[] = ["LAST","Sspecieswiki",$title] ;

			$q = $this->create_item($qs) ;
			if ( !isset($q) ) return false;
			if ( $this->verbose) print "Created new source https://www.wikidata.org/wiki/{$q}\n";
			$this->remove_candidate_from_database($title);
			return true;
		}


		if ( preg_match('|<a href="/wiki/File:Disambig.svg"|',$html) ) {
			$qs = [];
			$qs[] = ["LAST","Len",$title] ;
			$qs[] = ["LAST","P31","Q4167410","S143","Q13679"] ;
			$qs[] = ["LAST","Sspecieswiki",$title] ;

			$q = $this->create_item($qs) ;
			if ( !isset($q) ) return false;
			$this->remove_candidate_from_database($title);
			return true;
		}

		$html = str_replace("\r","",$html);
		$html = str_replace(" (1):\n<i>",': <i>',$html);
		$html = preg_replace('|: *\n<i>|',': <i>',$html);
		$html = str_replace('&#160;',' ',$html);
		$html = str_replace("\n</p><p>","\n",$html);
		$html = str_replace("ː",":",$html); # Weird ":" control char
		$html = str_replace("Genus unassigned:","Genus:",$html);
		$html = preg_replace("/(Species|Genera) \(\d+\)/","$1",$html);
		$html = preg_replace('| *<i>Incertae sedis[:ː]*</i>[:ː]* *|i',':',$html);
		

		$this->current_title = $title ;

		$taxon_p31_q = 'Q16521';
		$html = preg_replace('|\n<p> *|',"\n",$html);
		if ( stristr($html, '†') !== FALSE ) { # Extinct taxon
			$taxon_p31_q = 'Q98961713';
			$html = preg_replace('|† *|','',$html);
		}
		$part1 = [
			'[a-z]+[:ː] *<i><a href="/wiki/(.+?)" title=".*?">.*?</a></i>',
			'[a-z]+[:ː] *<a href="/wiki/(.+?)" title=".*?">.*?</a>',
		];
		$part2 = [
			'([a-z]+)[:ː]\s<i><b>(.+?)</b></i>',
			'([a-z]+)[:ː]\s<i>([^<].+?)</i>',
			'([a-z]+)[:ː]\s*<i><a class="mw-selflink selflink">(.+?)</a></i>',
			'([a-z]+)[:ː]\s*<i><a.*?>('.preg_quote($title).')</a></i>',
			'([a-z]+)[:ː]\s*<a class="mw-selflink selflink">(.+?)</a>',
			'([a-z]+)[:ː]\s<b>([^<].+?)</b>',
		];

		$patterns = [];
		foreach ( $part1 as $p1 ) {
			foreach ( $part2 as $p2 ) {
				$patterns[] = '|\n'.$p1.' *<br */>\n'.$p2.'|i';
				$patterns[] = '|\n'.$p1.' *\n<br */>\s*'.$p2.'|i';
			}
		}
		$patterns[] = '|\n([a-z]+)[:ː] *<i><a class="mw-selflink selflink">(.+?)</a></i>|i';
		$patterns[] = '|\n([a-z]+)[:ː] *<a class="mw-selflink selflink">(.+?)</a>|i';

		foreach ( $patterns as $pattern ) {
			if ( preg_match($pattern,$html,$m) ) break;
		}
		// print $html;
		if ( $m == null ) return;

		if ( count($m)==4 ) {
			$parent_taxon_title = $m[1] ;
			$taxon_rank = $m[2] ;
			$taxon_name = $m[3] ;
		} else {
			$parent_taxon_title = '' ;
			$taxon_rank = $m[1] ;
			$taxon_name = $m[2] ;
		}
		$parent_taxon_title = $this->fix_title($parent_taxon_title);
		$taxon_rank = $this->fix_title($taxon_rank);
		$taxon_name = $this->fix_title($taxon_name);
		$taxon_name = preg_replace('| *\(.*\) *|',' ',$taxon_name);

		if ( $parent_taxon_title!='' ) {
			$parent_taxon_q = $this->sitelink2q($parent_taxon_title);
			if ( !isset($parent_taxon_q) or $parent_taxon_q==-1 ) {
				if ( $this->verbose) print("Trying to create parent taxon {$parent_taxon_title}\n");
				$this->create_new_taxon_from_html($parent_taxon_title);
				$parent_taxon_q = $this->sitelink2q($parent_taxon_title);
				if ( !isset($parent_taxon_q) or $parent_taxon_q==-1 ) {
					if ( $this->verbose) print("Missing parent taxon: {$parent_taxon_title}\n");
					unset($parent_taxon_q);
					$parent_taxon_title = '';
					#return;
				}
			}
		}

		$taxon_rank = strtolower($taxon_rank);
		$taxon_rank_q = $this->taxon_rank2q($taxon_rank);
		if ( !isset($taxon_rank_q) ) {
			print "Unknown taxon rank: {$taxon_rank}\n";
			return;
		}

		$taxon_name = str_replace('_',' ',$taxon_name);
		if ( preg_match('|<abbr title="(.+?)">.*?</abbr>\.&#160;(.+?)$|',$taxon_name,$m) ) {
			$taxon_name = "{$m[1]} {$m[2]}";
		}

		if ( preg_match('|^([A-Z])\. (.+?)$|',$taxon_name,$m) and $parent_taxon_title!='' ) {
			$taxon_name = "{$parent_taxon_title} {$m[2]}" ;
		}
		$taxon_name = str_replace('_',' ',$taxon_name);

		$query = "haswbstatement:\"P225={$taxon_name}\"" ;
		$items = $this->search_query($query);
		if ( count($items)>1 and isset($parent_taxon_q) ) {
			$query .= " haswbstatement:P171={$parent_taxon_q}" ;
			$items = $this->search_query($query);
		}

		if ( count($items) == 1 ) { //and $taxon_rank=="species" ) {
			$q = $items[0];
			if ( $this->verbose) print ("Adding {$title} to https://www.wikidata.org/wiki/{$q}\n");
			$this->add_specieswiki_page_to_item($q,$title);
			$this->remove_candidate_from_database($title);
			return true;
		}
		if ( count($items) > 0 ) {
			if ( $this->verbose) print ("Too many items for {$taxon_name}\n");
			return ;
		}

		$qs = [];
		$qs[] = ["LAST","Len",$taxon_name] ;
		$qs[] = ["LAST","P31",$taxon_p31_q,"S143","Q13679"] ;
		$qs[] = ["LAST","P225","\"{$taxon_name}\"","S143","Q13679"] ;
		if ( $parent_taxon_title!='' ) $qs[] = ["LAST","P171",$parent_taxon_q,"S143","Q13679"] ;
		if ( $taxon_rank_q!='' ) $qs[] = ["LAST","P105",$taxon_rank_q,"S143","Q13679"] ;
		$qs[] = ["LAST","Sspecieswiki",$title] ;

		$q = $this->create_item($qs) ;
		if ( !isset($q) ) return false;
		$this->remove_candidate_from_database($title);
		return true;
	}

	public function try_simple_taxon_match($title) {
		$query = "haswbstatement:\"P225={$title}\"" ;
		$items = $this->search_query($query);
		if ( count($items)!=1 ) return false;
		$q = $items[0];
		if ( $this->does_item_have_specieswiki_link($q) ) return false;
		$this->add_specieswiki_page_to_item ( $q , $title ) ;
		return true;
	}


	public function create_or_match() {
		foreach ( $this->candidate_generator() as $title ) {
			$q = $this->sitelink2q($title) ;
			if ( isset($q) and $q!=-1 ) {
				$this->remove_candidate_from_database($title);
				continue;
			}

			try {
				if ( $this->create_new_taxon_from_html($title) ) continue ;
				if ( $this->try_simple_taxon_match($title) ) continue ;
				// print "Not found: https://wikidata-todo.toolforge.org/duplicity/#/article/specieswiki/".str_replace(' ','_',$title)."\n";
			} catch (Exception $e) {
		    	echo 'Caught exception: ',  $e->getMessage(), "\n";
			}

			try {
				$wikitext = $this->tfc->getWikiPageText("specieswiki",$title) ;
				if ( $this->process_specialized($title,$wikitext) ) continue ;
			} catch (Exception $e) {
		    	echo 'Caught exception: ',  $e->getMessage(), "\n";
			}


			#print "https://species.wikimedia.org/wiki/".$this->tfc->urlEncode($title)."\n" ;
			#print "{$title}\n" ;
			#print "{$wikitext}\n" ;
			#exit(0);
		}
	}

	function checkTaxon($taxon_q) {
		$taxon = new Taxon;
		$taxon->q = $taxon_q;
		$this->wil->loadItem($taxon->q);
		$i = $this->wil->getItem($taxon->q) ;
		if ( !isset($i) ) return ;
		if ( !$i->hasTarget("P31","Q16521") and !$i->hasTarget("P31","Q98961713") ) {
			print "{$taxon->q} given to checkTaxon but is not a taxon\n";
			return;
		}
		$sitelink = $i->getSitelink('specieswiki');
		if ( !isset($sitelink) ) return ;
		$taxon->setName ( $sitelink ) ;
		print "CHECKING {$taxon->q}: {$taxon->name}\n" ;
		$html = $this->getWikiHTML($sitelink);
		$html = str_replace("\n",' ',$html);

		# Taxon author
		$pattern = '|<ul><li>[\s†]*<i>'.preg_quote($taxon->name).'</i>\s*<span.*?><span.*?><a href=".*?" title="(.+?)">.*?</a></span></span>,\s*(\d+)</li></ul>|';
		if ( preg_match($pattern,$html,$m) ) {
			$taxon->author = $m[1] ;
			$taxon->author_q = $this->sitelink2q($taxon->author);
			$taxon->year = $m[2] ;
		}

		$html = preg_replace("|</p><p>|","<br />",$html);
		$html = preg_replace('|</p>\s+<h2.*$|','</p>',$html);

		// print "{$html}\n" ;


		print_r($taxon);
		$html .= " {$html}";
		if ( !preg_match_all('|<p>(.+?)</p>|', $html, $m) ) return ;
		if ( !isset($m[1]) ) return;
		$m = $m[1];
		$rows = [];
		foreach ($m as $p) {
			$tmp = preg_replace('|\s*<br */>\s*|',"\n",$p);
			$tmp = explode("\n",$tmp);
			$rows = array_merge($rows,$tmp);
		}
		$taxa = [];
		foreach($rows as $row) { // [taxon rank or '', taxon name, full row for debugging]
			$row = preg_replace('|^(<.*?>\s*)*\s*|','',$row);
			if ( preg_match('|^([A-Za-z]+).*?<a .*?title="(.+?)"|',$row,$m) ) $taxa[] = [$m[1],$m[2],$row];
			else if ( preg_match('|<a .*?title="(.+?)"|',$row,$m) ) $taxa[] = ['',$m[2],$row];
			else if ( preg_match('|^([A-Za-z]+).*?<a class="mw-selflink selflink">|',$row,$m) ) $taxa[] = [$m[1],$taxon->name,$row];
			else if ( preg_match('|<a class="mw-selflink selflink">|',$row,$m) ) $taxa[] = ['',$taxon->name,$row];
			// else $taxa[] = ['','',$row]; // For testing
		}
		// print_r($taxa);

		# Remove "lower"-ranked taxa, if any
		while ( count($taxa)>0 and $taxa[count($taxa)-1][1]!=$taxon->name ) array_pop($taxa);

		if ( count($taxa)==0 ) {
			print "Taxon {$taxon->name} not found in taxa list\n" ;
		} else {
			$taxon->setRank ( $taxa[count($taxa)-1][0], $this->taxon_ranks );
			if ( preg_match('|†|',$taxa[count($taxa)-1][2]) ) $taxon->p31 = 'Q98961713'; // TODO use this
		}

		$qs = [];

		# Taxon name
		$existing_taxon_names = $i->getStrings("P225");
		if ( !in_array($taxon->name, $existing_taxon_names) ) { // No taxon name, or not this one
			$tn = [$taxon->q,'P225',"\"{$taxon->name}\""] ;
			if ( isset($taxon->author_q) ) {
				$tn[] = 'P405';
				$tn[] = $taxon->author_q;
			}
			if ( isset($taxon->year) ) {
				$tn[] = 'P574';
				$tn[] = "{$taxon->year}-01-01T00:00:00Z/9";
			}
			$qs[] = $tn;
			foreach ( $existing_taxon_names as $taxon_name ) $qs[] = [$taxon->q,'-P225',"\"{$taxon_name}\""];
		}


		if ( $taxon->hasRankQ() and !$i->hasClaims('P105') ) { // No taxon rank
			$qs[] = [$taxon->q,'P105',$taxon->rank_q];
			$claims = $i->getClaims('P105') ;
			foreach ( $claims AS $c ) {
				$target = $i->getTarget($c) ;
				$qs[] = [$taxon->q,'-P105',$target];
			}
		}
		$aliases = $i->getAllAliases() ;
		$aliases[] = $i->getLabel('en',true);
		if ( !in_array($taxon->name,$aliases) ) {
			$qs[] = [$taxon->q,'Len',$taxon->name];
		}

		if ( count($taxa)>1 ) {
			$parent = new Taxon ( $taxa[count($taxa)-2][1] ) ;
			$parent->setRank ( $taxa[count($taxa)-2][0] , $this->taxon_ranks ); // Only to ensure high quality
			$parent->q = $this->sitelink2q($parent->name);
			if ( $parent->hasQ() and $parent->hasRankQ() and !$i->hasTarget('P171',$parent->q) ) {
				$qs[] = [$taxon->q,'P171',$parent->q];
			}
		}

		if ( count($qs)==0 ) return ; // Nothing to do

		$qs = $this->fix_qs ( $qs ) ;
		try {
			$this->tfc->runCommandsQS($qs) ;
		} catch (Exception $e) {
			print "QS: ".$e->getMessage()."\n";
		}
	}

	public function updateFromSpeciesWiki() {
		// No parent taxon
		$sparql = "SELECT ?item { ?article schema:about ?item ; schema:isPartOf <https://species.wikimedia.org/> . ?item wdt:P31 wd:Q16521 . MINUS { ?item wdt:P171 [] } }";
		$sparql .= " LIMIT 5"; # TESTING
		$items = $this->tfc->getSPARQLitems($sparql,'item');
		foreach ( $items as $item ) $this->checkTaxon($item);
	}
}

$sw = new SpeciesWikiMatcherCreator() ;
if ( isset($argv[1]) ) {
	$title = $argv[1];
	if ( !$sw->create_new_taxon_from_html($title) ) {
		$sw->try_simple_taxon_match($title) ;
	}
	exit(0); # TESTING
}
$sw->verbose = false;
$sw->create_or_match();

// $sw->updateFromSpeciesWiki();
// $sw->checkTaxon('Q96185740');

print ("All done!\n");

?>