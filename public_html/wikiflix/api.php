<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR|E_ALL);
ini_set('display_errors', 'On');

require_once ( '/data/project/wikidata-todo/scripts/wikiflix/wikiflix.php' ) ;

$wf = new WikiFlix;
$action = $wf->tfc->getRequest('action','');
$wf->language = preg_replace('|[^a-z-]|','',$wf->tfc->getRequest('language','en'));


$out = [ 'status'=>'OK' ];

if ( $action=='get_movie' ) {
	$q = preg_replace('|\D|','',$wf->tfc->getRequest('q',0))*1;
	$out['data'] = $wf->getMovie($q);
} else if ( $action=='get_all_sections' ) {
	$out['data'] = $wf->get_top_sections(PHP_INT_MAX);
} else if ( $action=='get_section' ) {
	# ATTENTION always assumes property is set
	$max = $wf->tfc->getRequest('max',25);
	if ($max='all') $max = PHP_INT_MAX;
	else $max = $max*1;
	$section = (object) [
		'section_q' => $wf->tfc->getRequest('q',0)*1,
		'property' => $wf->tfc->getRequest('prop',0)*1,
	];
	$wil = new WikidataItemList();
	$wil->loadItems([$section->section_q]);
	$item = $wil->getItem($section->section_q);
	if ( isset($item) ) $out['data'] = $wf->populate_section($section,$item,$max);
	else $out['status'] = "No such item Q{$section->section_q}";
} else if ( $action=='search' ) {
	$query = $wf->tfc->getRequest('query','');
	$out['data']['movies'] = $wf->search_movies($query);
	$out['data']['sections'] = $wf->search_sections($query);
	$out['data']['people'] = $wf->search_people($query);
} else if ( $action=='get_person' ) {
	$q = $wf->tfc->getRequest('q',0)*1;
	$out['data'] = $wf->getPerson($q);
} else {
	$out['status'] = "Bad action: {$action}";
}

header('Content-Type: application/json');
print json_encode ( $out ) ;
?>