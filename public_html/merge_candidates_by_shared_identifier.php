<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');

require_once ( '/data/project/wikidata-todo/public_html/php/ToolforgeCommon.php' ) ;
require_once ( 'php/wikidata.php' ) ;
header('Content-type: application/json');
#$db = openToolDB ( 'duplicity_p' ) ;
$callback = isset($_REQUEST['callback']) ? $_REQUEST['callback'] : '' ;
$out = array () ;
$testing = isset($_REQUEST['testing']) ;
$tfc = new ToolforgeCommon () ;
$tfc->tool_user_name = 'wikidata-todo' ;
$dbt = $tfc->openDBtool ( 'wikidata_merge_by_id_p' ) ;

if ( $_REQUEST['action'] == 'desc' ) {
	$props = [ 0 => 'All' ] ;
	$sql = "select distinct prop from candidates order by prop" ;
	$result = $tfc->getSQL ( $dbt , $sql ) ;
	while($o = $result->fetch_object()) $props[$o->prop] = 'P'.$o->prop ;

	$wil = new WikidataItemList ;
	$wil->testing = true ;
	$wil->loadItems ( $props ) ;
	foreach ( $props AS $k => $v ) {
		$i = $wil->getItem ( $v ) ;
		if ( !isset($i) ) continue ;
		$props[$k] = "{$v}: " . $i->getLabel() ;
	}

	$out = [
		"label" => array ( "en" => 'Merge by shared identifier' ) ,
		"description" => array ( "en" => "Potential items to be merged, via shared identifiers (VIAF etc)." ) ,
		"icon" => 'https://upload.wikimedia.org/wikipedia/commons/thumb/b/bd/Share_Icon.svg/100px-Share_Icon.svg.png' ,
		'options' => [ [ 'name' => 'Property' , 'key' => 'prop' , 'values' => $props ] ]
	] ;
} else if ( $_REQUEST['action'] == 'tiles' ) {
	$num = $tfc->getRequest('num',0)*1 ; // Number of games to return
	$lang = $tfc->getRequest('lang','en') ;
	$prop = $tfc->getRequest ( 'prop' , 0 ) * 1 ;

	$wil = new WikidataItemList ;

	$hadthat_tmp = explode ( ',' , preg_replace ( '/[^0-9,]/' , '' , $tfc->getRequest ( 'in_cache' , '' ) ) ) ;
	$hadthat = array() ;
	foreach ( $hadthat_tmp AS $v ) {
		if ( $v != '' ) $hadthat[] = $v ;
	}
	
	$db = $tfc->openDB ( 'wikidata' , 'wikidata' ) ;

	$rounds_left = 5 ;
	$out['tiles'] = [] ;
	while ( count($out['tiles']) < $num ) {
		if ( $rounds_left-- == 0 ) break ;

		$r = rand() / getrandmax() ;
		$sql = "SELECT * FROM candidates WHERE random>={$r} AND `done`='OPEN'" ;
		if ( $prop != 0 ) $sql .= " AND prop={$prop}" ;
		$sql .= " ORDER BY random LIMIT {$num}" ;
		$result = $tfc->getSQL ( $dbt , $sql ) ;
		while($o = $result->fetch_object() and count($out['tiles']) < $num ) {
			$q1 = 'Q' . $o->q1 ;
			$q2 = 'Q' . $o->q2 ;
			
			if ( in_array ( $o->id , $hadthat ) ) continue ;
			$hadthat[] = $o->id ;

			$wil->loadItem ( 'P'.$o->prop ) ;
			$i = $wil->getItem ( 'P'.$o->prop ) ;
			if ( !isset($i) ) continue ; # Huh
			
			# Check if one of them is redirect, already merged
			$skip = false ;
			$sql = "SELECT * FROM page WHERE page_namespace=0 AND page_title IN ('$q1','$q2') AND page_is_redirect=1" ;
			$result = $tfc->getSQL ( $db , $sql ) ;
			while($p = $result->fetch_object()) $skip = true ;
			if ( $skip ) {
				$sql = "UPDATE candidates SET `done`='RESOLVED' WHERE id={$o->id}" ;
				continue ;
			}
			
			
			$g = array(
				'id' => $o->id ,
				'sections' => array () ,
				'controls' => array ()
			) ;

			$section = [ 'type' => 'text' , 'title' => "Sharing property value for: " . $i->getLabel($lang) . " (P{$o->prop})" , 'text' => $o->value ] ;
			if ( $i->hasClaims('P1630') ) $section['url'] = str_replace ( '$1' , urlencode($o->value) , $i->getFirstString ( 'P1630' ) ) ;
			$g['sections'][] = $section ;

			$g['sections'][] = array ( 'type' => 'item' , 'q' => $q1 ) ;
			$g['sections'][] = array ( 'type' => 'item' , 'q' => $q2 ) ;
			$g['controls'][] = array (
				'type' => 'buttons' ,
				'entries' => array (
					array ( 'type' => 'green' , 'decision' => 'yes' , 'label' => 'Same topic' , 'api_action' => array ('action'=>'wbmergeitems','fromid'=>$q2,'toid'=>$q1,'ignoreconflicts'=>'description|sitelink' ) ) ,
					array ( 'type' => 'white' , 'decision' => 'skip' , 'label' => 'Skip' ) ,
					array ( 'type' => 'blue' , 'decision' => 'no' , 'label' => 'Ignore' )
				)
			) ;
			$out['tiles'][] = $g ;
		}
	}

} else if ( $_REQUEST['action'] == 'log_action' ) {

	$decision = $tfc->getRequest ( 'decision' , '' ) ;
	$id = $tfc->getRequest('tile',0) * 1 ;
	
	if ( $decision != 'skip' and $id>0 ) {
		$dec = $decision=='yes' ? 'MERGED' : 'IGNORE' ;
		$sql = "UPDATE candidates SET `done`='$dec' WHERE id={$id}" ;
		$result = $tfc->getSQL ( $dbt , $sql ) ;
	}
	
} else {
	$out['error'] = "No valid action!" ;
}

if ( $callback != '' ) print $callback . '(' ;
print json_encode ( $out ) ;
if ( $callback != '' ) print ")\n" ;

?>