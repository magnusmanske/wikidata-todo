#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR); // E_ALL|

require_once ( '../../public_html/php/common.php' ) ;

function hasLink ( $q , $title , $ns = 120 ) {
	global $db ;
	$has_link = false ;
	$sql = "SELECT * FROM page,pagelinks,linktarget WHERE pl_target_id=lt_id AND pl_from=page_id AND lt_namespace=$ns and page_namespace=0 and page_title='$q' AND lt_title='$title' LIMIT 1" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	while($o = $result->fetch_object()) $has_link = true ;
	return $has_link ;
}

$dbs = new SQLite3('t2t.sqlite');

$includes = array() ;
$included = array() ;
$results = $dbs->query('select template,includes from t2t');
while ($row = $results->fetchArray()) {
	$i = str_replace('_',' ',$row[1]) ;
	$includes[str_replace('_',' ',$row[0])][] = $i ;
	$included[$i] = 1 ;
}

$db = openDB ( 'wikidata' , 'wikidata' , true ) ;

// Find all sitelinks
$page2q = array() ;
$sql = "SELECT ips_item_id,ips_site_page FROM wb_items_per_site WHERE ips_site_id='specieswiki'" ;
if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
while($o = $result->fetch_object()) {
	$page = str_replace('_',' ',$o->ips_site_page) ;
	if ( isset($includes[$page]) ) $page2q[$page][$o->ips_item_id] = 1 ;
	if ( isset($included[$page]) ) $page2q[$page][$o->ips_item_id] = 1 ;
}


foreach ( $includes AS $t => $i ) {
	if ( count($i) > 1 ) continue ;
	$i = $i[0] ;
	
	if ( !isset($page2q[$t]) or !isset($page2q[$i]) ) continue ;
	if ( count($page2q[$t]) > 1 or count($page2q[$i]) > 1 ) continue ;
	foreach ( $page2q[$t] AS $q => $one ) $qt = $q ;
	foreach ( $page2q[$i] AS $q => $one ) $qi = $q ;

	// Make sure it does not have a parent taxon yet
	if ( hasLink ( "Q$qt" , 'P171' ) ) continue ;
	
	// Make sure it's a taxon
	if ( !hasLink ( "Q$qt" , 'Q16521' , 0 ) ) continue ;

	print "Q$qt\tP171\tQ$qi\tS143\tQ13679\n" ;
	if ( !hasLink ( "Q$qi" , 'P31' ) ) print "Q$qi\tP31\tQ16521\tS143\tQ13679\n" ;
	
//	print "$qi ($i) is parent of $qt ($t)\n" ;
}

?>