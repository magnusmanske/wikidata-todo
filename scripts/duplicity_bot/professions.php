#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');
ini_set('memory_limit','500M');

require_once ( '/data/project/quickstatements/public_html/quickstatements.php' ) ;
require_once ( '../../public_html/php/ToolforgeCommon.php' ) ;
require_once ( '../../public_html/php/wikidata.php' ) ;

$tfc = new ToolforgeCommon('duplicity_professions') ;

# Match professions to categories
$sparql = 'SELECT ?main_topic ?category {
	?category schema:about ?item ; schema:isPartOf <https://species.wikimedia.org/> .
	?item wdt:P31 wd:Q4167836; wdt:P301 ?main_topic .
	?main_topic wdt:P31 wd:Q28640
}';
$j = $tfc->getSPARQL($sparql);
$category2profession = [];
foreach ($j->results->bindings as $b) {
	$q = preg_replace('|^.+/|','',$b->main_topic->value);
	$category = str_replace(' ','_',preg_replace('|^.+/Category:|','',$b->category->value));
	$category = urldecode($category);
	$category2profession[$category] = $q ;
}

# Get Wikidata items with Wikispecies people without profession
$dbws = $tfc->openDBwiki("specieswiki");
$sparql = 'SELECT ?item ?article {
	?article schema:about ?item ; schema:isPartOf <https://species.wikimedia.org/> .
	?item wdt:P31 wd:Q5 .
	MINUS { ?item wdt:P106 [] }
}';
$j = $tfc->getSPARQL($sparql);
$articles_safe = [];
foreach ($j->results->bindings as $b) {
	$q = preg_replace('|^.+Q|','',$b->item->value);
	$article = str_replace(' ','_',preg_replace('|^.+/|','',$b->article->value));
	$article = urldecode($article);
	$articles_safe[] = $dbws->real_escape_string($article);
}
if ( count($articles_safe)==0 ) exit(0); # Nothing to do

# Map articles to categories to professions
$qs = [];
$sql = 'SELECT cl_to,pp_value FROM page,categorylinks,page_props
	WHERE page_namespace=0 AND cl_from=page_id AND page_is_redirect=0 
	AND pp_page=page_id AND pp_propname="wikibase_item"
	AND page_title IN ("'.implode('","',$articles_safe).'")' ;
$result = $tfc->getSQL ( $dbws , $sql ) ;
while($o = $result->fetch_object()){
	if ( !isset($category2profession[$o->cl_to]) ) continue;
	$profession_q = $category2profession[$o->cl_to];
	$article_q = $o->pp_value;
	$qs[] = implode("\t",[$article_q,'P106',$profession_q,'S143','Q13679']);
}

# Add statements to Wikidata
$tfc->getQS('duplicity');
$tfc->runCommandsQS($qs);

?>