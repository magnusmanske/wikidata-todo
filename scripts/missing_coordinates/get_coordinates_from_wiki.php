#!/usr/bin/php
<?PHP

ini_set('memory_limit','4500M');

// Run  update_missing_coordinates.sh first

require_once ( "../../public_html/php/common.php" ) ;

$wiki = $argv[1] ;
if ( !isset($wiki) ) die ( "Requires wiki as argument\n" ) ;

# Get item for this wiki, for reference
$sparql = 'SELECT ?q WHERE { ?q wdt:P1800 "'.$wiki.'" }' ;
$i = getSPARQLitems ( $sparql ) ;
if ( count($i) != 1 ) exit(0);//die ( "Bad return for $sparql (!=1)\n" ) ;
$q_wiki = 'Q'.$i[0] ;


# Limit to locations
$tmp = explode ( "\n" , file_get_contents ( 'location.items' ) ) ;
foreach ( $tmp AS $t ) $valid_items[$t] = 1 ;
unset ( $tmp ) ;

# Limit to events
$tmp = explode ( "\n" , file_get_contents ( 'events.items' ) ) ;
foreach ( $tmp AS $t ) $valid_items[$t] = 1 ;
unset ( $tmp ) ;


# Run
$out = '' ;
$db = @openDBwiki ( $wiki , true ) ;
if ( !is_object($db) ) exit(0) ;
$sql = 'SELECT DISTINCT page_title,gt_lat,gt_lon,pp_value FROM page,geo_tags,page_props WHERE page_is_redirect=0 AND page_namespace=0 and gt_page_id=page_id AND gt_primary=1 AND page_id=pp_page AND pp_propname="wikibase_item" AND NOT EXISTS (SELECT * FROM wikidatawiki_p.page p0,wikidatawiki_p.pagelinks,wikidatawiki_p.linktarget WHERE pl_target_id=lt_id AND p0.page_title=pp_value AND p0.page_namespace=0 AND p0.page_is_redirect=0 AND pl_from=p0.page_id AND lt_title="P625" AND lt_namespace=120)' ;
if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
while($o = $result->fetch_object()){
	if ( !isset($valid_items[$o->pp_value]) ) continue ;
	$out .= $o->pp_value . "\tP625\t@" . $o->gt_lat . "/" . $o->gt_lon . "\tS143\t$q_wiki\n" ;
}

# Write to file
$fn = "/data/project/wikidata-todo/scripts/missing_coordinates/out/$wiki.out" ;
$fp = fopen($fn, 'w');
fwrite ( $fp , $out ) ;
fclose ( $fp ) ;

?>
