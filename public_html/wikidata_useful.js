// <nowiki>
/*
USAGE : Add the line
importScript("User:Magnus Manske/wikidata useful.js");// [[User:Magnus Manske/wikidata useful.js]]
to your [[Special:Mypage/common.js]] page
*/

/************************************************************************************************************************
The section below contains the "Wikidata Usefuls" link definitions. Add new ones or edit existing ones as you see fit.
*************************************************************************************************************************/

/*
You can add your own links, just for yourself, if you add code on your common.js page ''before'' the importScript, like so:

var wd_useful_thisis_add = [
				[ 'village' , 'P31:Q532' ] , // instance of: village
				[ 'PD-100' , 'P6216:Q19652|P459:Q29940705' ] // copyright status:public domain, with qualifier determination method:100 years after author's death
				// You can add more (item only!) qualifiers by adding them with "|"
] ;


*/

var wd_useful_thisis_langs = {} ;
var wgUserLanguage = mw.config.wgUserLanguage;
wd_useful_thisis_langs['en'] = [
	[
		{
			pre : 'a ' ,
			options : [
				[ '<b>human</b>' , 'P31:Q5' ] ,
				[ '<b>fictional character </b>' , 'P31:Q95074' ] ,
				[ 'man' , 'P21:Q6581097' , 'P31:Q5' ] , // , 'P107:Q215627'
				[ 'woman' , 'P21:Q6581072' , 'P31:Q5' ] , // , 'P107:Q215627' ,
				[ 'intersex' , 'P21:Q1097630' ] ,
				[ 'transgender male' , 'P21:Q15145783 ' ] ,
				[ 'transgender female ' , 'P21:Q15145782' ] ,
				[ 'duo ' , 'P31:Q10648343' ] ,
			]
		} ,
		
		{
			pre : 'Jobs:' ,
			options : [
				[ 'actor' , 'P106:Q33999' ] ,
				[ 'artist' , 'P106:Q483501' ] ,
				[ 'director' , 'P106:Q2526255' ] ,
				[ 'economist' , 'P106:Q188094' ] ,
				[ 'engraver' , 'P106: Q329439' ] ,
				[ 'lawyer' , 'P106:Q40348' ] ,
				[ 'writer' , 'P106:Q36180' ] ,
				[ 'painter' , 'P106:Q1028181' ] ,
				[ 'politician' , 'P106:Q82955' ] ,
				[ 'photographer' , 'P106:Q33231' ] ,
				[ 'musician' , 'P106:Q639669' ] ,
				[ 'journalist' , 'P106:Q1930187' ] ,
				[ 'mathematician' , 'P106:Q170790' ] ,
				[ 'sculptor' , 'P106:Q1281618' ] ,
				[ 'scientist' , 'P106:Q901' ] ,
				[ 'singer' , 'P106:Q177220' ] ,
				[ 'sports journalist' , 'P106:Q13219447' ] ,
			]
		} ,
		
		{
			pre : 'Sport:' ,
			options : [
				[ 'sportsperson' , 'P106:Q2066131' ] ,
				[ 'baseball' , 'P106:Q10871364' ] ,
				[ 'basketball' , 'P106:Q3665646' ] ,
				[ 'bicycle racer ' , 'P106:Q2309784' ] ,
				[ 'cricket' , 'P106:Q12299841' ] ,
				[ 'football' , 'P106:Q937857' ] ,
				[ 'track cyclist' , 'P106:Q15117395' ] ,
				[ 'softball' , 'P106:Q13388586' ] ,
				[ 'volley' , 'P106:Q15117302' ] ,
			]
		} ,
		
		{
			pre : 'a ' ,
			options : [
				[ 'species' , 'P105:Q7432' , 'P31:Q16521' ] ,
				[ 'genus' , 'P105:Q34740' , 'P31:Q16521' ] ,
				[ 'family' , 'P105:Q35409' , 'P31:Q16521' ] ,
				[ 'order' , 'P105:Q36602' , 'P31:Q16521' ] ,
				[ 'class' , 'P105:Q37517' , 'P31:Q16521' ] ,
			]
		} ,
		
		{
			pre : 'a ' ,
			options : [
//				[ '<b>location</b>' , 'P107:Q618123' ] ,
				[ 'mountain' , 'P31:Q8502' ] ,
				[ 'mountain range' , 'P31:Q46831' ] ,
				[ 'river' , 'P31:Q4022' ] ,
				[ 'lake' , 'P31:Q23397' ] ,
				[ 'railway station' , 'P31:Q55488' ] ,
				[ 'bridge' , 'P31:Q12280' ] ,
			]
		} ,
		
		{
			pre : 'a ' ,
			options : [
//				[ '<b>work</b>' , 'P107:Q386724' ] ,
				[ 'movie' , 'P31:Q11424' ] ,
				[ 'album' , 'P31:Q482994' ] ,
				[ 'song' , 'P31:Q7366' ] ,
				[ 'book' , 'P31:Q571' ] ,
				[ 'novel' , 'P31:Q571' , 'P136:Q8261' ] ,
				[ 'fictional character' , 'P31:Q95074' ] ,
				[ 'video game' , 'P31:Q7889' ] ,
				[ 'TV series' , 'P31:Q5398426' ] ,
				[ 'play' , 'P31:Q25379' ] ,
			]
		} ,

		{
			pre : 'an ' ,
			options : [
//				[ '<b>organization</b>' , 'P107:Q43229' ] ,
				[ 'band' , 'P31:Q215380' ] ,
				[ 'rock band' , 'P31:Q5741069' ] ,
				[ 'Category' , 'P31:Q4167836' ] ,
				[ 'Template' , 'P31:Q11266439' ] ,
				[ 'List' , 'P31:Q13406463' ] ,
				[ 'Disambig' , 'P31:Q4167410' ] ,
			]
		} ,
	]
] ;


wd_useful_thisis_langs['fr'] = [
	[
		{
			pre : 'Sexe : ' ,
			options : [
				[ '<b>être humain</b>' , 'P31:Q5' ] ,
				[ '<b>pers. de fiction </b>' , 'P31:Q95074' ] ,
				[ 'homme' , 'P21:Q6581097' ] ,//'P107:Q215627'
				[ 'femme' , 'P21:Q6581072' ] ,//'P107:Q215627'
			]
		} ,
		
		{
			pre : 'Profession : ' ,
			options : [
				[ 'acteur' , 'P106:Q33999' ] ,
				[ 'artiste' , 'P106:Q483501' ] ,
				[ 'aviateur' , 'P106:Q2095549' ] ,
				[ 'avocat' , 'P106:Q40348' ] ,
				[ 'chanteur' , 'P106:Q177220' ] ,
				[ 'compositeur' , 'P106:Q36834' ] ,
				[ 'dramaturge' , 'P106:Q214917' ] ,
				[ 'économiste' , 'P106:Q188094' ] ,
				[ 'écrivain' , 'P106:Q36180' ] ,
				[ 'explorateur' , 'P106:Q11900058' ] ,
				[ 'historien' , 'P106:Q201788' ] ,
				[ 'ingénieur' , 'P106:Q81096' ] ,
				[ 'inventeur' , 'P106:Q205375' ] ,
				[ 'journaliste' , 'P106:Q1930187' ] ,
				[ 'journaliste sportif' , 'P106:Q13219447' ] ,
				[ 'librettiste' , 'p106:Q8178443' ] ,
				[ 'mannequin' , 'p106:Q4610556' ] ,
				[ 'mathématicien' , 'P106:Q170790' ] ,
				[ 'militaire' , 'p106:Q4991371' ] ,
				[ 'musicien' , 'P106:Q639669' ] ,
				[ 'officier' , 'P106:Q189290' ] ,
				[ 'peintre' , 'P106:Q1028181' ] ,
				[ 'photographe' , 'P106:Q33231' ] ,
				[ 'poète' , 'P106:Q49757' ] ,
				[ 'politicien' , 'P106:Q82955' ] ,
				[ 'prêtre catholique' , 'P106:Q250867' ] ,
				[ 'réalisateur' , 'P106:Q2526255' ] ,
				[ 'sculpteur' , 'P106:Q1281618' ] ,
			]
		} ,

		{
			pre : 'Fonction : ' ,
			options : [
				[ 'député français' , 'P39:Q3044918' ] ,
				[ 'sénateur français' , 'P39:Q14828018' ] ,
				[ 'maire français' , 'P39:Q382617' ] ,
				[ 'évêque' , 'P39:Q29182' ] ,
				[ 'archevêque' , 'P39:Q49476' ] ,
				[ 'cardinal' , 'P39:Q45722' ] ,
				[ 'pape' , 'P39:Q19546' ] ,
			]
		} ,

		{
			pre : 'Sport :' ,
			options : [
				[ 'sportif' , 'P106:Q2066131' ] ,
				[ 'basket-ball' , 'P106:Q3665646' ] ,
				[ 'football' , 'P106:Q937857' ] ,
				[ 'cricket' , 'P106:Q5185059' ] ,
				[ 'cycliste ' , 'P106:Q2309784' ] ,
				[ 'cycliste sur piste' , 'P106:Q15117395' ] ,
				[ 'baseball' , 'P106:Q10871364' ] ,
				[ 'rugby' , 'p106:q13415036' ] ,
				[ 'softball' , 'P106:Q13388586' ] ,
				[ 'volley' , 'P106:Q15117302' ] ,
			]
		} ,
		
		{
			pre : 'Une : ' ,
			options : [
				[ 'espèce' , 'P105:Q7432' ] ,//'P107:Q1969448'
				[ 'genre' , 'P105:Q34740' ] ,
				[ 'famille' , 'P105:Q35409' ] ,
				[ 'ordre' , 'P105:Q36602' ] ,
				[ 'classe' , 'P105:Q37517' ] ,
			]
		} ,
		
		{
			pre : 'Lieu : ' ,
			options : [
			    //[ '<b>lieu</b>' , 'P107:Q618123' ] ,
				[ 'cordillière' , 'P31:Q46831' ] ,
				[ 'lac' , 'P31:Q23397' ] ,
				[ 'montagne' , 'P31:Q8502' ] ,
				[ 'rivière ' , 'P31:Q4022' ] ,
				[ ' château' , 'P31:Q751876' ] ,
				[ 'château fort' , 'P31:Q23413' ] ,
				[ 'manoir ' , 'P31:Q879050' ] ,
				[ ' abbaye' , 'P31:Q160742' ] ,
				[ 'basilique' , 'P31:Q163687' ] ,
				[ 'cathédrale' , 'P31:Q2977' ] ,
				[ 'chapelle' , 'P31:Q108325' ] ,
				[ 'église ' , 'P31:Q16970' ] ,
				[ ' gare ferroviaire' , 'P31:Q55488' ] ,
				[ 'pont' , 'P31:Q12280' ] ,
			]
		} ,
		
		{
			pre : 'Œuvre : ' ,
			options : [
				//[ '<b>œuvre</b>' , 'P107:Q386724' ] ,
				[ 'film' , 'P31:Q11424' ] ,
				[ 'album' , 'P31:Q482994' ] ,
				[ 'chanson' , 'P31:Q7366' ] ,
				[ 'livre' , 'P31:Q571' ] ,
				[ 'roman' , 'P31:Q571' , 'P136:Q8261' ] ,
				[ 'jeu de cartes' , 'P31:Q142714' ] ,
				[ 'jeu de société' , 'P31:Q131436' ] ,
				[ 'jeu vidéo' , 'P31:Q7889' ] ,
				[ 'série TV' , 'P31:Q5398426' ] ,
				[ 'théâtre' , 'P31:Q25379' ] ,
			]
		} ,

		{
			pre : 'Un : ' ,
			options : [
				//[ '<b>organisation</b>' , 'P107:Q43229' ] ,
				[ 'groupe de musique' , 'P31:Q215380' ] ,
				[ 'groupe de rock' , 'P31:Q5741069' ] ,
			]
		} ,
		
		{
			pre : 'Wiki : ' ,
			options : [
				[ 'catégorie' , 'P31:Q4167836' ] ,
				[ 'modèle' , 'P31:Q11266439' ] ,
				[ 'liste' , 'P31:Q13406463' ] ,
				[ 'homonymie' , 'P31:Q4167410' ] ,
			]
		} ,
		
	]
] ;
wd_useful_thisis_langs['it'] = [
	[
		{
			pre : 'Sesso : ' ,
			options : [
				[ '<b>essere umano</b>' , 'P31:Q5' ] ,
				[ '<b>pers. immaginario </b>' , 'P31:Q95074' ] ,
				[ 'maschio' , 'P21:Q6581097' ] ,//'P107:Q215627'
				[ 'femmina' , 'P21:Q6581072' ] ,//'P107:Q215627'
				[ 'intersessuato' , 'P21:Q1097630' ] ,
				[ 'transessuale uomo' , 'P21:Q15145783) ' ] ,
				[ 'transessuale femmina' , 'P21:Q15145782' ] ,
			]
		} ,
 
		{
			pre : 'Professione : ' ,
			options : [
				[ 'attore' , 'P106:Q33999' ] ,
				[ 'artista' , 'P106:Q483501' ] ,
				[ 'aviatore' , 'P106:Q2095549' ] ,
				[ 'avvocato' , 'P106:Q40348' ] ,
				[ 'cantante' , 'P106:Q177220' ] ,
				[ 'compositore' , 'P106:Q36834' ] ,
				[ 'drammaturgo' , 'P106:Q214917' ] ,
				[ 'economista' , 'P106:Q188094' ] ,
				[ 'esploratore' , 'P106:Q11900058' ] ,
				[ 'fotografo' , 'P106:Q33231' ] ,
				[ 'giornalista' , 'P106:Q1930187' ] ,
				[ 'giornalista sportivo' , 'P106:Q13219447' ] ,
				[ 'ingegnere' , 'P106:Q81096' ] ,
				[ 'inventore' , 'P106:Q205375' ] ,
				[ 'librettista' , 'p106:Q8178443' ] ,
				[ 'matematico' , 'P106:Q170790' ] ,
				[ 'modella' , 'p106:Q4610556' ] ,
				[ 'militare' , 'p106:Q4991371' ] ,
				[ 'musicista' , 'P106:Q639669' ] ,
				[ 'pittore' , 'P106:Q1028181' ] ,
				[ 'poeta' , 'P106:Q49757' ] ,
				[ 'prete cattolico' , 'P106:Q250867' ] ,
				[ 'politico' , 'P106:Q82955' ] ,
				[ 'regista cinematografico' , 'P106:Q2526255' ] ,
				[ 'regista teatrale' , 'P106:Q3387717' ] ,
				[ 'regista televisivo' , 'P106:Q2059704' ] ,
				[ 'scrittore' , 'P106:Q36180' ] ,
				[ 'scultore' , 'P106:Q1281618' ] ,
				[ 'storico' , 'P106:Q201788' ] ,
				[ 'ufficiale militare' , 'P106:Q189290' ] ,
			]
		} ,
 
		{
			pre : 'Funzione : ' ,
			options : [
				[ 'deputato' , 'P39:Q1055894' ] ,
				[ 'senatore' , 'P39:Q1672074' ] ,
				[ 'sindaco' , 'P39:Q30185' ] ,
				[ 'vescovo' , 'P39:Q29182' ] ,
				[ 'arcivescovo' , 'P39:Q49476' ] ,
				[ 'cardinale' , 'P39:Q45722' ] ,
				[ 'papa' , 'P39:Q19546' ] ,
			]
		} ,
 
		{
			pre : 'Sport :' ,
			options : [
				[ 'atleta' , 'P106:Q2066131' ] ,
				[ 'baseball' , 'P106:Q10871364' ] ,
				[ 'calciatore' , 'P106:Q937857' ] ,
				[ 'cestista' , 'P106:Q3665646' ] ,
				[ 'ciclista' , 'P106:Q2309784' ] ,
				[ 'ciclista su pista' , 'P106:Q15117395' ] ,
				[ 'cricket' , 'P106:Q5185059' ] ,
				[ 'pallavolista' , 'P106:Q15117302' ] ,
				[ 'rugbysta' , 'p106:q13415036' ] ,
				[ 'softball' , 'P106:Q13388586' ] ,
			]
		} ,
 
		{
			pre : 'Un : ' ,
			options : [
				[ 'specie' , 'P105:Q7432' ] ,//'P107:Q1969448'
				[ 'genere' , 'P105:Q34740' ] ,
				[ 'famiglia' , 'P105:Q35409' ] ,
				[ 'ordine' , 'P105:Q36602' ] ,
				[ 'classe' , 'P105:Q37517' ] ,
			]
		} ,
 
		{
			pre : 'Luogo : ' ,
			options : [
			    //[ '<b>lieu</b>' , 'P107:Q618123' ] ,
				[ 'catena montuosa' , 'P31:Q46831' ] ,
				[ 'lago' , 'P31:Q23397' ] ,
				[ 'montagna' , 'P31:Q8502' ] ,
				[ 'fiume' , 'P31:Q4022' ] ,
				[ 'castello' , 'P31:Q751876' ] ,
				[ 'forte' , 'P31:Q23413' ] ,
				[ 'maniero' , 'P31:Q879050' ] ,
				[ 'abbazia' , 'P31:Q160742' ] ,
				[ 'basilica' , 'P31:Q163687' ] ,
				[ 'cattedrale' , 'P31:Q2977' ] ,
				[ 'cappella' , 'P31:Q108325' ] ,
				[ 'chiesa ' , 'P31:Q16970' ] ,
				[ 'stazione ferroviria' , 'P31:Q55488' ] ,
				[ 'ponte' , 'P31:Q12280' ] ,
			]
		} ,
 
		{
			pre : 'Un : ' ,
			options : [
				//[ '<b>œuvre</b>' , 'P107:Q386724' ] ,
				[ 'film' , 'P31:Q11424' ] ,
				[ 'album' , 'P31:Q482994' ] ,
				[ 'canzone' , 'P31:Q7366' ] ,
				[ 'libro' , 'P31:Q571' ] ,
				[ 'romanzo' , 'P31:Q571' , 'P136:Q8261' ] ,
				[ 'gioco di carte' , 'P31:Q142714' ] ,
				[ 'gioco di società' , 'P31:Q131436' ] ,
				[ 'videogioco' , 'P31:Q7889' ] ,
				[ 'serie tv' , 'P31:Q5398426' ] ,
				[ 'opera teatrale' , 'P31:Q25379' ] ,
			]
		} ,
 
		{
			pre : 'Un : ' ,
			options : [
				//[ '<b>organisation</b>' , 'P107:Q43229' ] ,
				[ 'gruppo musicale' , 'P31: Q215380' ] ,
				[ 'gruppo rock' , 'P31: Q5741069' ] ,
			]
		} ,
 
		{
			pre : 'Wiki : ' ,
			options : [
				[ 'categoria' , 'P31:Q4167836' ] ,
				[ 'template' , 'P31:Q11266439' ] ,
				[ 'liste' , 'P31:Q13406463' ] ,
				[ 'Disambigua' , 'P31:Q4167410' ] ,
			]
		} ,
 
	]
] ;

wd_useful_thisis_langs['nb'] = [
	[
		{
			pre : 'en/et ' ,
			options : [
				[ '<b>menneske</b>' , 'P31:Q5' ] ,
				[ '<b>rollefigur </b>' , 'P31:Q95074' ] ,
				[ 'mann' , 'P21:Q6581097' , 'P31:Q5' ] , // , 'P107:Q215627'
				[ 'kvinne' , 'P21:Q6581072' , 'P31:Q5' ] , // , 'P107:Q215627' ,
				[ 'duo ' , 'P31:Q10648343' ] ,
			]
		} ,
		
		{
			pre : 'Yrker:' ,
			options : [
				[ 'skuespiller' , 'P106:Q33999' ] ,
				[ 'artist' , 'P106:Q483501' ] ,
				[ 'regissør' , 'P106:Q2526255' ] ,
				[ 'økonom' , 'P106:Q188094' ] ,
				[ 'advokat' , 'P106:Q40348' ] ,
				[ 'forfatter' , 'P106:Q482980' ] ,
				[ 'maler' , 'P106:Q1028181' ] ,
				[ 'politiker' , 'P106:Q82955' ] ,
				[ 'fotograf' , 'P106:Q33231' ] ,
				[ 'musiker' , 'P106:Q639669' ] ,
				[ 'journalist' , 'P106:Q1930187' ] ,
				[ 'matematiker' , 'P106:Q170790' ] ,
				[ 'skulptør' , 'P106:Q1281618' ] ,
				[ 'sanger' , 'P106:Q177220' ] ,
				[ 'sportsjournalist' , 'P106:Q13219447' ] ,
			]
		} ,
		
		{
			pre : 'Sport:' ,
			options : [
				[ 'idrettsutøver' , 'P106:Q2066131' ] ,
//				[ 'baseball' , 'P106:Q10871364' ] ,
				[ 'basketball' , 'P106:Q3665646' ] ,
				[ 'sykler' , 'P106:Q2309784' ] ,
//				[ 'cricket' , 'P106:Q12299841' ] ,
				[ 'fotball' , 'P106:Q937857' ] ,
				[ 'håndball' , 'P106:Q13365117' ],
				[ 'ishockey' , 'P106:Q11774891' ],
				[ 'golf' , 'P106:Q13156709' ],
//				[ 'track cyclist' , 'P106:Q15117395' ] ,
//				[ 'softball' , 'P106:Q13388586' ] ,
//				[ 'volley' , 'P106:Q15117302' ] ,
				[ 'langrenn' , 'P106:Q13382608' ] ,
				[ 'skiskytter' , 'P106:Q16029547' ],
				[ 'alpinist' , 'P106:Q4144610' ],
				[ 'skihopper', 'P106:Q13382603' ],
				[ 'skøyter' , 'P106:Q10866633' ] ,
				[ 'friidrett', 'P106:Q11513337' ] ,
			]
		} ,
		
		{
			pre : 'en ' ,
			options : [
				[ 'art' , 'P105:Q7432' ] , // , 'P107:Q1969448'
				[ 'slekt' , 'P105:Q34740' ] ,
				[ 'familie' , 'P105:Q35409' ] ,
				[ 'orden' , 'P105:Q36602' ] ,
				[ 'klasse' , 'P105:Q37517' ] ,
			]
		} ,
		
		{
			pre : 'en/et ' ,
			options : [
//				[ '<b>location</b>' , 'P107:Q618123' ] ,
				[ 'fjell' , 'P31:Q8502' ] ,
				[ 'fjellkjede' , 'P31:Q46831' ] ,
				[ 'elv' , 'P31:Q4022' ] ,
				[ 'innsjø' , 'P31:Q23397' ] ,
				[ 'jernbanestasjon' , 'P31:Q55488' ] ,
				[ 'bro' , 'P31:Q12280' ] ,
			]
		} ,
		
		{
			pre : 'en/ei/et ' ,
			options : [
//				[ '<b>work</b>' , 'P107:Q386724' ] ,
				[ 'film' , 'P31:Q11424' ] ,
				[ 'album' , 'P31:Q482994' ] ,
				[ 'sang' , 'P31:Q7366' ] ,
				[ 'bok' , 'P31:Q571' ] ,
				[ 'roman' , 'P31:Q571' , 'P136:Q8261' ] ,
				[ 'rollefigur' , 'P31:Q95074' ] ,
				[ 'videospill' , 'P31:Q7889' ] ,
				[ 'TV-serie' , 'P31:Q5398426' ] ,
				[ 'skuespill' , 'P31:Q25379' ] ,
			]
		} ,

		{
			pre : 'en/et ' ,
			options : [
//				[ '<b>organization</b>' , 'P107:Q43229' ] ,
				[ 'band' , 'P31:Q215380' ] ,
				[ 'rockeband' , 'P31:Q5741069' ] ,
				[ 'Kategori' , 'P31:Q4167836' ] ,
				[ 'Mal' , 'P31:Q11266439' ] ,
				[ 'Liste' , 'P31:Q13406463' ] ,
				[ 'Peker' , 'P31:Q4167410' ] ,
			]
		} ,
	]
] ;


// ***********************************************************************************************************************
// The section below contains the "Wikidata Usefuls" country list. Add new ones or edit existing ones as you see fit.
// ***********************************************************************************************************************

var wd_useful_countries = {
	"---" : "",
	"Afghanistan" : "Q889",
	"Albania" : "Q222",
	"Algeria" : "Q262",
	"Andorra" : "Q228",
	"Angola" : "Q916",
	"Antigua and Barbuda" : "Q781",
	"Argentina" : "Q414",
	"Armenia" : "Q399",
	"Australia" : "Q408",
	"Austria" : "Q40",
	"Azerbaijan" : "Q227",
	"Bahamas" : "Q778",
	"Bahrain" : "Q398",
	"Bangladesh" : "Q902",
	"Barbados" : "Q244",
	"Belarus" : "Q184",
	"Belgium" : "Q31",
	"Belize" : "Q242",
	"Benin" : "Q962",
	"Bhutan" : "Q917",
	"Bolivia" : "Q750",
	"Bosnia and Herzegovina" : "Q225",
	"Botswana" : "Q963",
	"Brazil" : "Q155",
	"Brunei" : "Q921",
	"Bulgaria" : "Q219",
	"Burkina Faso" : "Q965",
	"Burundi" : "Q967",
	"Cambodia" : "Q424",
	"Cameroon" : "Q1009",
	"Canada" : "Q16",
	"Cape Verde" : "Q1011",
	"Central African Republic" : "Q929",
	"Chad" : "Q657",
	"Chile" : "Q298",
	"China" : "Q148",
	"Colombia" : "Q739",
	"Comoros" : "Q970",
	"Costa Rica" : "Q800",
	"Côte d'Ivoire" : "Q1008",
	"Croatia" : "Q224",
	"Cuba" : "Q241",
	"Cyprus" : "Q229",
	"Czech Republic" : "Q213",
	"Democratic Republic of the Congo" : "Q974",
	"Denmark" : "Q35",
	"Djibouti" : "Q977",
	"Dominica" : "Q784",
	"Dominican Republic" : "Q786",
	"East Timor" : "Q574",
	"Ecuador" : "Q736",
	"Egypt" : "Q79",
	"El Salvador" : "Q792",
	"Equatorial Guinea" : "Q983",
	"Eritrea" : "Q986",
	"Estonia" : "Q191",
	"Ethiopia" : "Q115",
	"Federated States of Micronesia" : "Q702",
	"Fiji" : "Q712",
	"Finland" : "Q33",
	"France" : "Q142",
	"Gabon" : "Q1000",
	"Gambia" : "Q1005",
	"Georgia" : "Q230",
	"Germany" : "Q183",
	"Ghana" : "Q117",
	"Greece" : "Q41",
	"Grenada" : "Q769",
	"Guatemala" : "Q774",
	"Guinea" : "Q1006",
	"Guinea-Bissau" : "Q1007",
	"Guyana" : "Q734",
	"Haiti" : "Q790",
	"Honduras" : "Q783",
	"Hungary" : "Q28",
	"Iceland" : "Q189",
	"India" : "Q668",
	"Indonesia" : "Q252",
	"Iran" : "Q794",
	"Iraq" : "Q796",
	"Ireland" : "Q27",
	"Israel" : "Q801",
	"Italy" : "Q38",
	"Jamaica" : "Q766",
	"Japan" : "Q17",
	"Jordan" : "Q810",
	"Kazakhstan" : "Q232",
	"Kenya" : "Q114",
	"Kiribati" : "Q710",
	"Kuwait" : "Q817",
	"Kyrgyzstan" : "Q813",
	"Laos" : "Q819",
	"Latvia" : "Q211",
	"Lebanon" : "Q822",
	"Lesotho" : "Q1013",
	"Liberia" : "Q1014",
	"Libya" : "Q1016",
	"Liechtenstein" : "Q347",
	"Lithuania" : "Q37",
	"Luxembourg" : "Q32",
	"Macedonia" : "Q221",
	"Madagascar" : "Q1019",
	"Malawi" : "Q1020",
	"Malaysia" : "Q833",
	"Maldives" : "Q826",
	"Mali" : "Q912",
	"Malta" : "Q233",
	"Marshall Islands" : "Q709",
	"Mauritania" : "Q1025",
	"Mauritius" : "Q1027",
	"Mexico" : "Q96",
	"Moldova" : "Q217",
	"Mongolia" : "Q711",
	"Montenegro" : "Q236",
	"Monaco" : "Q235",
	"Morocco" : "Q1028",
	"Mozambique" : "Q1029",
	"Myanmar" : "Q836",
	"Namibia" : "Q1030",
	"Nauru" : "Q697",
	"Nepal" : "Q837",
	"Netherlands" : "Q55",
	"New Zealand" : "Q664",
	"Nicaragua" : "Q811",
	"Niger" : "Q1032",
	"Nigeria" : "Q1033",
	"North Korea" : "Q423",
	"Norway" : "Q20",
	"Oman" : "Q842",
	"Pakistan" : "Q843",
	"Palestine": "Q219060",
	"Palau" : "Q695",
	"Panama" : "Q804",
	"Papua New Guinea" : "Q691",
	"Paraguay" : "Q733",
	"Peru" : "Q419",
	"Philippines" : "Q928",
	"Poland" : "Q36",
	"Portugal" : "Q45",
	"Qatar" : "Q846",
	"Republic of the Congo" : "Q971",
	"Romania" : "Q218",
	"Russia" : "Q159",
	"Rwanda" : "Q1037",
	"Saint Kitts and Nevis" : "Q763",
	"Saint Lucia" : "Q760",
	"Saint Vincent and the Grenadines" : "Q757",
	"Samoa" : "Q683",
	"San Marino" : "Q238",
	"São Tomé and Príncipe" : "Q1039",
	"Saudi Arabia" : "Q851",
	"Senegal" : "Q1041",
	"Serbia" : "Q403",
	"Seychelles" : "Q1042",
	"Sierra Leone" : "Q1044",
	"Singapore" : "Q334",
	"Slovakia" : "Q214",
	"Slovenia" : "Q215",
	"Solomon Islands" : "Q685",
	"Somalia" : "Q1045",
	"South Africa" : "Q258",
	"South Korea" : "Q884",
	"South Sudan" : "Q958",
	"Spain" : "Q29",
	"Sri Lanka" : "Q854",
	"Sudan" : "Q1049",
	"Suriname" : "Q730",
	"Swaziland" : "Q1050",
	"Sweden" : "Q34",
	"Switzerland" : "Q39",
	"Syria" : "Q858",
	"Taiwan (Republic of China)" : "Q865",
	"Tajikistan" : "Q863",
	"Tanzania" : "Q924",
	"Thailand" : "Q869",
	"Togo" : "Q945",
	"Tonga" : "Q678",
	"Trinidad and Tobago" : "Q754",
	"Tunisia" : "Q948",
	"Turkey" : "Q43",
	"Turkmenistan" : "Q874",
	"Tuvalu" : "Q672",
	"Uganda" : "Q1036",
	"Ukraine" : "Q212",
	"United Arab Emirates" : "Q878",
	"United Kingdom" : "Q145",
	"United States" : "Q30",
	"Uruguay" : "Q77",
	"Uzbekistan" : "Q265",
	"Vanuatu" : "Q686",
	"Vatican City" : "Q237",
	"Venezuela" : "Q717",
	"Vietnam" : "Q881",
	"Yemen" : "Q805",
	"Zambia" : "Q953",
	"Zimbabwe" : "Q954",
	// Some former countries that are often encountered
	"Czechoslovakia" : "Q33946",
	"East Germany" : "Q16957",
	"Soviet Union" : "Q15180",
	"Yugoslavia" : "Q36704"
} ;

// ************************************************************************************************************************
// DO NOT EDIT BELOW THIS LINE UNLESS YOU REALLY KNOW WHAT YOU ARE DOING!
// ************************************************************************************************************************


var wd_useful_toolbar ;

let wikidata_useful = function () {

	if ( mw.config.get('wgNamespaceNumber') != 0 ) return ;
	if ( mw.config.get('wgAction') != 'view' ) return ;
	if ( mw.config.get('wgNamespaceNumber') != 0 ) return ;
	if ( mw.config.get('wbIsEditView') == false ) return ;
	if ( mw.config.get('wgIsRedirect') == true ) return ;

mw.loader.using(['vue', '@wikimedia/codex', 'jquery.ui', 'mediawiki.api'], function ( require ) {

	const WikidataUsefulApp = {
		template:`<div style='display: flex; font-size: 9pt;'>
		<div style='flex-grow: 1;'>
		<div v-for='section in links'>
		<div v-for='subsection in section' style='display:inline-flex; margin-bottom:3px'>
			<div style='margin-right:10px;'>{{subsection.pre}}</div>
			<div>
				<span v-for='(opt,idx) in subsection.options'>
					<span v-if='idx>0'> / </span>
					<a href='#' @click.prevent='addThis(opt)'>
						<b v-if='opt[0].substr(0,3)=="<b>"'>{{opt[0].substr(3,opt[0].length-7)}}</b>
						<span v-else>{{opt[0]}}</span>
					</a>
				</span>
			</div>
		</div>
		</div>
		</div>

		<div style='white-space: nowrap; margin-left: 0.2rem; padding-left: 0.2rem; border-left:1px dotted black;'>
			<div>
				<span v-for='(cc,num) in core_countries'>
					<span v-if='num>0'>|</span>
					<a href='#' @click.prevent='country=cc.q'>{{cc.name}}</a>
				</span>
			</div>
			<div>
				<select v-model='country' style='width: 100%;'>
					<option v-for='(q,name) in countries' :value='q'>{{name}}</option>
				</select>
			</div>
			<div v-if='country!=""'>
				<a href='#' @click.prevent='addThis(["dummy","P17:"+country])'>Country</a> |
				<a href='#' @click.prevent='addThis(["dummy","P27:"+country])'>Country of citizenship</a> |
				<a href='#' @click.prevent='addThis(["dummy","P495:"+country])'>Country of origin</a>
			</div>
			<!--
			<hr/>
			<div><a href='#' onclick='wd_useful.duplicateStatements();return false'>Duplicate statements to en category</a></div>
			<div><a href='#' onclick='wd_useful.duplicateStatementsFromItemList();return false'>Duplicate statements for items linked on Wikidata page</a></div>
			<div><a href='#' onclick='wd_useful.showImages();return false'>Show/add images used on Wikipedia</a></div>
			-->
		</div>
		</div>`,

		compatConfig: {
	        MODE: 3
	    },
	    compilerOptions: {
	        whitespace: 'condense'
	    },

		data() { return { links:[] , statements:[] , running:false , interval_handler:0 , country:'' , countries:{} ,
			core_countries : [ {q:'Q30',name:'U.S.'} , {q:'Q145',name:'U.K.'} , {q:'Q183',name:'Germany'} , {q:'Q142',name:'France'} , {q:'Q29',name:'Spain'} , {q:'Q38',name:'Italy'} , {q:'Q55',name:'Netherlands'} ] ,
		} } ,

		created : function () {
			this.constructLinks();
		} ,

		methods : {
			constructLinks : function () {
				let self = this ;

				self.countries = wd_useful_countries ;

				if ( typeof wd_useful_thisis_langs[wgUserLanguage]=='undefined' ) { // Fallback
					self.links =  JSON.parse(JSON.stringify(wd_useful_thisis_langs['en'])) ;
				} else {
					self.links =  JSON.parse(JSON.stringify(wd_useful_thisis_langs[wgUserLanguage])) ;
				}
				if ( typeof wd_useful_thisis_add != 'undefined' ) {
					self.links[0].push ( { pre:'' , options:JSON.parse(JSON.stringify(wd_useful_thisis_add)) } ) ;
				}

			} ,
			addThis : function ( opt ) {
				let self = this ;
				let statements = JSON.parse(JSON.stringify(opt)) ;
				statements.shift() ; // Label
				self.statements = self.statements.concat(statements) ;
				if ( self.interval_handler != 0 ) clearInterval(self.interval_handler);
				self.interval_handler = setInterval(self.addNextStatement, 500);
			} ,
			addNextStatement : function () {
				let self = this ;
				if ( self.running ) return ;
				if ( self.statements.length == 0 ) {
					clearInterval(self.interval_handler);
					self.interval_handler = 0 ;
					return ;
				}
				self.running = true ;
				let statement = self.statements.shift() ;
				let qualifiers = statement.split('|');
				qualifiers = qualifiers.map(s => s.split(':')) ;
				let mainsnak = qualifiers.shift() ;
				//console.log(mainsnak,qualifiers);
				let summary = '' ;
				wdutil.addItemStatement(mainsnak[0],mainsnak[1],qualifiers,summary,function(){
					self.running = false ;
				});
			}
		} ,

		components: {
		}
	}

	function show_tab() {
		wdutil_app.addTab({
			name: 'wikidata_useful',
			label: "Wikidata Useful",
		},function(id){
			const wikidata_useful_app = Vue.createMwApp(WikidataUsefulApp);
			wikidata_useful_app.mount(id);
		});
		return false ;
	}

	if ( typeof wd_useful_toolbar != 'undefined' && wd_useful_toolbar ) {
		let portletLink = mw.util.addPortletLink( 'p-tb', '#', 'Wikidata useful','wikitext-wd_ac');
		$(portletLink).click ( show_tab ) ;
	} else {
		show_tab() ;
	}

})}



// This is to use the tab display and wdutil
var wdutil_app ;
mw.loader.load('https://wikidata-todo.toolforge.org/wdutils.js');
//mw.loader.load('https://www.wikidata.org/w/index.php?title=User:Magnus_Manske/wdutil.js&action=raw&ctype=text/javascript');
if ( typeof wdutil!='undefined' ) wdutil.loadCallback(wikidata_useful);
else {
	var wdutil_loaded_callbacks ;
	if ( typeof wdutil_loaded_callbacks=='undefined' ) wdutil_loaded_callbacks = [] ;
	wdutil_loaded_callbacks.push(wikidata_useful);
}

// </nowiki>
