#!/usr/bin/php
<?PHP

require_once ( '/data/project/mix-n-match/public_html/php/ToolforgeCommon.php' ) ;

$tfc = new ToolforgeCommon('wikidata-todo') ;
$db = $tfc->openDBtool ( 'userscripts_p' ) ;
$dbwd = $tfc->openDBwiki('wikidatawiki');

$options  = array('http' => array('user_agent' => 'User script usage updater'));
$context  = stream_context_create($options);

function normalize_wikipage ( $page ) {
	$page = trim ( str_replace('_',' ',$page) ) ;
	return ucfirst ( str_replace ( '_' , ' ' , $page ) ) ;
}

function get_or_create_page_id($page,$last_touched) {
	global $user_pages , $db , $tfc ;
	$page = normalize_wikipage ( $page ) ;
	if ( isset($user_pages[$page]) ) return $user_pages[$page]->id ;
	$page_orig = $page ;
	$page = $db->real_escape_string ( $page ) ;
	$last_touched = $db->real_escape_string ( $last_touched ) ;
	$sql = "INSERT IGNORE INTO `user_page` (page,last_touched) VALUES ('{$page}','{$last_touched}')" ;
	$tfc->getSQL ( $db , $sql ) ;
	$user_pages[$page_orig] = (object) [ 'id'=> $db->insert_id , 'last_touched' => $last_touched ] ;
	return $db->insert_id ;
}

function get_or_create_script_id($path,$on_wiki) {
	global $scripts , $db , $tfc ;
	if ( isset($scripts[$path]) ) return $scripts[$path]->id ;
	$on_wiki *= 1 ;
	$path_orig = $path ;
	$path = $db->real_escape_string ( $path ) ;
	$sql = "INSERT IGNORE INTO `scripts` (path,on_wiki) VALUES ('{$path}',{$on_wiki})" ;
	$tfc->getSQL ( $db , $sql ) ;
	$scripts[$path_orig] = (object) [ 'id'=> $db->insert_id , 'on_wiki' => $on_wiki ] ;
	return $db->insert_id ;
}

$user_pages = [] ;
$sql = "SELECT * FROM user_page" ;
$result = $tfc->getSQL ( $db , $sql ) ;
while($o = $result->fetch_object()) $user_pages[normalize_wikipage($o->page)] = $o ;

$scripts = [] ;
$sql = "SELECT * FROM scripts" ;
$result = $tfc->getSQL ( $db , $sql ) ;
while($o = $result->fetch_object()) $scripts[$o->path] = $o ;

$pages_to_update = [] ;
$sql = "SELECT * FROM page WHERE page_namespace=2 AND page_title LIKE '%/common.js' AND page_is_redirect=0" ;
$result = $tfc->getSQL ( $dbwd , $sql ) ;
while($o = $result->fetch_object()) {
	$page_title = normalize_wikipage($o->page_title) ;
	if ( isset($user_pages[$page_title]) and $o->page_touched <= $user_pages[$page_title]->last_touched ) continue ;
	$pages_to_update[$page_title] = $o->page_touched ;
}

#$pages_to_update = [ 'User:Magnus Manske/common.js' => '0' ] ; # TESTING

foreach ( $pages_to_update AS $page => $last_touched ) {
	try {
		#$url = "https://www.wikidata.org/wiki/User:".urlencode(str_replace(' ','_',$page))."?action=raw" ;
		$url = "https://www.wikidata.org/w/api.php?action=parse&page=User:".urlencode(str_replace(' ','_',$page))."&prop=wikitext&format=json" ;
		$j = file_get_contents($url, false, $context);
		$j = json_decode($j) ;
		$j = $j->parse->wikitext->{'*'} ;
	} catch(Exception $e) {
		print "{$page}: " . $e->getMessage() . "\n" ;
		continue ;
	}

	$j = "\n{$j}\n" ;
	$j = preg_replace('|/\*.*?\*/s|','',$j) ;
	$j = preg_replace("|\s*;\s*//.*?\n|","\n",$j) ;
	$old_j = $j.'!' ;
	while ( $j!=$old_j ) {
		$old_j = $j ;
		$j = preg_replace("|\n\s*//.*?\n|","\n",$j) ;
	}

	$page_id = get_or_create_page_id ( $page , $last_touched ) ;
	$sql = "DELETE FROM script_use WHERE user_page_id={$page_id}" ;
	$tfc->getSQL ( $db , $sql ) ;

	preg_match_all('/(importScript|mw\.loader\.load)\s*\(\s*[\'"](.+?)[\'"]/', $j, $m) ;
	foreach ( $m[2] AS $path ) {
		$path = preg_replace('|^http[s]:|','',$path) ;
		if ( preg_match('|//www.wikidata.org(/.*)$|',$path,$m) ) $path = $m[1] ;
		if ( preg_match('|/w/index\.php\?title=(.+?)(&.*)$|',$path,$m) ) $path = $m[1] ;

		$on_wiki = true ;
		if ( preg_match('|^/|',$path) ) {
			// external script
			$on_wiki = false ;
		} else {
			// On-wiki script
			$path = normalize_wikipage($path);
		}

		$script_id = get_or_create_script_id($path,$on_wiki);
		$sql = "INSERT IGNORE INTO script_use (script_id,user_page_id) VALUES ({$script_id},{$page_id})" ;
		$tfc->getSQL ( $db , $sql ) ;
	}
	$sql = "UPDATE user_page SET last_touched='{$last_touched}' WHERE id={$page_id}" ;
	$tfc->getSQL ( $db , $sql ) ;
}

?>