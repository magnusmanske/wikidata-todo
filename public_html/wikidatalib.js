/**
	WikidataLib (c) 2014 by Magnus Manske
	GFDL 3+
 */


var WikidataLib = function ( params , callback ) {
	var self = this ;
	
	self.allowLabelOauthEdit = false ;
	self.reasonator_base = '//tools.wmflabs.org/reasonator/' ;
	
	self.infobox_properties = {
		main : [31,21,513,97,279,361,138,27,127,61,460,461,366,575,580,582,585,569,19,570,20,509,119,625] ,
		secondary : [301,155,156,485,518,527,26,40] ,
		media : [18,109,94,51,10,443,996]
	} ;

	self.getUrlVars = function () {
		var vars = {} ;
		var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1) ;
		var hash = window.location.href.slice(window.location.href.indexOf('#') + 1) ;
		if ( hash == window.location.href ) hash = '' ;
		if ( hash.length > 0 ) hashes = hash ;
		else hashes = hashes.replace ( /#$/ , '' ) ;
		hashes = hashes.split('&');
		$.each ( hashes , function ( i , j ) {
			var hash = j.split('=');
			hash[1] += '' ;
			vars[hash[0]] = decodeURI(hash[1]).replace(/_/g,' ');
		} ) ;
		return vars;
	}


	// CONSTRUCTOR BEGIN
	self.url_parameters = self.getUrlVars() ;
	
	$('head').append('<link rel="stylesheet" href="//tools.wmflabs.org/magnustools/resources/js/jquery/jquery.cluetip.css" type="text/css" />');
	$('head').append ( '<style>h3.ui-cluetip-header { line-height: 100%;font-size:1.5em;padding:2px;padding-left:10px;} .cluetip-inner div {margin-bottom:3px;}</style>' ) ;
	
	// Default parameters
	self.language = self.url_parameters.lang ;
	if ( self.language === undefined ) self.language = self.url_parameters.language ;
	if ( self.language === undefined ) self.language = 'en' ;
	self.isRTL = ( -1 < $.inArray ( self.language , [ 'fa','ar','ur','dv','he' ] ) ) ;
	self.verbose = false ;
	
	// Overwrite parameters with the ones passed to the constructor
	$.each ( params , function ( k , v ) { self[k] = v } ) ;
	
	// Load secondary scripts and initialise
	if ( typeof callback == 'undefined' ) callback = function () {} ; // Dummy callback
	var loading = 4 ;
	
	function init () {
		loading-- ;
		if ( loading > 0 ) return ;
		self.wd = new WikiData () ;
		self.wd.language = self.language ;
		wd_auto_desc.lang = self.language ;
		callback() ;
	}
	
	// Load language names
	$.getJSON ( '//www.wikidata.org/w/api.php?callback=?' , { // Get site info (languages)
		action:'query',
		meta:'siteinfo',
		siprop:'languages',
		format:'json'
	} , function ( d ) {
		self.all_languages = {} ;
		$.each ( d.query.languages , function ( k , v ) { self.all_languages[v.code] = v['*'] } ) ;
		init() ;
	} ) ;
	
	// Load autodesc
	if ( typeof wd_auto_desc == 'undefined' ) {
		$.getScript ( "//en.wikipedia.org/w/index.php?title=MediaWiki:Wdsearch-autodesc.js&action=raw&ctype=text/javascript" , function () {
			if ( self.verbose ) console.log ( "autodesc loaded" ) ;
			init () ;
		} ) ;
	} else {
		init() ;
	}
	
	// Load wikidata.js
	if ( typeof WikiDataItem == 'undefined' ) {
		$.getScript ( "//tools.wmflabs.org/magnustools/resources/js/wikidata.js" , function () {
			if ( self.verbose ) console.log ( "wikidata.js loaded" ) ;
			init() ;
		} ) ;
	} else {
		init() ;
	}
	
	// Load cluetip.js
	$.getScript ( "//tools.wmflabs.org/magnustools/resources/js/jquery/jquery.cluetip.min.js" , function () {
		if ( self.verbose ) console.log ( "cluetips loaded" ) ;
		init() ;
	} ) ;

	// CONSTRUCTOR END
	


	self.t = function ( key ) {
		return key ;
	}

	
	self.fixQ = function ( q ) {
		return 'Q'+(q+'').replace(/\D/g,'') ;
	}
	
	self.fixParams = function ( params , function_name ) {
		if ( typeof params == 'undefined' ) {
			console.log ( "WikidataLib:"+function_name+" needs parameter hash" ) ;
			return false ;
		}
		if ( typeof params.q == 'undefined' ) {
			console.log ( "WikidataLib:"+function_name+" needs parameter 'selector'" ) ;
			return false ;
		}
		if ( typeof params.selector == 'undefined' ) {
			if ( typeof params.id == 'undefined' ) {
				console.log ( "WikidataLib:"+function_name+" needs parameter 'selector'" ) ;
				return false ;
			}
			params.selector = '#' + params.id ;
		}
		return true ;
	}

	self.hasNoLabelInMainLanguage = function ( item ) {
		var ml = self.language ;
		if ( item.raw === undefined ) return true ;
		if ( item.raw.labels === undefined ) return true ;
		if ( item.raw.labels[ml] === undefined ) return true ;
		return false ;
	}

	
	self.showLabel = function ( params ) {
		if ( !self.fixParams ( params , 'showLabel' ) ) return ;
		var q = self.fixQ ( params.q ) ;
		$(params.selector).text ( q ) ;
		wd_auto_desc.labelItem ( q , function ( label ) {
			if ( typeof params.links == 'undefined' ) {
				$(params.selector).text ( label ) ;
			} else {
				var h = '' ;
				h += "<a" ;
				if ( typeof params.linktarget != 'undefined' ) h += " target='" + params.linktarget + "'" ;
				h += " href='" ;
				if ( params.links == 'wikidata' ) h += "//www.wikidata.org/wiki/" + q ;
				else if ( params.links == 'reasonator' ) h += self.reasonator_base + "?q=" + q + "&lang=" + self.language ;
				else console.log ( "WikidataLib:showLabel - wrong parameter links='" + params.links + "'" ) ;
				h += "'>" + label + "</a>" ;
				$(params.selector).html ( h ) ;
			}
		} ) ;
	}

	self.showDescription = function ( params ) {
		if ( !self.fixParams ( params , 'showDescription' ) ) return ;
		var q = self.fixQ ( params.q ) ;
		wd_auto_desc.loadItem ( q , { skip_cache:true , target:$(params.selector) , callback:params.callback , links:params.links , linktarget:params.linktarget , reasonator_lang:self.language } ) ;
	}
	
	self.addHoverbox = function ( params ) {
		if ( !self.fixParams ( params , 'addHoverbox' ) ) return ;
		var q = self.fixQ ( params.q ) ;
		var a = $(params.selector) ;
		var wikipedia_language = self.language ;

		var icons = {
			reasonator:'//upload.wikimedia.org/wikipedia/commons/thumb/e/e8/Reasonator_logo_proposal.png/18px-Reasonator_logo_proposal.png' ,
			wiki:'//upload.wikimedia.org/wikipedia/commons/thumb/8/80/Wikipedia-logo-v2.svg/18px-Wikipedia-logo-v2.svg.png' ,
			wikivoyage:'//upload.wikimedia.org/wikipedia/commons/thumb/8/8a/Wikivoyage-logo.svg/18px-Wikivoyage-logo.svg.png' ,
			wikisource:'//upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Wikisource-logo.svg/18px-Wikisource-logo.svg.png'
		} ;

		self.wd.getItemBatch ( [ q ] , function () {

			a.cluetip ( { // http://plugins.learningjquery.com/cluetip/#options
				splitTitle:'|',
				multiple : false,
				sticky : true ,
				mouseOutClose : 'both' ,
				cluetipClass : 'myclue' ,
				leftOffset : 0 ,
	//				delayedClose : 500 ,
				onShow : function(ct, ci) { // Outer/inner jQuery node
					var a = $(this) ;
					var i = self.wd.items[q] ;
					var title = i.getLabel ( self.language ) ;
					var dl = i.getLabelDefaultLanguage() ;
					var rank = a.hasClass('rank_deprecated') ? 'deprecated' : ( a.hasClass('rank_preferred') ? 'preferred' : 'normal' ) ;

					var h = "" ;
					h += "<div><span style='margin-right:10px;font-size:12pt'><a class='wikidata' target='_blank' href='//www.wikidata.org/wiki/"+q+"'>"+q+"</a></span>" ;

					h += "<span style='margin-left:5px'><a title='Reasonator' target='_blank' href='//tools.wmflabs.org/reasonator/?q="+q+"&lang="+self.language+"'><img border=0 src='"+icons['reasonator']+"'/></a></span>" ;				
					
					var sl = i.getWikiLinks() ;
					$.each ( [ 'wiki' , 'wikivoyage' , 'wikisource' ] , function ( dummy , site ) {
						var s2 = site=='wiki'?'wikipedia':site ;
						if ( sl[wikipedia_language+site] != undefined ) h += "<span style='margin-left:5px'><a title='"+self.t(s2)+" "+self.all_languages[wikipedia_language]+"' target='_blank' href='//"+wikipedia_language+"."+s2+".org/wiki/"+escape(sl[wikipedia_language+site].title)+"'><img border=0 src='"+icons[site]+"'/></a></span>" ;
					} ) ;
					var commons = i.getClaimObjectsForProperty ( 373 ) ;
					if ( commons.length > 0 ) {
						h += "<span style='margin-left:5px'><a title='"+self.t('commons_cat')+"' target='_blank' href='//commons.wikimedia.org/wiki/Category:"+escape(commons[0].s)+"'><img src='https://upload.wikimedia.org/wikipedia/commons/thumb/4/4a/Commons-logo.svg/18px-Commons-logo.svg.png' border=0 /></a></span>" ;
					}


					if ( self.showSearchMapLink && self.wd.items[q].hasClaims('P625') ) {
						var claims = self.wd.items[q].getClaimsForProperty ( 'P625' ) ;
						var lat = claims[0].mainsnak.datavalue.value.latitude ;
						var lon = claims[0].mainsnak.datavalue.value.longitude ;
						var url = '/wikidata-todo/around.html?lat='+lat+'&lon='+lon+'&radius=15&lang=' + self.getMainLang() ;
						h += "<span style='margin-left:5px'><a title='"+self.t('link_around_title')+"' href='"+url+"' target='_blank'><img border=0 src='"+self.map_icon_url+"'/></a></span>" ;
					
					}

					h += "</div>" ;
				
				
					if ( self.hasNoLabelInMainLanguage(i) ) {
						h += "<div class='internal missing_label'><i>" ;
						h += self.t('no_label_in').replace(/\$1/g,self.all_languages[wikipedia_language]||wikipedia_language) ;
						h += "</i>" ;
						if ( self.allowLabelOauthEdit ) {
							h += "<br/><a href='#' onclick='reasonator.addLabelOauth(\""+q+"\",\""+wikipedia_language+"\",\""+escattr(i.getLabel())+"\");return false'><b>" ;
							h += self.t('add_a_label') + "</b></a> (" + self.t('via_widar').replace(/\$1/,"<a target='_blank' href='/widar/'>WiDaR</a>") + ")" ;
						}
						h += "</div>" ;
					}

					h += "<div>" + i.getDesc(self.language) + "</div>" ;

					var adid = 'cluetip_autodesc_'+q ;
					h += "<div id='"+adid+"'>...</div>" ;
				
					// Rank
					if ( rank != 'normal' ) h += "<div>" + self.t('rank_label') + " : " + self.t('rank_'+rank) + "</div>" ;
			
			
					ct.css({'background-color':'white'}) ; // TODO use cluetipClass for real CSS
					var title_element = $(ct.find('h3')) ;
					title_element.html ( title ) ;
					ci.attr({q:q}) ;
					ci.html ( h ) ;
					if ( self.isRTL ) ci.css ( { 'direction':'RTL' } ) ;

					var images = i.getMultimediaFilesForProperty(18);
					if ( images.length > 0 ) {
						var img = images[0] ;
						$.getJSON ( '//commons.wikimedia.org/w/api.php?callback=?' , {
							action:'query',
							titles:'File:'+img,
							prop:'imageinfo',
							format:'json',
							iiprop:'url',
							iiurlwidth:120,
							iiurlheight:300
						} , function ( d ) {
							if ( d.query === undefined || d.query.pages === undefined ) return ;
							$.each ( d.query.pages , function ( dummy , v ) {
								if ( v.imageinfo === undefined ) return ;
								var ii = v.imageinfo[0] ;
								var h = "<div style='float:right'><img src='"+ii.thumburl+"' /></div>" ;
								if ( ci.attr('q') != q ) return ;
								$('#cluetip').css({width:'400px'});
								ci.prepend ( h ) ;
							} ) ;
						} ) ;
					}

					self.showDescription ( { q:q , selector:'#'+adid , links:'reasonator' , link_target:'_blank' } ) ;
				}
			} ) ;
			
		} ) ;
		
	}
	
	self.getRows = function ( i , prop , links ) {
		var ret = [] ;
		var datatype = self.wd.items[prop].raw.datatype ;
		var c = i.getClaimsForProperty(prop) ;
		if ( datatype == 'wikibase-item' ) {
			$.each ( c , function ( k2 , v2 ) {
				var s = i.getClaimTargetItemID(v2) ;
				var i2 = self.wd.items[s] ;
				if ( typeof i2 == 'undefined' ) return ;
				var text = i2.getLabel ( self.language ) ;
				var url ;
				if ( links.links == 'reasonator' ) {
					url = self.reasonator_base + "?q=" + v2 + "&lang=" + self.lang ;
				}
				
				if ( typeof url == 'undefined' ) {
					ret.push ( text ) ;
				} else {
					var r = "<a" ;
					r += " href='" + url + "'" ;
					r += ">" + text + "</a>" ;
					ret.push ( r ) ;
				}
				
//				if ( undefined !== self.wd.items[s] ) h2.push ( self.getItemLink ( { type:'item',q:s } , {ucfirst:true,desc:true,q_desc:true} ) ) ;
//				else h2.push ( s ) ;
			} ) ;
		} else if ( datatype == 'time' ) {
			$.each ( c , function ( k2 , v2 ) {
				var date = i.getClaimDate(v2) ;
				if ( date === undefined ) {
//					h2.push ( '???' ) ; // Unknown value
					return ;
				}
				var m = date.time.match ( /^([+-])0+(\d{4,})-(\d\d)-(\d\d)T/ ) ;
				if ( m == null ) {
					ret.push ( "MALFORMED DATE: " + date.time ) ;
				} else {
					var year = ( m[1] == '-' ) ? '-'+m[2] : ''+m[2] ;
					var s = '???' ;
					if ( date.precision >= 11 ) s = year+'-'+m[3]+'-'+m[4] ;
					else if ( date.precision == 10 ) s = year+'-'+m[3] ;
					else if ( date.precision == 9 ) s = year ;
					else if ( date.precision == 8 ) s = parseInt(year/10)+'0s' ;
					else if ( date.precision == 7 ) s = parseInt(year/100)+'00s' ;
//					var url = '?date='+s ;
//					url += self.getLangParam() ;
					ret.push ( s ) ;
				}
			} ) ;
		} else {
			console.log ( "Unknown data type " + datatype + " for property " + prop ) ;
		}
/*
		if ( v.type == 'string' ) {
			h2 = [] ;
			$.each ( c , function ( k2 , v2 ) {
				var s = i.getClaimTargetString(v2) ;
				if ( v.ucfirst ) s = ucFirst ( s ) ;
				if ( undefined !== s && s != '' ) h2.push ( s ) ;
			} ) ;
			h2 = h2.join ( "<br/>" ) ;
			if ( h2 == '' ) h2 = v.default || '' ;
		} else if ( v.type == 'quantity' ) {

			h2 = self.renderQuantity ( v ) ;
			
		} else {
			h2 = [] ;
			$.each ( c , function ( k2 , v2 ) {
				var s = i.getClaimTargetItemID(v2) ;
				if ( undefined !== self.wd.items[s] ) h2.push ( self.getItemLink ( { type:'item',q:s } , {ucfirst:true,desc:true,q_desc:true} ) ) ;
				else h2.push ( s ) ;
			} ) ;
			h2 = h2.join ( "<br/>" ) ;
			if ( h2 == '' ) h2 = v.default || '' ;
//							c = i.getClaimTargetItemID(c[0]) ;
//							if ( undefined !== self.wd.items[c] ) h2 = self.getItemLink ( { type:'item',q:c } , {ucfirst:true,desc:true,q_desc:true} ) ;
		}
*/
		return ret ;
	}

	self.showInfobox = function ( params ) {
		if ( !self.fixParams ( params , 'showInfobox' ) ) return ;
		var q = self.fixQ ( params.q ) ;
		var div = $(params.selector) ;
		
		if ( typeof params.css == 'undefined' ) params.css = { padding:'2px' , margin:'5px' , border:'2px solid #DDD' , 'max-width':'400px' } ;
		div.css ( params.css ) ;
		div.html ( '<i>Loading infobox...</i>' ) ;
		
		self.wd.getItemBatch ( [ q ] , function () {
			var i = self.wd.items[q] ;
			var title = i.getLabel ( self.language ) ;
			var h = '' ;
			h += "<table border='solid 1px #DDD' cellspacing=0 cellpadding=2'><tbody class='wdl_infobox'>" ;
			
			h += "<tr><th colspan=2>" + title + "</th></tr>" ;
			
			if ( params.description ) {
				h += "<tr><td colspan=2 class='wdl_infobox_desc'></td></tr>" ;
			}
			
			var props = [] ;
			$.each ( self.infobox_properties.media , function ( k , v ) { props.push ( v ) } ) ;
			$.each ( self.infobox_properties.main , function ( k , v ) { props.push ( v ) } ) ;
			$.each ( self.infobox_properties.secondary , function ( k , v ) { props.push ( v ) } ) ;
			var get_items = [] ;
			var props2 = [] ;
			$.each ( props , function ( k , prop ) {
				prop = 'P' + prop ;
				if ( !i.hasClaims(prop) ) return ;
				get_items.push ( prop ) ;
				props2.push ( prop ) ;
				$.each ( i.getClaimItemsForProperty ( prop , true ) , function ( k2 , q2 ) {
					get_items.push ( q2 ) ;
				} ) ;
			} ) ;
			
			self.wd.getItemBatch ( get_items , function () {
				$.each ( props2 , function ( k , prop ) {
					var rows = self.getRows ( i , prop , (params.valuelinks||{}) ) ;
					
				
					if ( rows.length == 0 ) return ;
					h += "<tr>" ;
					h += "<td rowspan='"+rows.length+"' class='wdl_infobox_property'>" + self.wd.items[prop].getLabel ( self.language ) + "</td>" ;
					$.each ( rows , function ( k3 , v3 ) {
						if ( k3 > 0 ) h += "<tr>" ;
						h += "<td class='wdl_infobox_value'>" + v3 + "</td>" ;
						h += "</tr>" ;
					} ) ;
				} ) ;
				
			
				h += "</tbody></table>" ;
				div.html ( h ) ;

				if ( params.description ) {
					self.showDescription ( { q:q , selector:params.selector+' td.wdl_infobox_desc' } ) ;
				}
			} ) ;
			
		} ) ;
		
	}
	
	
} ;
