#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR); // E_ALL|

require_once ( '../../public_html/php/common.php' ) ;

$db = openDB ( 'wikidata' , 'wikidata' , true ) ;
$dbsw = openDB ( 'en' , 'wikispecies' , true ) ;

$fh = fopen ( "botanists.add" , 'w' ) ;

#$people = explode ( "\n" , file_get_contents ( 'people.tab' ) ) ;

$botanists = getPagesInCategory ( $dbsw , 'Botanists' , 10 , 0 , true ) ;

$sql = "SELECT ips_site_page FROM wb_items_per_site WHERE ips_site_id='specieswiki'" ;
if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
while($o = $result->fetch_object()){
	$p = str_replace ( ' ' , '_' , $o->ips_site_page ) ;
	if ( isset ( $botanists[$p] ) ) unset ( $botanists[$p] ) ;
}

#$botanists = array ( 'Girolamo_Giardina' ) ;

foreach ( $botanists AS $p ) {
	$w = file_get_contents ( "https://species.wikimedia.org/w/index.php?title=" . myurlencode(str_replace(' ','_',$p)) . "&action=raw" ) ;
	$w = str_replace ( "\n" , '!' , $w ) ;

#print "Checking $p\n" ;
#print "$w\n" ;
	$m = '' ;
	$key = '' ;
	if ( preg_match ( '/\b(form|abbreviation)\s*:\s*(.+?)\s*!/i' , $w , $m ) ) $key = $m[2] ;
	else if ( preg_match ( '/\bbotanist_name\s*=\s*(.+?)\s*!/i' , $w , $m ) ) $key = $m[1] ;
	else if ( preg_match ( "/\('''(.+?)'''\)/" , $w , $m ) ) $key = preg_replace ( '/\s+/' , '' , $m[1] ) ;
	else if ( preg_match ( "/'''\((.+?)\)'''/" , $w , $m ) ) $key = preg_replace ( '/\s+/' , '' , $m[1] ) ;
	else if ( preg_match ( "/\('''\[\[(.+?)\]\]'''\)/" , $w , $m ) ) $key = preg_replace ( '/\s+/' , '' , $m[1] ) ;
	else if ( preg_match ( "/'''\(\[\[(.+?)\]\]\)'''/" , $w , $m ) ) $key = preg_replace ( '/\s+/' , '' , $m[1] ) ;
	else continue ;

	
	$key = preg_replace ( '/\{\{aut\|/' , '' , $key ) ;
	$key = preg_replace ( '/[ \[\]\{\}]/' , '' , $key ) ;
	$key = preg_replace ( '/\'{2,}/' , '' , $key ) ;
#print "$key\n" ;

	$j = getSPARQLitems ( 'select distinct ?item where { ?item wdt:P428 "' . $key . '" }' , 'item' ) ;

	if ( count ( $j ) != 1 ) continue ;
	$q = $j[0] ;
	$s = "Q$q\tSspecieswiki\t\"" . str_replace ( '_' , ' ' , $p ) . "\"\n" ;

	fwrite ( $fh , $s ) ;
}

fclose ( $fh ) ;

?>