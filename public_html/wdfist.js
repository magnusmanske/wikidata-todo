// <nowiki>
/*
To add this gadget, put the following line on your common.js User subpage:

 importScript( 'User:Magnus_Manske/wdfist.js' );

This script will runn automatically on EVERY item you visit, if it has associated Wikipedia pages and lack an image (P18).
To prevent this and use a sidebar tool link instead, set:

 var wdfist_as_sidebar_link = true ;

*/

var wdfist_as_sidebar_link ;

let wdfist_gadget = function () {

mw.loader.using(['vue', '@wikimedia/codex', 'jquery.ui', 'mediawiki.api'], function ( require ) {
	
	
	const CdxThumbnail = require( '@wikimedia/codex' ).CdxThumbnail;
	const Thumbnail = require( '@wikimedia/codex' ).Thumbnail;

	const WDfistApp = {
		template: `<div id='wdfist_gadget_container' style='padding: 0.2rem; display:flex; flex-wrap: wrap;'>
<div v-for='file in files' style='display:flex; margin:5px;'>
	<!-- THIS SHOULD BE <cdx-thumbnail :thumbnail='file'></cdx-thumbnail> BUT THE SIZE IS WRONG -->
	<div :style='"max-width:"+file.width+"px;"'>
		<div :style='"width:"+file.width+"px; height:"+file.height+"px; margin-bottom:2px; padding:2px; display:flex; justify-content:center; align-items:center; border:1px solid #DDD"'>
			<div>
				<a :title='file.filename' :href='file.page_url' target='_blank'>
					<img :src='file.url' :style='"max-height:"+file.height+"px"' border=0 />
					</a>
			</div>
		</div>
		<div style='text-align: center'>
			Add as
			<span v-for='al in add_links' style='margin-right: 0.1rem;'>
				<a @click.prevent='addImage(al.property,file)'>{{al.label}}</a>
			</span>
		</div>
	</div>
</div>
</div>`,
		data() { return { files:[] , thumbnail_width:120 , add_links:[{property:"P18",label:"image"}] } },
		created : function () {
			let self = this ;
			let tmp = wdfist_results.map(function(filename){
				let thumbnailData = {
					width:self.thumbnail_width,
					height:self.thumbnail_width,
					filename:filename,
					url:'https://commons.wikimedia.org/wiki/Special:Redirect/file/'+encodeURIComponent(filename)+'?width='+self.thumbnail_width,
					page_url:'https://commons.wikimedia.org/wiki/File:'+encodeURIComponent(filename)
				};
				return thumbnailData ;
			});
			self.files = tmp ;
		} ,
		methods: {
			addImage : function ( property , file ) {
				wdutil.addExternalIdStatement ( property , file.filename.replace(/_/g,' ') , "Added via wdfist gadget" , function(){
					//console.log("OK");
				} ) ;
			}
		},
		components: {
			CdxThumbnail,
			Thumbnail,
		},
	};

	// Code to run on page load
	if ( mw.config.get('wgNamespaceNumber') != 0 ) return ;
	if ( mw.config.get('wgAction') != 'view' ) return ;

	wdutil_app.addTab({
		name: 'wdfist_gadget',
		label: 'WD-FIST',
	},function(id){
		const wdfist_gadget_app = Vue.createMwApp(WDfistApp);
		wdfist_gadget_app.mount(id);
	});

})
};

var wdfist_results = [] ;
var wdutil_app ;
var wdutil_loaded_callbacks ;
function wdfist_gadget_add_tab () {
	//mw.loader.load('https://wikidata-todo.toolforge.org/wdutils.js');
	mw.loader.load('https://www.wikidata.org/w/index.php?title=User:Magnus_Manske/wdutil.js&action=raw&ctype=text/javascript');
	if ( typeof wdutil!='undefined' ) {
		wdutil.loadCallback(wdfist_gadget);
	} else {
		if ( typeof wdutil_loaded_callbacks=='undefined' ) wdutil_loaded_callbacks = [] ;
		wdutil_loaded_callbacks.push(wdfist_gadget);
	}
}

function wdfist_check_petscan() {
	let q = mw.config.get("wgTitle") ;
	//console.log("Checking PetScan...");
	$.getJSON('https://petscan.wmflabs.org/?callback=?',{
		wdf_main:1,
		doit:1,
		format:'json',
		manual_list:q,
		manual_list_wiki:'wikidatawiki',
		wdf_langlinks:1
	},function(d){
		//console.log(d);
		if ( (d.status||'') != 'OK' ) return ; // PetScan issue
		if ( typeof d.data=='undefined' ) return ;
		if ( typeof d.data[q]=='undefined' ) return ;
		if ( $(d.data[q]||{}).length == 0 ) return ; // No results
		let tmp = Object.keys(d.data[q]) ;

		// Remove images that are already used in the item
		tmp = tmp.filter(function(filename){
			let url = "https://commons.wikimedia.org/wiki/File:"+encodeURIComponent(filename.replace(/ /g,'_')) ;
			return $('a.image[href="'+url+'"]').length==0
		});
		
		if ( tmp.length == 0 ) return ;
		wdfist_results = tmp ;
		wdfist_gadget_add_tab();
	});	
}

// Only run if the item has sitelinks but no image
if ( $('span.wikibase-sitelinkview-page').length > 0 && $('#P18').length == 0 ) {
	if ( typeof wdfist_as_sidebar_link!='undefined' && wdfist_as_sidebar_link ) {
		let portletLink = mw.util.addPortletLink( 'p-tb', '#', 'WD-FIST','wd-fist');
		$(portletLink).click ( wdfist_check_petscan ) ;
	} else {
		wdfist_check_petscan();
	}
}
// </nowiki>
