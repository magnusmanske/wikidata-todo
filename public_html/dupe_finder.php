<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');
ini_set('memory_limit','2500M');
set_time_limit ( 60 * 10 ) ; // Seconds
include_once ( "php/common.php" ) ;

$site = trim ( strtolower ( get_request ( 'site' , 'enwiki' ) ) ) ;
$limit = get_request ( 'limit' , 0 ) * 1 ;
$testing = isset($_REQUEST['test']) ;

print get_common_header ( '' , 'Wikidata duplicate item finder' ) ;
print "
<div class='lead'>This tool finds potential duplicate items on Wikidata, via identical labels/aliases.</div>
<form method='get' class='form-inline'>
<div>Site: <input type='text' value='".htmlspecialchars($site)."' name='site' /><input type='submit' name='run' value='Do it' class='btn btn-outline-primary' /></div>
<div style='display:block'>Limit: <input name='limit' type='number' placeholder='e.g. 1000' /> (optional, for testing; number of initial candidates, not necessarily results)</div>
</form>
<div>
<i>Note:</i> For large wikis such as enwiki, this will take several minutes to run. Please be patient and do not hit reload in vain!
</div>
" ;

if ( !isset($_REQUEST['run']) ) {
	print get_common_footer() ;
	exit ( 0 ) ;
}

$db = openDB ( 'wikidata' , 'wikidata' ) ;
$site = $db->real_escape_string ( $site ) ;
$wp_types = "'Q4167410','Q13406463','Q4167836','Q11266439'" ;

$sql = "select ips_item_id,group_concat(ips_site_id separator '|') AS sites,group_concat(ips_site_page separator '|') AS pages,count(*) AS cnt from wb_items_per_site  group by ips_item_id having cnt=1 and sites='$site'" ;
if ( $limit > 0 ) $sql .= " LIMIT $limit" ;

$items = array() ;
$result = getSQL ( $db , $sql ) ;
while($o = $result->fetch_object()){
	if ( preg_match ( '/:/' , $o->pages ) ) continue ;
	$items[$o->ips_item_id] = preg_replace ( '/\s+\(.+\)$/' , '' , $o->pages ) ;
}

if ( $testing ) {
	print "<p>".count($items)." items found.</p>" ;
	#print "<pre>" ; print_r ( $items ) ; print "</pre>" ;
}

// Remove disambig etc.
$sql = "SELECT DISTINCT page_title FROM pagelinks,page,linktarget WHERE pl_target_id=lt_id AND page_namespace=0 AND page_title IN ('Q" . implode("','Q",array_keys($items)) . "') and page_id=pl_from AND lt_namespace=0 AND lt_title IN ($wp_types)" ;
if ( $testing ) {
	print "<pre>" ; print $sql ; print "</pre>" ;
}
$result = getSQL ( $db , $sql ) ;
while($o = $result->fetch_object()){
	$tmp_q = preg_replace ( '/\D/' , '' , $o->page_title ) ;
	if ( $testing ) print "Removing $tmp_q<br/>" ;
	unset ( $items[$tmp_q] ) ;
}

print "<div>Found " . number_format(count($items),0) . " items unique to $site.</div>" ;
myflush() ;


$had_label = array() ;
print "<ol>" ;
foreach ( $items AS $q => $page ) {
	$s = $db->real_escape_string ( $page ) ;
	if ( isset ( $had_label[$s] ) ) continue ;
	$had_label[$s] = 1 ;
	$sql = "SELECT DISTINCT concat('Q',wbit_item_id) AS term_full_entity_id
		FROM wbt_item_terms,wbt_term_in_lang,wbt_text_in_lang,wbt_text
		WHERE wbit_term_in_lang_id = wbtl_id AND wbtl_type_id IN (1,3) /* label or alias */
		AND wbtl_text_in_lang_id = wbxl_id
		AND wbxl_text_id = wbx_id
		AND wbx_text='{$s}'
		AND wbit_item_id!={$q}" ;
	$sql .= " AND NOT EXISTS (SELECT * FROM  pagelinks,page,linktarget WHERE pl_target_id=lt_id AND page_namespace=0 AND concat('Q',wbit_item_id)=page_title and page_id=pl_from AND lt_namespace=0 AND lt_title IN ($wp_types))" ; // Disambig etc
	$sql .= " AND NOT EXISTS (SELECT * FROM wb_items_per_site WHERE ips_item_id=wbit_item_id AND ips_site_id='{$site}')" ; // No link to same wiki
	$result = getSQL ( $db , $sql ) ;
	$found = [] ;
	while($o = $result->fetch_object()){
		$qq = $o->term_full_entity_id ;
		$found[] = "<a target='_blank' href='//www.wikidata.org/wiki/$qq'>$qq</a>" ;
	}
	if ( count ( $found ) == 0 ) continue ;
	print "<li>" ;
	print "Item <a href='//www.wikidata.org/wiki/Q$q' target='_blank'>Q$q</a> (<i>$page</i>) has potential duplicates: " ;
	print implode ( ", " , $found ) ;
	print "</li>" ;
	myflush() ;
}
print "</ol>" ;

print get_common_footer() ;

?>