#!/usr/bin/php
<?PHP

require_once ( '/data/project/wikidata-todo/public_html/php/ToolforgeCommon.php' ) ;
require_once ( '/data/project/wikidata-todo/public_html/php/wikidata.php' ) ;
require_once ( '/data/project/quickstatements/public_html/quickstatements.php' ) ;

class DenkmallisteHessen {
	public $tfc ;
	protected $update_existing_entries = false;
	protected $existing_ids = [];
	protected $table = 'denkmalliste_hessen';
	protected $qs;
	protected $p31 = [
		'|friedhof\b|i' => 'Q39614',
		'|brücke\b|i' => 'Q12280',
		'|kirche\b|i' => 'Q16970',
		'|schule\b|i' => 'Q1244442',
	];

	function __construct () {
		$ini_file = '/data/project/wikidata-todo/reinheitsgebot.conf';
		$this->tfc = new ToolforgeCommon ( 'denkmallisten_hessen_magnus' ) ;
		$this->qs = $this->tfc->getQS('denkmallisten_hessen_magnus',$ini_file) ;
		$this->qs->config->bot_config_file = $ini_file;
		$this->qs->use_oauth = false ;
		$this->tfc->dbt = $this->tfc->openDBtool ( 'wikidata_cache_p' ) ;
	}

	protected function process_row ( $row ) {
		foreach ( $row AS $k => $v ) {
			$v = str_replace('&zwnj;','',$v);
			$v = strip_tags($v);
			$v = html_entity_decode($v);
			if ( strlen($v)>250 ) $v = substr($v,0,250).'...';
			$row->$k = $v;
		}
		if ( isset($this->existing_ids[$v->id]) ) {
			if ( $this->update_existing_entries ) {
				die("update_existing_entries not implemented yet\n");
			}
		} else {
			$sql = "INSERT IGNORE INTO `{$this->table}` (id,strassenname,normstrassenname,flur,flurstk,kdname,ort,kreis,ortsteil,definition,begruendung,denkmalwert) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)" ;
			$stmt = $this->tfc->dbt->prepare($sql);
			$stmt->bind_param('dsssssssssss', $row->id,$row->strassenname,$row->normstrassenname,$row->flur,$row->flurstk,$row->kdname,$row->ort,$row->kreis,$row->ortsteil,$row->definition,$row->begruendung,$row->denkmalwert);
			$stmt->execute();
			$this->existing_ids[$row->id] = 1;
		}
	}

	protected function import_existing_ids() {
		$sql = "SELECT `id` FROM `{$this->table}`" ;
		$result = $this->tfc->getSQL ( $this->tfc->dbt , $sql ) ;
		while($o = $result->fetch_object()) $this->existing_ids["{$o->id}"] = 1;	
	}

	public function update_tool_db_from_denkmalpflege_hessen() {
		$this->import_existing_ids();
		$batch_size = 100;
		$total = 1000000; # absurdly high
		$loaded = 0 ;
		while ( $loaded < $total ) {
			$url = "https://denkxweb.denkmalpflege-hessen.de/getData/?offset={$loaded}&limit={$batch_size}&strassenname=None&kdname=None&ort=None&kreis=None&begruendung=None&datierungvon=None&datierungbis=None&definition=None&denkmalwert=" ;
			$j = json_decode(file_get_contents($url));
			if ( !isset($j->total) ) {
				print "BAD JSON:\n";
				print_r($j) ;
				sleep(10);
				continue;
			}
			$total = $j->total ;
			$new_entries = 0 ;
			foreach ( $j->rows AS $row ) {
				$this->process_row($row);
				$new_entries++ ;
			}
			if ( $new_entries==0 ) {
				print "Premature abort? ({$loaded})\n" ;
				exit(0);
			}
			$loaded += $new_entries;
		}
	}

	protected function update_ortsteil() {
		$sparql_cache = [] ;
		$sql = "SELECT `id`,`ortsteil`,`ort_q` FROM `{$this->table}` WHERE `ort`!=`ortsteil` AND `ortsteil`!='' AND `ort_q` IS NOT NULL AND `ortsteil_q` IS NULL" ;
		$result = $this->tfc->getSQL ( $this->tfc->dbt , $sql ) ;
		while($o = $result->fetch_object()) {
			$ortsteil = $this->tfc->dbt->real_escape_string($o->ortsteil);
			$sparql = "SELECT ?q { ?q wdt:P131* wd:Q{$o->ort_q}. ?q rdfs:label \"{$ortsteil}\"@de }" ;
			if ( isset($sparql_cache[$sparql]) ) {
				$items = $sparql_cache[$sparql];
			} else {
				$items = $this->tfc->getSPARQLitems($sparql,'q');
				$sparql_cache[$sparql] = $items;
			}
			if ( count($items)!=1 ) continue;
			$q = str_replace('Q','',$items[0])*1;
			$sql = "UPDATE `{$this->table}` SET `ortsteil_q`={$q} WHERE `id`={$o->id}" ;
			$this->tfc->getSQL ( $this->tfc->dbt , $sql ) ;
		}
	}

	protected function update_kreis_ort() {
		foreach ( ['kreis','ort'] AS $key ) {
			$key_q = "{$key}_q";
			$name2q = [] ;
			$sql = "SELECT DISTINCT `{$key}` AS `name`,`{$key_q}` AS `q` FROM `{$this->table}` WHERE `{$key_q}` IS NOT null";
			$result = $this->tfc->getSQL ( $this->tfc->dbt , $sql ) ;
			while($o = $result->fetch_object()) $name2q[$o->name] = $o->q ;
			foreach ( $name2q AS $name=>$q ) {
				$name = $this->tfc->dbt->real_escape_string($name);
				$sql = "UPDATE `{$this->table}` SET `{$key_q}`={$q} WHERE `{$key_q}` IS NULL AND `{$key}`=\"{$name}\"" ;
				$this->tfc->getSQL ( $this->tfc->dbt , $sql );
			}
		}
	}

	public function update_metadata_matchings() {
		$this->update_kreis_ort() ;
		$this->update_ortsteil();
	}

	public function sync_from_wikidata() {
		$in_db = [] ;
		$sql = "SELECT id,q FROM `{$this->table}` WHERE q IS NOT NULL" ;
		$result = $this->tfc->getSQL ( $this->tfc->dbt , $sql ) ;
		while($o = $result->fetch_object()) $in_db["{$o->id}"] = $o->q ;

		$with_image = [] ;
		$sparql = "SELECT ?q ?id ?img { ?q wdt:P1769 ?id OPTIONAL { ?q wdt:P18 ?img } }" ;
		foreach ( $this->tfc->getSPARQL_TSV($sparql) AS $o ) {
			$q = str_replace('Q','',$this->tfc->parseItemFromURL($o['q']))*1;
			$id = $o['id']*1;
			if ( !isset($in_db[$id]) or $in_db[$id]!=$q ) {
				$sql = "UPDATE `{$this->table}` SET `q`={$q} WHERE `id`={$id} AND (`q` IS NULL OR `q`!={$q})" ;
				$this->tfc->getSQL ( $this->tfc->dbt , $sql );
			}
			if ( $o['img']!='' ) $with_image[] = $id ;
		}
		$sql = "UPDATE `{$this->table}` SET `has_image`=1 WHERE `id` IN (".implode(',',$with_image).") AND `has_image`=0" ;
		$this->tfc->getSQL ( $this->tfc->dbt , $sql );

	}

	public function import_coordinates() {
		$sql = "SELECT * FROM `{$this->table}` WHERE `coordinates_checked`=0" ;
		#$sql = "SELECT * FROM `{$this->table}` WHERE id=2576" ; # TESTING
		$result = $this->tfc->getSQL ( $this->tfc->dbt , $sql ) ;
		while($o = $result->fetch_object()) {
			$sql = "UPDATE `{$this->table}` SET `coordinates_checked`=1 WHERE `id`={$o->id}" ;
			$this->tfc->getSQL ( $this->tfc->dbt , $sql ) ;
			$url = "https://denkxweb.denkmalpflege-hessen.de/work/getStrassenObj/{$o->id}";
			$j = json_decode(file_get_contents($url));
			if ( !isset($j->features) ) {
				$url = "https://denkxweb.denkmalpflege-hessen.de/work/getStrassenRefpt/{$o->id}";
				$j = json_decode(file_get_contents($url));
			}
			#print "{$url}\n" ;
			if ( !isset($j->features) ) continue;
			foreach ( $j->features AS $f ) {
				if ( !isset($f->geometry) ) continue;
				$g = $f->geometry;
				if ( !isset($g->coordinates) ) continue;
				$c = $g->coordinates;
				if ( is_array($c[0]) ) {
					$c = $c[0];
					if ( count($c) == 0 ) continue;
					$lat = 0 ;
					$lon = 0 ;
					$cnt = 0 ;
					if ( is_array($c) and !is_array($c[0]) ) $c = [$c];
					else if ( is_array($c) and is_array($c[0]) ) $c = $c[0];
					foreach ( $c AS $coord ) {
						if ( !is_array($coord) ) continue;
						if ( count($coord)<2 ) continue ;
						if ( !is_numeric($coord[0]) ) continue ;
						$cnt++ ;
						$new_lat = $coord[1]*1;
						$new_lon = $coord[0]*1;
						$lat += $new_lat;
						$lon += $new_lon;
					}
					#print "{$lat}/{$lon}/{$cnt}\n" ;
					if ( $cnt == 0 ) continue ;
					$lat /= $cnt;
					$lon /= $cnt;
				} else {
					$lat = $c[1];
					$lon = $c[0];
				}
				if ( $lat==0 or $lon==0 ) continue;
				if ( strlen($polygon)>250 ) $polygon = '' ; # better than broken JSON
				$sql = "UPDATE `{$this->table}` SET `lat`={$lat},`lon`={$lon} WHERE `id`={$o->id}" ;
				$this->tfc->getSQL ( $this->tfc->dbt , $sql ) ;
			}
		}
	}

	protected function getLabelForItem($q) {
		$i = $this->wil->getItem($q);
		if ( !isset($i) ) {
			$this->wil->loadItem($q);
			$i = $this->wil->getItem($q);
		}
		if ( !isset($i) ) return "Q{$q}"; # Fallback
		return $i->getLabel('de',true);
	}

	public function create_new_items() {
		# Paranoia
		$sql = "SELECT * FROM `{$this->table}` WHERE ortsteil_q is not null and ort_q is null UNION SELECT * FROM `{$this->table}` WHERE ort_q is not null and kreis_q is null";
		$result = $this->tfc->getSQL ( $this->tfc->dbt , $sql ) ;
		if($o = $result->fetch_object()) die("#{$o->id}: ortsteil_q but not ort_q, or ort_q but not kreis_q");

		$sql = "SELECT * FROM `{$this->table}` WHERE `q` IS NULL";// AND `lat` IS NOT NULL AND ort_q IS NOT NULL"; // AND `kdname`!='' 
		$result = $this->tfc->getSQL ( $this->tfc->dbt , $sql ) ;
		while($o = $result->fetch_object()) {
			$this->wil = new WikidataItemList(); # Because it runs out of memory
			$label = $o->kdname;
			if ( $label == '' and $o->definition='Einzelkulturdenkmal' ) $label = "Haus {$o->strassenname}";
			if ( $label == '' ) $label = $o->strassenname ;
			$label = trim(str_replace('"',"'",str_replace("''","'",$label)));
			if ( strlen($label)>250 ) $label = substr($label,0,250);
			if ( $label == '' ) {
				print "No label for {$o->id}\n" ;
				continue;
			}

			$desc = 'Hessen';
			if ( isset($ortsteil_q) ) {
				$location_q = $o->ortsteil_q;
				$desc = $this->getLabelForItem($o->ort_q).", ".$this->getLabelForItem($o->kreis_q).", {$desc}";
			} else if ( isset($o->ort_q) ) {
				$location_q = $o->ort_q;
				$desc = $this->getLabelForItem($o->kreis_q).", {$desc}";
			} else if ( isset($o->kreis_q) ) {
				$location_q = $o->kreis_q;
			} else {
				continue;
			}
			$desc = "Kulturdenkmal in {$desc}";
			if ( strlen($desc)>250 ) $desc = substr($desc,0,250).'...';

			$p31 = 'Q811979';
			foreach ( $this->p31 as $pattern => $item ) {
				if ( preg_match($pattern,$o->kdname) ) $p31 = $item ;
			}

			$street = preg_replace('|(\d)([A-Z])|','$1|$2',$o->strassenname);
			$streets = explode("|", $street);
			foreach ( $streets AS $k=>$street ) $streets[$k] = str_replace('"',"'",str_replace("''","'",$street));

			$file = $this->get_commons_image_for_id($o->id);

			# Construct QS commands
			$qs = [['CREATE']] ;
			$qs[] = ['LAST','P1769',"\"{$o->id}\""]; # this ID
			$qs[] = ['LAST','P1435','Q85171284']; # heritage designation
			$qs[] = ['LAST','P31',$p31] ; # architectural structure
			$qs[] = ['LAST','Lde',"\"{$label}\""]; # Label
			if ( $desc!='') $qs[] = ['LAST','Dde',"\"{$desc}\"; denkXweb {$o->id}"]; # Label
			if ( isset($o->lat) ) $qs[] = ['LAST','P625',"@{$o->lat}/{$o->lon}"]; # coordinates
			$qs[] = ['LAST','P131',"Q{$location_q}"] ; # adminitrative unit
			$qs[] = ['LAST','P17','Q183']; # country
			if ( isset($file) ) $qs[] = ['LAST','P18',"\"{$file}\""];
			foreach ( $streets AS $street ) $qs[] = ['LAST','P6375',"de:\"{$street}\""];
			foreach ( $qs AS $k=>$v ) {
				if ( $v[0]=='LAST' and !in_array($v[1][0], ['L','D','S']) ) $v = $this->append_references($o,$v);
				$qs[$k] = implode("\t",$v);
			}


			# Run commands
			$q = $this->runQS($qs);
			#print "#{$o->id} : {$q}\n" ;
			if ( preg_match('|^Q?(\d+)$|',"{$q}",$m) ) {
				$has_image = isset($file)?1:0;
				$q = $m[1] ;
				$sql = "UPDATE `{$this->table}` SET `q`={$q},`bot_created`=1,`has_image`={$has_image},`image_on_creation`={$has_image} WHERE `id`={$o->id}" ;
				$this->tfc->getSQL ( $this->tfc->dbt , $sql ) ;
			} else {
				print "Could not create item for #{$o->id}\n";
			}
		}
	}

	public function get_commons_image_for_id($id) {
		$query = 'insource:"{{Kulturdenkmal Hessen|'.$id.'}}"' ;
		$url = "https://commons.wikimedia.org/w/api.php?action=query&list=search&srnamespace=6&format=json&srsearch=".urlencode($query);
		try {
			$j = json_decode(file_get_contents($url));
		} catch(Exception $e) {
			return ;
		}
		if ( count($j->query->search)>=1 ) { # If multiple, take first
			$file = $j->query->search[0]->title;
			$file = preg_replace('|^File:|','',$file);
			return $file;
		}
	}

	protected function runQS($commands) {
		$qs = $this->qs;
		$commands = implode ( "\n" , $commands ) ;
		$tmp_uncompressed = $qs->importData ( $commands , 'v1' ) ;
		$tmp['data']['commands'] = $qs->compressCommands ( $tmp_uncompressed['data']['commands'] ) ;
		$qs->runCommandArray ( $tmp['data']['commands'] ) ;
		return $qs->last_item;
	}

	protected function append_references($o,$v) {
		$v[] = "S248"; # Stated in
		$v[] = "Q19411765";
		$v[] = "S1769"; # Denkmalliste Hessen ID
		$v[] = "\"{$o->id}\"";
		$v[] = "S813"; # Retrieved
		$v[] = date('+Y-m-d').'T00:00:00Z/11';
		return $v ;
	}

	public function find_image_candidates() {
		$sql = "SELECT * FROM `{$this->table}` WHERE q IS NOT NULL AND has_image=0 AND image_candidate_checked=0 AND ort_q IS NOT NULL" ;
		$result = $this->tfc->getSQL ( $this->tfc->dbt , $sql ) ;
		while($o = $result->fetch_object()) {
			$query = "\"{$o->strassenname}\" \"{$o->ort}\"";
			$url = "https://commons.wikimedia.org/w/api.php?action=query&list=search&srnamespace=6&format=json&srsearch=".urlencode($query);
			#print "{$url}\n" ;
			try {
				$j = json_decode(file_get_contents($url));
				if ( count($j->query->search)!=1 ) {
					$sql = "UPDATE `{$this->table}` SET image_candidate_checked=1 WHERE id={$o->id}" ;
					$this->tfc->getSQL ( $this->tfc->dbt , $sql ) ;
					continue ;
				}
				$file = $j->query->search[0]->title;
				$file = preg_replace('|^File:|','',$file);
				$file = $this->tfc->dbt->real_escape_string($file);
				$sql = "UPDATE `{$this->table}` SET image_candidate_checked=1,image_candidate='{$file}' WHERE id={$o->id}" ;
				$this->tfc->getSQL ( $this->tfc->dbt , $sql ) ;
			} catch(Exception $e) {
				$sql = "UPDATE `{$this->table}` SET image_candidate_checked=1 WHERE id={$o->id}" ;
				$this->tfc->getSQL ( $this->tfc->dbt , $sql ) ;
				continue ;
			}
		}
		$this->output_image_candidates();
	}

	public function output_image_candidates() {
		$filename = '/data/project/wikidata-todo/public_html/file_candidates_hessen.txt';
		$fp = fopen($filename, 'w');
		$sql = "SELECT * FROM `{$this->table}` WHERE q IS NOT NULL AND image_candidate IS NOT NULL" ;
		$result = $this->tfc->getSQL ( $this->tfc->dbt , $sql ) ;
		while($o = $result->fetch_object()) {
			$row = json_encode(["Q{$o->q}",$o->image_candidate]);
			fwrite($fp,"{$row}\n");
		}
		fclose($fp);
	}

}

$dh = new DenkmallisteHessen();
#$dh->update_tool_db_from_denkmalpflege_hessen();
$dh->import_coordinates();
$dh->update_metadata_matchings();
$dh->sync_from_wikidata();
$dh->create_new_items();
#$dh->find_image_candidates();

?>