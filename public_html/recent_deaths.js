var pb = 'P569' ;
var pd = 'P570' ;
var wd = new WikiData() ;
var items = [] ;
var loading_count = 0 ;
var m2m = {
	'Jan' : '01' ,
	'Feb' : '02' ,
	'Mar' : '03' ,
	'Apr' : '04' ,
	'May' : '05' ,
	'Jun' : '06' ,
	'Jul' : '07' ,
	'Aug' : '08' ,
	'Sep' : '09' ,
	'Oct' : '10' ,
	'Nov' : '11' ,
	'Dec' : '12'
} ;

function loading ( diff ) {
	loading_count += diff ;
	if ( diff < 0 && loading_count == 0 ) $('#loading').hide() ;
}

function getDateClaim ( q , p ) {
	var claims = wd.items[q].getClaimsForProperty ( p ) ;
	if ( claims[0] === undefined ) return "???" ;
	var t = claims[0].mainsnak.datavalue.value.time ;
	t = t.substr ( 1 , 10 ) ; // Uh-oh...
	return t ;
}

function isSameDate ( d1 , d2 ) {
	if ( d2 == '???' ) return false ;
	if ( undefined === d1 ) return ;
	var m = d1.match ( /^(\S+)\s(\d+),\s(\d+)/ ) ;
	if ( 4 != m.length ) return false ;
	if ( undefined === m2m[m[1]] ) return false ;
	if ( m[2].length == 1 ) m[2] = '0' + m[2] ;
	var nd = m[3] + '-' + m2m[m[1]] + '-' + m[2] ;
	return nd == d2 ;
}

function showItem ( i ) {
	var h = '' ;
	
	$.each ( i.q , function ( dummy , q ) {
		if ( undefined === wd.items[q] ) return ;
		var born = getDateClaim ( q , pb ) ;
		var died = getDateClaim ( q , pd ) ;
		if ( isSameDate ( i.born , born ) ) born = "<b>" + born + "</b>" ;
		if ( isSameDate ( i.died , died ) ) died = "<b>" + died + "</b>" ;
		h += wd.items[q].getLink({target:'_blank'}) + " (" + born + "/" + died + ")<br/>" ;
	} ) ;
	
	$('#iid'+i.id+' td.wikidata').html ( h ) ;
}

function loadItems () {
	$('#loading').html('Loading Wikidata item details...').show() ;
	var q = [] ;
	$.each ( items , function ( id , i ) {
		$.each ( i.q , function ( dummy , v ) {
			q.push ( v ) ;
		} ) ;
	} ) ;
	
	wd.loadItems ( q , {
		finished : function ( params ) {
			
			$.each ( items , function ( id , i ) {
				showItem ( i ) ;
			} ) ;
			
			$('#loading').hide();
		}
	} ) ;
}

function matchItem ( i ) {
	loading ( 1 ) ;
	$.getJSON ( '//www.wikidata.org/w/api.php?callback=?' , {
/*
		action:'wbsearchentities',
		search:'Michael%20Palmer',
		language:'en',
		limit:10,
*/

	
		action:'query',
		list:'search',
		srsearch:i.name,
		srnamespace:0,
		srlimit:10,
		
		format:'json'
	} , function ( d ) {
		loading ( -1 ) ;
		if ( d.query === undefined || d.query.search === undefined ) {
			if ( loading_count == 0 ) loadItems() ;
			return ;
		}
		
//		var h = '' ;
		$.each ( d.query.search , function ( k , v ) {
			i.q.push ( v.title ) ;
//			h += "<div>" + v.title + "</div>" ;
		} ) ;
		
		$('#iid'+i.id+' td.wikidata').html ( "Loaded, waiting..." ) ;

		if ( loading_count == 0 ) loadItems() ;
		
	} ) ;
}

$(document).ready ( function () {

	$('#loading').html('Loading Find-A-Grave RSS feed...').show() ;

	$.get ( './findagrave_recent_celeb_deaths_xml.php' , function ( d ) {
	
		// Generate item list
		$(d).find('item').each ( function ( k , v ) {
			var i = [] ;
			i.q = [] ;
			i.id = items.length ;
			i.title = $($(v).find('title').get(0)).text() ;
			i.link = $($(v).find('link').get(0)).text() ;
			i.desc = $($(v).find('description').get(0)).text() ;
			
			var m = i.title.match ( /^(.+)\s\((.+)\s*-\s*(.+)\)$/i ) ;
			if ( m.length != 4 ) return ;
			
			i.name = $.trim(m[1]) ;
			i.born = $.trim(m[2]) ;
			i.died = $.trim(m[3]) ;
			
			items[i.id] = i ;
			
		} ) ;
		
		// Show item list
		var h = "<table class='table table-condensed table-striped'>" ;
		h += "<thead><tr><th>Person</th><th>Dates</th><th>Wikidata</th></tr></thead><tbody style='font-size:9pt'>" ;
		$.each ( items , function ( id , v ) {
			h += "<tr id='iid" + id + "'>" ;
			h += "<td><a target='_blank' href='" + v.link + "'>" + v.name + "</a></td>" ;
			h += "<td nowrap>" + v.born + "<br/>" + v.died + "</td>" ;
			h += "<td nowrap rowspan='2' class='wikidata'><i>Loading...</i></td>" ;
			h += "</tr>" ;
			h += "<tr><td colspan='2'>" + v.desc + "</td></tr>" ;
		} ) ;
		h += "</tbody></table>" ;
		$('#results').html ( h ) ;

		$('#loading').html('Matching Wikidata items...').show() ;

		$.each ( items , function ( id , v ) {
			matchItem ( v ) ;
		} ) ;
		
	} , 'xml' ) ;
} ) ;
